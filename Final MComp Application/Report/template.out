\BOOKMARK [1][-]{section.1}{Introduction: What is Molecular Docking and Why is it Useful?}{}% 1
\BOOKMARK [1][-]{section.2}{Related Work}{}% 2
\BOOKMARK [2][-]{subsection.2.1}{Molecular Docking Techniques}{section.2}% 3
\BOOKMARK [3][-]{subsubsection.2.1.1}{Automatic}{subsection.2.1}% 4
\BOOKMARK [3][-]{subsubsection.2.1.2}{Interactive}{subsection.2.1}% 5
\BOOKMARK [3][-]{subsubsection.2.1.3}{Additional Techniques}{subsection.2.1}% 6
\BOOKMARK [2][-]{subsection.2.2}{Haptic Devices and Technology}{section.2}% 7
\BOOKMARK [3][-]{subsubsection.2.2.1}{The PHANTOM Haptic Feedback Device}{subsection.2.2}% 8
\BOOKMARK [3][-]{subsubsection.2.2.2}{Uses of Haptics and Haptic Devices}{subsection.2.2}% 9
\BOOKMARK [2][-]{subsection.2.3}{Existing Software with Focus on Usability}{section.2}% 10
\BOOKMARK [2][-]{subsection.2.4}{Collision Detection Methods}{section.2}% 11
\BOOKMARK [3][-]{subsubsection.2.4.1}{Sphere Collisions}{subsection.2.4}% 12
\BOOKMARK [3][-]{subsubsection.2.4.2}{Spatial Partitioning}{subsection.2.4}% 13
\BOOKMARK [2][-]{subsection.2.5}{Collision Response}{section.2}% 14
\BOOKMARK [1][-]{section.3}{Design and Scope of the Project}{}% 15
\BOOKMARK [1][-]{section.4}{Program Design}{}% 16
\BOOKMARK [2][-]{subsection.4.1}{Window Structure}{section.4}% 17
\BOOKMARK [1][-]{section.5}{Protein Viewing}{}% 18
\BOOKMARK [2][-]{subsection.5.1}{Protein Loading}{section.5}% 19
\BOOKMARK [2][-]{subsection.5.2}{Protein Rendering}{section.5}% 20
\BOOKMARK [3][-]{subsubsection.5.2.1}{Sphere-rendering Techniques}{subsection.5.2}% 21
\BOOKMARK [2][-]{subsection.5.3}{Viewing Proteins Individually}{section.5}% 22
\BOOKMARK [2][-]{subsection.5.4}{Transformation of Molecules}{section.5}% 23
\BOOKMARK [3][-]{subsubsection.5.4.1}{Projecting and Unprojecting Coordinates}{subsection.5.4}% 24
\BOOKMARK [3][-]{subsubsection.5.4.2}{Proximity of Cursor to Molecules}{subsection.5.4}% 25
\BOOKMARK [3][-]{subsubsection.5.4.3}{Translation of Molecules}{subsection.5.4}% 26
\BOOKMARK [3][-]{subsubsection.5.4.4}{Rotation of Molecules}{subsection.5.4}% 27
\BOOKMARK [2][-]{subsection.5.5}{Atom Clipping}{section.5}% 28
\BOOKMARK [1][-]{section.6}{Collision Detection and Response}{}% 29
\BOOKMARK [2][-]{subsection.6.1}{Collision Detection}{section.6}% 30
\BOOKMARK [3][-]{subsubsection.6.1.1}{Brute-Force Collisions}{subsection.6.1}% 31
\BOOKMARK [3][-]{subsubsection.6.1.2}{Building a 3D Regular Grid}{subsection.6.1}% 32
\BOOKMARK [3][-]{subsubsection.6.1.3}{Utilising a 3D Regular Grid to Aid Collision Detection}{subsection.6.1}% 33
\BOOKMARK [2][-]{subsection.6.2}{Collision Response}{section.6}% 34
\BOOKMARK [1][-]{section.7}{Force Calculations}{}% 35
\BOOKMARK [1][-]{section.8}{Haptics and Tactile Feedback}{}% 36
\BOOKMARK [2][-]{subsection.8.1}{Haptic Control}{section.8}% 37
\BOOKMARK [2][-]{subsection.8.2}{Haptic Feedback}{section.8}% 38
\BOOKMARK [1][-]{section.9}{Additional User Interaction, Control and Feedback}{}% 39
\BOOKMARK [2][-]{subsection.9.1}{Keyboard and Mouse Control}{section.9}% 40
\BOOKMARK [2][-]{subsection.9.2}{User Feedback}{section.9}% 41
\BOOKMARK [3][-]{subsubsection.9.2.1}{Audio Feedback}{subsection.9.2}% 42
\BOOKMARK [3][-]{subsubsection.9.2.2}{The Force Viewer}{subsection.9.2}% 43
\BOOKMARK [1][-]{section.10}{Graphical User Interface \(GUI\)}{}% 44
\BOOKMARK [2][-]{subsection.10.1}{GUI Design}{section.10}% 45
\BOOKMARK [2][-]{subsection.10.2}{Problems with the GUI}{section.10}% 46
\BOOKMARK [2][-]{subsection.10.3}{Interaction Logic}{section.10}% 47
\BOOKMARK [1][-]{section.11}{Testing}{}% 48
\BOOKMARK [2][-]{subsection.11.1}{Performance Testing}{section.11}% 49
\BOOKMARK [3][-]{subsubsection.11.1.1}{Performance Comparison}{subsection.11.1}% 50
\BOOKMARK [3][-]{subsubsection.11.1.2}{Rendering Performance Testing}{subsection.11.1}% 51
\BOOKMARK [3][-]{subsubsection.11.1.3}{3D Regular Grid Performance Testing}{subsection.11.1}% 52
\BOOKMARK [3][-]{subsubsection.11.1.4}{Attempting to Dock Molecules}{subsection.11.1}% 53
\BOOKMARK [3][-]{subsubsection.11.1.5}{Force Calculation Testing}{subsection.11.1}% 54
\BOOKMARK [3][-]{subsubsection.11.1.6}{OpenTK vs SharpGL}{subsection.11.1}% 55
\BOOKMARK [2][-]{subsection.11.2}{User Testing}{section.11}% 56
\BOOKMARK [1][-]{section.12}{Conclusions}{}% 57
\BOOKMARK [1][-]{section.13}{Future Work}{}% 58
\BOOKMARK [2][-]{subsection.13.1}{Saving and Loading Dock Sites}{section.13}% 59
\BOOKMARK [2][-]{subsection.13.2}{Rendering Performance}{section.13}% 60
\BOOKMARK [1][-]{section*.31}{References}{}% 61

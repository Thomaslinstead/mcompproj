#include <iostream>
#include <math.h>
#include <iterator>
#include "PDBLoading\FileHandle.h"
#include "ProteinRendering\Atoms.h"
using namespace std;

#ifdef __cplusplus    // If used by C++ code, 
extern "C" {          // we need to export the C interface
#endif

//////////////////////DLL Export Methods////////////////////////////////////

//Protein Loading
extern "C" __declspec(dllexport) bool __stdcall loadAtom(int id, char* p1);
extern "C" __declspec(dllexport) bool __stdcall init(char* p1, char* p2);

//Getter Methods
extern "C" __declspec(dllexport) int __stdcall getProteinSize(int id);
extern "C" __declspec(dllexport) float __stdcall getAtomX(int id, int i);
extern "C" __declspec(dllexport) float __stdcall getAtomY(int id, int i);
extern "C" __declspec(dllexport) float __stdcall getAtomZ(int id, int i);
extern "C" __declspec(dllexport) float __stdcall getAtomR(int id, int i);
extern "C" __declspec(dllexport) float __stdcall getAtomG(int id, int i);
extern "C" __declspec(dllexport) float __stdcall getAtomB(int id, int i);
extern "C" __declspec(dllexport) float __stdcall getAtomRadius(int id, int i);
extern "C" __declspec(dllexport) float __stdcall getForceVectorX();
extern "C" __declspec(dllexport) float __stdcall getForceVectorY();
extern "C" __declspec(dllexport) float __stdcall getForceVectorZ();
extern "C" __declspec(dllexport) bool __stdcall moleculeColliding();
extern "C" __declspec(dllexport) bool __stdcall canRender(int id, int i);

//Setter Methods
extern "C" __declspec(dllexport) void __stdcall setCollision(int id, int i);
extern "C" __declspec(dllexport) void __stdcall setGridContact(int id, int i);
extern "C" __declspec(dllexport) void __stdcall setParamA(float A);
extern "C" __declspec(dllexport) void __stdcall setParamB(float B);
extern "C" __declspec(dllexport) void __stdcall setMatrix(int whichMat, int whichVal, float val);
extern "C" __declspec(dllexport) void __stdcall recolourAtom(int r, int g, int b, int id, int i);
extern "C" __declspec(dllexport) void __stdcall applyTranslation(int proteinID, float x, float y, float z);
extern "C" __declspec(dllexport) void __stdcall highlightCollisions(bool r);
extern "C" __declspec(dllexport) void __stdcall renderHydrogen(bool f);

//Reset Methods
extern "C" __declspec(dllexport) void __stdcall resetForceValue();
extern "C" __declspec(dllexport) void __stdcall resetCollision(int id, int i);
extern "C" __declspec(dllexport) void __stdcall resetAtomColour(int id, int i);
extern "C" __declspec(dllexport) bool __stdcall ReleaseMemory(int id);

//Force Collision Methods
extern "C" __declspec(dllexport) void __stdcall atomCollisionID(int atom1, int atom2);
extern "C" __declspec(dllexport) int __stdcall BruteForceCollision(float paramA, float paramB, float x, float y, float z, float x2, float y2, float z2);
extern "C" __declspec(dllexport) int __stdcall calculateForce(float x, float y, float z, float x2, float y2, float z2);

#ifdef __cplusplus
}
#endif
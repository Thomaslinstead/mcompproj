#include <math.h>
#include <sstream>
#include <vector>
#include <array>
#include <list>
#include <array>
#include "ProteinRendering\Atoms.h"
#include "AtomSource.h"

#ifdef __cplusplus    // If used by C++ code, 
extern "C" {          // we need to export the C interface
#endif

#define max(a,b)    (((a) > (b)) ? (a) : (b))
#define min(a,b)    (((a) < (b)) ? (a) : (b))

int base = 32;
const int MAXDEPTH = 10;
const int ANGSTROM = 10;

//Grid 1
int sizeX = 0;
int sizeY = 0;
int sizeZ = 0;
int maxSize = 0;
int quadSize = 0;
float gridXPos[MAXDEPTH + 1][MAXDEPTH + 1][MAXDEPTH + 1];
float gridYPos[MAXDEPTH + 1][MAXDEPTH + 1][MAXDEPTH + 1];
float gridZPos[MAXDEPTH + 1][MAXDEPTH + 1][MAXDEPTH + 1];
float rotatedGridXPos[MAXDEPTH + 1][MAXDEPTH + 1][MAXDEPTH + 1];
float rotatedGridYPos[MAXDEPTH + 1][MAXDEPTH + 1][MAXDEPTH + 1];
float rotatedGridZPos[MAXDEPTH + 1][MAXDEPTH + 1][MAXDEPTH + 1];
int gridXmin = 0;
int gridXmax = 0;
int gridYmin = 0;
int gridYmax = 0;
int gridZmin = 0;
int gridZmax = 0;
int gridXcentre = 0;
int gridYcentre = 0;
int gridZcentre = 0;
std::vector<int> populatedGrid[MAXDEPTH][MAXDEPTH][MAXDEPTH];

//Grid 2
int sizeXTwo = 0;
int sizeYTwo = 0;
int sizeZTwo = 0;
int maxSizeTwo = 0;
int quadSizeTwo = 0;
float gridXPosTwo[MAXDEPTH + 1][MAXDEPTH + 1][MAXDEPTH + 1];
float gridYPosTwo[MAXDEPTH + 1][MAXDEPTH + 1][MAXDEPTH + 1];
float gridZPosTwo[MAXDEPTH + 1][MAXDEPTH + 1][MAXDEPTH + 1];
float rotatedGridXPosTwo[MAXDEPTH + 1][MAXDEPTH + 1][MAXDEPTH + 1];
float rotatedGridYPosTwo[MAXDEPTH + 1][MAXDEPTH + 1][MAXDEPTH + 1];
float rotatedGridZPosTwo[MAXDEPTH + 1][MAXDEPTH + 1][MAXDEPTH + 1];
int gridXminTwo = 0;
int gridXmaxTwo = 0;
int gridYminTwo = 0;
int gridYmaxTwo = 0;
int gridZminTwo = 0;
int gridZmaxTwo = 0;
int gridXcentreTwo = 0;
int gridYcentreTwo = 0;
int gridZcentreTwo = 0;
std::vector<int> populatedGridTwo[MAXDEPTH][MAXDEPTH][MAXDEPTH];

std::vector<int> collidedCells;
std::list<int> collidingAtoms;
std::list<int> collidingAtoms2;
int numCellsInAng = 0;
int prevMaxSize = 0;
float rotMat1[16] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
float rotMat2[16] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
int pop1 = 0;
int pop2 = 0;


/* --- NON-EXTERN FUNCTIONS --- */
float find3DArrayMin(int list[][MAXDEPTH+1][MAXDEPTH+1]) {
	float min = list[0][0][0];
	for (int i = 0; i < (MAXDEPTH+1); i++) {
		for (int j = 0; j < (MAXDEPTH + 1); j++) {
			for (int k = 0; k < (MAXDEPTH + 1); k++) {
				if (list[i][j][k] < min) {
					min = list[i][j][k];
				}
			}
		}
	}
	return min;
}

float find3DArrayMax(int list[][MAXDEPTH+1][MAXDEPTH+1]) {
	float max = list[0][0][0];
	for (int i = 0; i < (MAXDEPTH + 1); i++) {
		for (int j = 0; j < (MAXDEPTH + 1); j++) {
			for (int k = 0; k < (MAXDEPTH + 1); k++) {
				if (list[i][j][k] > max) {
					max = list[i][j][k];
				}
			}
		}
	}
	return max;
}

//Determine XYZ boundaries for first grid
void getGridBoundaries(int id) {
	//Set initial XYZ min/max values from first atom
	gridXmin = getAtomX(id, 0) - getAtomRadius(id, 0);
	gridXmax = getAtomX(id, 0) + getAtomRadius(id, 0);
	gridYmin = getAtomY(id, 0) - getAtomRadius(id, 0);
	gridYmax = getAtomY(id, 0) + getAtomRadius(id, 0);
	gridZmin = getAtomZ(id, 0) - getAtomRadius(id, 0);
	gridZmax = getAtomZ(id, 0) + getAtomRadius(id, 0);
	//Loop through each atom, overwriting exceeded values accordingly
	if (getProteinSize(id) > 1) {
		for (int i = 0; i < getProteinSize(id); i++) {
			if (getAtomX(id, i) - getAtomRadius(id, i) < gridXmin) {
				gridXmin = getAtomX(id, i) - getAtomRadius(id, i);
			}
			else if (getAtomX(id, i) + getAtomRadius(id, i) > gridXmax) {
				gridXmax = getAtomX(id, i) + getAtomRadius(id, i);
			}
			if (getAtomY(id, i) - getAtomRadius(id, i) < gridYmin) {
				gridYmin = getAtomY(id, i) - getAtomRadius(id, i);
			}
			else if (getAtomY(id, i) + getAtomRadius(id, i) > gridYmax) {
				gridYmax = getAtomY(id, i) + getAtomRadius(id, i);
			}
			if (getAtomZ(id, i) - getAtomRadius(id, i) < gridZmin) {
				gridZmin = getAtomZ(id, i) - getAtomRadius(id, i);
			}
			else if (getAtomZ(id, i) + getAtomRadius(id, i) > gridZmax) {
				gridZmax = getAtomZ(id, i) + getAtomRadius(id, i);
			}
			/*if (getAtomX(id, i) < gridXmin) {
				gridXmin = getAtomX(id, i);
			}
			else if (getAtomX(id, i) > gridXmax) {
				gridXmax = getAtomX(id, i);
			}
			if (getAtomY(id, i) < gridYmin) {
				gridYmin = getAtomY(id, i);
			}
			else if (getAtomY(id, i) > gridYmax) {
				gridYmax = getAtomY(id, i);
			}
			if (getAtomZ(id, i) < gridZmin) {
				gridZmin = getAtomZ(id, i);
			}
			else if (getAtomZ(id, i) > gridZmax) {
				gridZmax = getAtomZ(id, i);
			}*/
		}
	}
}

//Determine XYZ boundaries for second grid
void getGridBoundariesTwo(int id) {
	//Set initial XYZ min/max values from first atom
	gridXminTwo = getAtomX(id, 0) - getAtomRadius(id, 0);
	gridXmaxTwo = getAtomX(id, 0) + getAtomRadius(id, 0);
	gridYminTwo = getAtomY(id, 0) - getAtomRadius(id, 0);
	gridYmaxTwo = getAtomY(id, 0) + getAtomRadius(id, 0);
	gridZminTwo = getAtomZ(id, 0) - getAtomRadius(id, 0);
	gridZmaxTwo = getAtomZ(id, 0) + getAtomRadius(id, 0);
	//Loop through each atom, overwriting exceeded values accordingly
	if (getProteinSize(id) > 1) {
		for (int i = 0; i < getProteinSize(id); i++) {
			if (getAtomX(id, i) - getAtomRadius(id, i) < gridXminTwo) {
				gridXminTwo = getAtomX(id, i) - getAtomRadius(id, i);
			}
			else if (getAtomX(id, i) + getAtomRadius(id, i) > gridXmaxTwo) {
				gridXmaxTwo = getAtomX(id, i) + getAtomRadius(id, i);
			}
			if (getAtomY(id, i) - getAtomRadius(id, i) < gridYminTwo) {
				gridYminTwo = getAtomY(id, i) - getAtomRadius(id, i);
			}
			else if (getAtomY(id, i) + getAtomRadius(id, i) > gridYmaxTwo) {
				gridYmaxTwo = getAtomY(id, i) + getAtomRadius(id, i);
			}
			if (getAtomZ(id, i) - getAtomRadius(id, i) < gridZminTwo) {
				gridZminTwo = getAtomZ(id, i) - getAtomRadius(id, i);
			}
			else if (getAtomZ(id, i) + getAtomRadius(id, i) > gridZmaxTwo) {
				gridZmaxTwo = getAtomZ(id, i) + getAtomRadius(id, i);
			}
			/*if (getAtomX(id, i) < gridXminTwo) {
				gridXminTwo = getAtomX(id, i);
			}
			else if (getAtomX(id, i) > gridXmaxTwo) {
				gridXmaxTwo = getAtomX(id, i);
			}
			if (getAtomY(id, i) < gridYminTwo) {
				gridYminTwo = getAtomY(id, i);
			}
			else if (getAtomY(id, i) > gridYmaxTwo) {
				gridYmaxTwo = getAtomY(id, i);
			}
			if (getAtomZ(id, i) < gridZminTwo) {
				gridZminTwo = getAtomZ(id, i);
			}
			else if (getAtomZ(id, i) > gridZmaxTwo) {
				gridZmaxTwo = getAtomZ(id, i);
			}*/
		}
	}
}

//Determines if a single cell-pair are colliding
bool findCellCollisions(int cellX1, int cellY1, int cellZ1, int cellX2, int cellY2, int cellZ2, float tx1, float ty1, float tz1, float tx2, float ty2, float tz2) {
	//Approximate single cell AABBs (for rotations, deprecated)
	/*float cellRotXmin = findRotatedXmin(cellX1, cellY1, cellZ1, cellX1 + 1, cellY1 + 1, cellZ1 + 1);
	float cellRotXmax = findRotatedXmax(cellX1, cellY1, cellZ1, cellX1 + 1, cellY1 + 1, cellZ1 + 1);
	float cellRotYmin = findRotatedYmin(cellX1, cellY1, cellZ1, cellX1 + 1, cellY1 + 1, cellZ1 + 1);
	float cellRotYmax = findRotatedYmax(cellX1, cellY1, cellZ1, cellX1 + 1, cellY1 + 1, cellZ1 + 1);
	float cellRotZmin = findRotatedZmin(cellX1, cellY1, cellZ1, cellX1 + 1, cellY1 + 1, cellZ1 + 1);
	float cellRotZmax = findRotatedZmax(cellX1, cellY1, cellZ1, cellX1 + 1, cellY1 + 1, cellZ1 + 1);
	float cellRotXminTwo = findRotatedXminTwo(cellX2, cellY2, cellZ2, cellX2 + 1, cellY2 + 1, cellZ2 + 1);
	float cellRotXmaxTwo = findRotatedXmaxTwo(cellX2, cellY2, cellZ2, cellX2 + 1, cellY2 + 1, cellZ2 + 1);
	float cellRotYminTwo = findRotatedYminTwo(cellX2, cellY2, cellZ2, cellX2 + 1, cellY2 + 1, cellZ2 + 1);
	float cellRotYmaxTwo = findRotatedYmaxTwo(cellX2, cellY2, cellZ2, cellX2 + 1, cellY2 + 1, cellZ2 + 1);
	float cellRotZminTwo = findRotatedZminTwo(cellX2, cellY2, cellZ2, cellX2 + 1, cellY2 + 1, cellZ2 + 1);
	float cellRotZmaxTwo = findRotatedZmaxTwo(cellX2, cellY2, cellZ2, cellX2 + 1, cellY2 + 1, cellZ2 + 1);*/

	//Compare the two cells provided to check for collision, return true if colliding or false if not
	if ((gridXPos[cellX1][cellY1][cellZ1] + quadSize + (4 * ANGSTROM) >= gridXPosTwo[cellX2][cellY2][cellZ2] - (4 * ANGSTROM)) && (gridXPos[cellX1][cellY1][cellZ1] - (4 * ANGSTROM) <= gridXPosTwo[cellX2][cellY2][cellZ2] + quadSizeTwo + (4 * ANGSTROM))) {
		if ((gridYPos[cellX1][cellY1][cellZ1] + quadSize + (4 * ANGSTROM) >= gridYPosTwo[cellX2][cellY2][cellZ2] - (4 * ANGSTROM)) && (gridYPos[cellX1][cellY1][cellZ1] - (4 * ANGSTROM) <= gridYPosTwo[cellX2][cellY2][cellZ2] + quadSizeTwo + (4 * ANGSTROM))) {
			if ((gridZPos[cellX1][cellY1][cellZ1] + quadSize + (4 * ANGSTROM) >= gridZPosTwo[cellX2][cellY2][cellZ2] - (4 * ANGSTROM)) && (gridZPos[cellX1][cellY1][cellZ1] - (4 * ANGSTROM) <= gridZPosTwo[cellX2][cellY2][cellZ2] + quadSizeTwo + (4 * ANGSTROM))) {
				return true;
			} else { return false; }
		} else { return false; }
	} else { return false; }
}

/* --- EXTERN FUNCTIONS --- */

/** Function to build first grid **/
extern "C" __declspec(dllexport) void __stdcall buildRegularGrid(int id, float tx, float ty, float tz) {
	//Get boundaries
	getGridBoundaries(id);
	//Calculate size in each axis
	sizeX = abs(gridXmin - gridXmax);
	sizeY = abs(gridYmin - gridYmax);
	sizeZ = abs(gridZmin - gridZmax);
	//Calculate overall grid size and thus the size of each cell
	maxSize = max(max(sizeX,sizeY),sizeZ);
	quadSize = round(maxSize / MAXDEPTH);
	//Loop through depth of grid on each axis, storing coordinates of each cell
	for (int x = 0; x < MAXDEPTH + 1; x++) {
		for (int y = 0; y < MAXDEPTH + 1; y++) {
			for (int z = 0; z < MAXDEPTH + 1; z++) {
				gridXPos[x][y][z] = ((gridXmin + gridXmax) / 2) - (maxSize / 2) + (quadSize * x);
				gridYPos[x][y][z] = ((gridYmin + gridYmax) / 2) - (maxSize / 2) + (quadSize * y);
				gridZPos[x][y][z] = ((gridZmin + gridZmax) / 2) - (maxSize / 2) + (quadSize * z);
			}
		}
	}
}

/** Function to build second grid **/
extern "C" __declspec(dllexport) void __stdcall buildRegularGridTwo(int id, float tx, float ty, float tz) {
	//Get boundaries
	getGridBoundariesTwo(id);
	//Calculate size in each axis
	sizeXTwo = abs(gridXminTwo - gridXmaxTwo);
	sizeYTwo = abs(gridYminTwo - gridYmaxTwo);
	sizeZTwo = abs(gridZminTwo - gridZmaxTwo);
	//Calculate overall grid size and thus the size of each cell
	maxSizeTwo = max(max(sizeXTwo, sizeYTwo), sizeZTwo);
	quadSizeTwo = round(maxSizeTwo / MAXDEPTH);
	//Loop through depth of grid on each axis, storing coordinates of each cell
	for (int x = 0; x < MAXDEPTH + 1; x++) {
		for (int y = 0; y < MAXDEPTH + 1; y++) {
			for (int z = 0; z < MAXDEPTH + 1; z++) {
				gridXPosTwo[x][y][z] = ((gridXminTwo + gridXmaxTwo) / 2) - (maxSizeTwo / 2) + (quadSizeTwo * x);
				gridYPosTwo[x][y][z] = ((gridYminTwo + gridYmaxTwo) / 2) - (maxSizeTwo / 2) + (quadSizeTwo * y);
				gridZPosTwo[x][y][z] = ((gridZminTwo + gridZmaxTwo) / 2) - (maxSizeTwo / 2) + (quadSizeTwo * z);
			}
		}
	}
}

//Populate first grid
extern "C" __declspec(dllexport) void __stdcall populateGrid(int id) {
	pop1 = 0;
	//Clear previous population data
	for (int x = 0; x < MAXDEPTH; x++) {
		for (int y = 0; y < MAXDEPTH; y++) {
			for (int z = 0; z < MAXDEPTH; z++) {
				populatedGrid[x][y][z].clear();
			}
		}
	}
	//Loop through each atom of molecule
	for (int i = 0; i < getProteinSize(id); i++) {
		//Reset flags to 0
		int xEdge = 0;
		int yEdge = 0;
		int zEdge = 0;
		//Calculate the cell that atom's centre is located in
		int cellX = floor((getAtomX(id,i) - gridXPos[0][0][0]) / quadSize);
		int cellY = floor((getAtomY(id,i) - gridYPos[0][0][0]) / quadSize);
		int cellZ = floor((getAtomZ(id,i) - gridZPos[0][0][0]) / quadSize);
		populatedGrid[cellX][cellY][cellZ].push_back(i); pop1++;
		//Calculate which cells adjacent to the one above the atom partially resides in, and set flags to calculate non-adjacent neighbours
		if ((cellX > 0) && ((getAtomX(id, i) - getAtomRadius(id,i)) < gridXPos[cellX][cellY][cellZ])) {
			populatedGrid[cellX - 1][cellY][cellZ].push_back(i); pop1++;
			xEdge = -1;
		}
		else if ((cellX < MAXDEPTH - 1) && ((getAtomX(id,i) + getAtomRadius(id,i)) > gridXPos[cellX + 1][cellY][cellZ])) {
			populatedGrid[cellX + 1][cellY][cellZ].push_back(i); pop1++;
			xEdge = 1;
		}
		if ((cellY > 0) && ((getAtomY(id,i) - getAtomRadius(id,i)) < gridYPos[cellX][cellY][cellZ])) {
			populatedGrid[cellX][cellY - 1][cellZ].push_back(i); pop1++;
			yEdge = -1;
		}
		else if ((cellY < MAXDEPTH - 1) && ((getAtomY(id,i) + getAtomRadius(id,i)) > gridYPos[cellX][cellY + 1][cellZ])) {
			populatedGrid[cellX][cellY + 1][cellZ].push_back(i); pop1++;
			yEdge = 1;
		}
		if ((cellZ > 0) && ((getAtomZ(id,i) - getAtomRadius(id,i)) < gridZPos[cellX][cellY][cellZ])) {
			populatedGrid[cellX][cellY][cellZ - 1].push_back(i); pop1++;
			zEdge = -1;
		}
		else if ((cellZ < MAXDEPTH - 1) && ((getAtomZ(id,i) + getAtomRadius(id,i)) > gridZPos[cellX][cellY][cellZ + 1])) {
			populatedGrid[cellX][cellY][cellZ + 1].push_back(i); pop1++;
			zEdge = 1;
		}
		//Infer non-adjacents from flags gathered above
		if ((xEdge != 0) && (yEdge != 0)) {
			populatedGrid[cellX + xEdge][cellY + yEdge][cellZ].push_back(i); pop1++;
		}
		if ((xEdge != 0) && (zEdge != 0)) {
			populatedGrid[cellX + xEdge][cellY][cellZ + zEdge].push_back(i); pop1++;
		}
		if ((yEdge != 0) && (zEdge != 0)) {
			populatedGrid[cellX][cellY + yEdge][cellZ + zEdge].push_back(i); pop1++;
		}
		if ((xEdge != 0) && (yEdge != 0) && (zEdge != 0)) {
			populatedGrid[cellX + xEdge][cellY + yEdge][cellZ + zEdge].push_back(i); pop1++;
		}
	}
}

//Populate second grid
extern "C" __declspec(dllexport) void __stdcall populateGridTwo(int id) {
	pop2 = 0;
	//Clear previous population data
	for (int x = 0; x < MAXDEPTH; x++) {
		for (int y = 0; y < MAXDEPTH; y++) {
			for (int z = 0; z < MAXDEPTH; z++) {
				populatedGridTwo[x][y][z].clear();
			}
		}
	}
	//Loop through each atom of molecule
	for (int i = 0; i < getProteinSize(id); i++) {
		//Reset flags to 0
		int xEdge = 0;
		int yEdge = 0;
		int zEdge = 0;
		//Calculate the cell that atom's centre is located in
		int cellX = floor((getAtomX(id, i) - gridXPosTwo[0][0][0]) / quadSizeTwo);
		int cellY = floor((getAtomY(id, i) - gridYPosTwo[0][0][0]) / quadSizeTwo);
		int cellZ = floor((getAtomZ(id, i) - gridZPosTwo[0][0][0]) / quadSizeTwo);
		populatedGridTwo[cellX][cellY][cellZ].push_back(i); pop2++;
		//Calculate which cells adjacent to the one above the atom partially resides in, and set flags to calculate non-adjacent neighbours
		if ((cellX > 0) && ((getAtomX(id, i) - getAtomRadius(id, i)) < gridXPosTwo[cellX][cellY][cellZ])) {
			populatedGridTwo[cellX - 1][cellY][cellZ].push_back(i); pop2++;
			xEdge = -1;
		}
		else if ((cellX < MAXDEPTH - 1) && ((getAtomX(id, i) + getAtomRadius(id, i)) > gridXPosTwo[cellX + 1][cellY][cellZ])) {
			populatedGridTwo[cellX + 1][cellY][cellZ].push_back(i); pop2++;
			xEdge = 1;
		}
		if ((cellY > 0) && ((getAtomY(id, i) - getAtomRadius(id, i)) < gridYPosTwo[cellX][cellY][cellZ])) {
			populatedGridTwo[cellX][cellY - 1][cellZ].push_back(i); pop2++;
			yEdge = -1;
		}
		else if ((cellY < MAXDEPTH - 1) && ((getAtomY(id, i) + getAtomRadius(id, i)) > gridYPosTwo[cellX][cellY + 1][cellZ])) {
			populatedGridTwo[cellX][cellY + 1][cellZ].push_back(i); pop2++;
			yEdge = 1;
		}
		if ((cellZ > 0) && ((getAtomZ(id, i) - getAtomRadius(id, i)) < gridZPosTwo[cellX][cellY][cellZ])) {
			populatedGridTwo[cellX][cellY][cellZ - 1].push_back(i); pop2++;
			zEdge = -1;
		}
		else if ((cellZ < MAXDEPTH - 1) && ((getAtomZ(id, i) + getAtomRadius(id, i)) > gridZPosTwo[cellX][cellY][cellZ + 1])) {
			populatedGridTwo[cellX][cellY][cellZ + 1].push_back(i); pop2++;
			zEdge = 1;
		}
		//Infer non-adjacents from flags gathered above
		if ((xEdge != 0) && (yEdge != 0)) {
			populatedGridTwo[cellX + xEdge][cellY + yEdge][cellZ].push_back(i); pop2++;
		}
		if ((xEdge != 0) && (zEdge != 0)) {
			populatedGridTwo[cellX + xEdge][cellY][cellZ + zEdge].push_back(i); pop2++;
		}
		if ((yEdge != 0) && (zEdge != 0)) {
			populatedGridTwo[cellX][cellY + yEdge][cellZ + zEdge].push_back(i); pop2++;
		}
		if ((xEdge != 0) && (yEdge != 0) && (zEdge != 0)) {
			populatedGridTwo[cellX + xEdge][cellY + yEdge][cellZ + zEdge].push_back(i); pop2++;
		}
	}
}

//Finds all cell-pair collisions between the two grids
extern "C" __declspec(dllexport) int __stdcall findGridCollisions(float tx1, float ty1, float tz1, float tx2, float ty2, float tz2) {
	/* ROTATIONS - NO LONGER NEEDED */
	/*
	//Update rotation matrices
	//setRotMats1(rx1, ry1, rz1);
	//setRotMats2(rx2, ry2, rz2);
	
	//Update rotated grid positions
	float tempVec[3];
	for (int i = 0; i < MAXDEPTH; i++) {
		for (int j = 0; j < MAXDEPTH; j++) {
			for (int k = 0; k < MAXDEPTH; k++) {
				//First grid
				tempVec[0] = gridXPos[i][j][k];
				tempVec[1] = gridYPos[i][j][k];
				tempVec[2] = gridZPos[i][j][k];
				//rotatedGridXPos[i][j][k] = rotMat1[0] * tempVec[0] + rotMat1[4] * tempVec[1] + rotMat1[8] * tempVec[2];
				//rotatedGridYPos[i][j][k] = rotMat1[1] * tempVec[0] + rotMat1[5] * tempVec[1] + rotMat1[9] * tempVec[2];
				//rotatedGridZPos[i][j][k] = rotMat1[2] * tempVec[0] + rotMat1[6] * tempVec[1] + rotMat1[10] * tempVec[2];

				rotatedGridXPos[i][j][k] = rotMat1[0] * tempVec[0] + rotMat1[1] * tempVec[1] + rotMat1[2] * tempVec[2];
				rotatedGridYPos[i][j][k] = rotMat1[4] * tempVec[0] + rotMat1[5] * tempVec[1] + rotMat1[6] * tempVec[2];
				rotatedGridZPos[i][j][k] = rotMat1[8] * tempVec[0] + rotMat1[9] * tempVec[1] + rotMat1[10] * tempVec[2];

				//Second grid
				tempVec[0] = gridXPosTwo[i][j][k];
				tempVec[1] = gridYPosTwo[i][j][k];
				tempVec[2] = gridZPosTwo[i][j][k];
				//rotatedGridXPosTwo[i][j][k] = rotMat2[0] * tempVec[0] + rotMat2[4] * tempVec[1] + rotMat2[8] * tempVec[2];
				//rotatedGridYPosTwo[i][j][k] = rotMat2[1] * tempVec[0] + rotMat2[5] * tempVec[1] + rotMat2[9] * tempVec[2];
				//rotatedGridZPosTwo[i][j][k] = rotMat2[2] * tempVec[0] + rotMat2[6] * tempVec[1] + rotMat2[10] * tempVec[2];

				rotatedGridXPos[i][j][k] = rotMat1[0] * tempVec[0] + rotMat1[1] * tempVec[1] + rotMat1[2] * tempVec[2];
				rotatedGridYPos[i][j][k] = rotMat1[4] * tempVec[0] + rotMat1[5] * tempVec[1] + rotMat1[6] * tempVec[2];
				rotatedGridZPos[i][j][k] = rotMat1[8] * tempVec[0] + rotMat1[9] * tempVec[1] + rotMat1[10] * tempVec[2];

			}
		}
	}

	//Approximate whole grid AABBs
	float gridRotXmin = findRotatedXmin(0, 0, 0, MAXDEPTH + 1, MAXDEPTH + 1, MAXDEPTH + 1);
	float gridRotXmax = findRotatedXmax(0, 0, 0, MAXDEPTH + 1, MAXDEPTH + 1, MAXDEPTH + 1);
	float gridRotYmin = findRotatedYmin(0, 0, 0, MAXDEPTH + 1, MAXDEPTH + 1, MAXDEPTH + 1);
	float gridRotYmax = findRotatedYmax(0, 0, 0, MAXDEPTH + 1, MAXDEPTH + 1, MAXDEPTH + 1);
	float gridRotZmin = findRotatedZmin(0, 0, 0, MAXDEPTH + 1, MAXDEPTH + 1, MAXDEPTH + 1);
	float gridRotZmax = findRotatedZmax(0, 0, 0, MAXDEPTH + 1, MAXDEPTH + 1, MAXDEPTH + 1);
	float gridRotXminTwo = findRotatedXminTwo(0, 0, 0, MAXDEPTH + 1, MAXDEPTH + 1, MAXDEPTH + 1);
	float gridRotXmaxTwo = findRotatedXmaxTwo(0, 0, 0, MAXDEPTH + 1, MAXDEPTH + 1, MAXDEPTH + 1);
	float gridRotYminTwo = findRotatedYminTwo(0, 0, 0, MAXDEPTH + 1, MAXDEPTH + 1, MAXDEPTH + 1);
	float gridRotYmaxTwo = findRotatedYmaxTwo(0, 0, 0, MAXDEPTH + 1, MAXDEPTH + 1, MAXDEPTH + 1);
	float gridRotZminTwo = findRotatedZminTwo(0, 0, 0, MAXDEPTH + 1, MAXDEPTH + 1, MAXDEPTH + 1);
	float gridRotZmaxTwo = findRotatedZmaxTwo(0, 0, 0, MAXDEPTH + 1, MAXDEPTH + 1, MAXDEPTH + 1);
	*/

	int colls = 0;
	bool done = false;
	bool found = false;
	//Set boolean array of cells from second grid that have already been found colliding, to avoid repeatedly setting same flags for efficiency
	bool cellsDone[MAXDEPTH + 1][MAXDEPTH + 1][MAXDEPTH + 1];
	for (int x = 0; x < MAXDEPTH; x++) {
		for (int y = 0; y < MAXDEPTH; y++) {
			for (int z = 0; z < MAXDEPTH; z++) {
				cellsDone[x][y][z] = false;
			}
		}
	}
	//Check for overall grid collisions for efficiency, rest of method can be skipped if this test fails
	if ((gridXPos[MAXDEPTH][0][0] + (4 * ANGSTROM) >= gridXPosTwo[0][0][0] - (4 * ANGSTROM)) && (gridXPos[0][0][0] - (4 * ANGSTROM) <= gridXPosTwo[MAXDEPTH][0][0] + (4 * ANGSTROM))) {
		if ((gridYPos[0][MAXDEPTH][0] + (4 * ANGSTROM) >= gridYPosTwo[0][0][0] - (4 * ANGSTROM)) && (gridYPos[0][0][0] - (4 * ANGSTROM) <= gridYPosTwo[0][MAXDEPTH][0] + (4 * ANGSTROM))) {
			if ((gridZPos[0][0][MAXDEPTH] + (4 * ANGSTROM) >= gridZPosTwo[0][0][0] - (4 * ANGSTROM)) && (gridZPos[0][0][0] - (4 * ANGSTROM) <= gridZPosTwo[0][0][MAXDEPTH] + (4 * ANGSTROM))) {
				//Loop through every cell of first grid
				for (int i = 0; i < MAXDEPTH; i++) {
					for (int j = 0; j < MAXDEPTH; j++) {
						for (int k = 0; k < MAXDEPTH; k++) {
							/*//Calculate true min/max of each axis after rotations - deprecated
							float cellRotXmin = findRotatedXmin(i, j, k, i + 1, j + 1, k + 1);
							float cellRotXmax = findRotatedXmax(i, j, k, i + 1, j + 1, k + 1);
							float cellRotYmin = findRotatedYmin(i, j, k, i + 1, j + 1, k + 1);
							float cellRotYmax = findRotatedYmax(i, j, k, i + 1, j + 1, k + 1);
							float cellRotZmin = findRotatedZmin(i, j, k, i + 1, j + 1, k + 1);
							float cellRotZmax = findRotatedZmax(i, j, k, i + 1, j + 1, k + 1);*/
							//Nothing to collide with if cell is empty, skip cell if fail
							if (!populatedGrid[i][j][k].empty()) {
								//Compare cell to other whole grid before its individual cells for efficiency, skip cell if fail
								if ((gridXPos[i + 1][0][0] + (4 * ANGSTROM) >= gridXPosTwo[0][0][0] - (4 * ANGSTROM)) && (gridXPos[i][0][0] - (4 * ANGSTROM) <= gridXPosTwo[MAXDEPTH][0][0] + (4 * ANGSTROM))) {
									if ((gridYPos[0][j + 1][0] + (4 * ANGSTROM) >= gridYPosTwo[0][0][0] - (4 * ANGSTROM)) && (gridYPos[0][j][0] - (4 * ANGSTROM) <= gridYPosTwo[0][MAXDEPTH][0] + (4 * ANGSTROM))) {
										if ((gridZPos[0][0][k + 1] + (4 * ANGSTROM) >= gridZPosTwo[0][0][0] - (4 * ANGSTROM)) && (gridZPos[0][0][k] - (4 * ANGSTROM) <= gridZPosTwo[0][0][MAXDEPTH] + (4 * ANGSTROM))) {
											
											/* INCOMPLETE - Highly optimised new method of finding grid collisions, using similar concept to populateGrid methods */
											//found = false;
											////Calculate exact point of which cell of grid 2 is in contact
											//float diffX = (gridXPos[i][j][k] - gridXPos[0][0][0]) / quadSizeTwo;
											//float diffY = (gridYPos[i][j][k] - gridYPos[0][0][0]) / quadSizeTwo;
											//float diffZ = (gridZPos[i][j][k] - gridZPos[0][0][0]) / quadSizeTwo;
											////Determine central contact cell from grid 2
											//int cellX = ceil(diffX);
											//int cellY = ceil(diffY);
											//int cellZ = ceil(diffZ);
											////Reset flags to 0
											//int xFlag = 0;
											//int yFlag = 0;
											//int zFlag = 0;
											////Determine which neighbour cells from grid 2 are colliding
											//if (fmod(diffX, 1) < 0.5) {
											//	xFlag = -1;
											//}
											//else if (fmod(diffX, 1) > 0.5) {
											//	xFlag = 1;
											//}
											//if (fmod(diffY, 1) < 0.5) {
											//	yFlag = -1;
											//}
											//else if (fmod(diffY, 1) > 0.5) {
											//	yFlag = 1;
											//}
											//if (fmod(diffZ, 1) < 0.5) {
											//	zFlag = -1;
											//}
											//else if (fmod(diffZ, 1) > 0.5) {
											//	zFlag = 1;
											//}
											////Central cell collision
											//if (!populatedGridTwo[cellX][cellY][cellZ].empty()) {
											//	//if (!cellsDone[cellX][cellY][cellZ]) {
											//		for (int p = 0; p < populatedGridTwo[cellX][cellY][cellZ].size(); p++) {
											//			setGridContact(2, populatedGridTwo[cellX][cellY][cellZ].at(p));
											//		}
											//		//cellsDone[cellX][cellY][cellZ] = true;
											//	//}
											//	found = true;
											//}
											////Adjacent neighbour cells colliding
											//if (!populatedGridTwo[cellX + xFlag][cellY][cellZ].empty()) {
											//	//if (!cellsDone[cellX + xFlag][cellY][cellZ]) {
											//		for (int p = 0; p < populatedGridTwo[cellX + xFlag][cellY][cellZ].size(); p++) {
											//			setGridContact(2, populatedGridTwo[cellX + xFlag][cellY][cellZ].at(p));
											//		}
											//		//cellsDone[cellX + xFlag][cellY][cellZ] = true;
											//	//}
											//	found = true;
											//}
											//if (!populatedGridTwo[cellX][cellY + xFlag][cellZ].empty()) {
											//	//if (!cellsDone[cellX][cellY + yFlag][cellZ]) {
											//		for (int p = 0; p < populatedGridTwo[cellX][cellY + yFlag][cellZ].size(); p++) {
											//			setGridContact(2, populatedGridTwo[cellX][cellY + yFlag][cellZ].at(p));
											//		}
											//		//cellsDone[cellX][cellY + yFlag][cellZ] = true;
											//	//}
											//	found = true;
											//}
											//if (!populatedGridTwo[cellX][cellY][cellZ + zFlag].empty()) {
											//	//if (!cellsDone[cellX][cellY][cellZ + zFlag]) {
											//		for (int p = 0; p < populatedGridTwo[cellX][cellY][cellZ + zFlag].size(); p++) {
											//			setGridContact(2, populatedGridTwo[cellX][cellY][cellZ + zFlag].at(p));
											//		}
											//		//cellsDone[cellX][cellY][cellZ + zFlag] = true;
											//	//}
											//	found = true;
											//}
											////Non-adjacent neighbour cells colliding
											//if (!populatedGridTwo[cellX + xFlag][cellY + yFlag][cellZ].empty()) {
											//	//if (!cellsDone[cellX + xFlag][cellY + yFlag][cellZ]) {
											//		for (int p = 0; p < populatedGridTwo[cellX + xFlag][cellY + yFlag][cellZ].size(); p++) {
											//			setGridContact(2, populatedGridTwo[cellX + xFlag][cellY + yFlag][cellZ].at(p));
											//		}
											//		//cellsDone[cellX + xFlag][cellY + yFlag][cellZ] = true;
											//	//}
											//	found = true;
											//}
											//if (!populatedGridTwo[cellX + xFlag][cellY][cellZ + zFlag].empty()) {
											//	//if (!cellsDone[cellX + xFlag][cellY][cellZ + zFlag]) {
											//		for (int p = 0; p < populatedGridTwo[cellX + xFlag][cellY][cellZ + zFlag].size(); p++) {
											//			setGridContact(2, populatedGridTwo[cellX + xFlag][cellY][cellZ + zFlag].at(p));
											//		}
											//		//cellsDone[cellX + xFlag][cellY][cellZ + zFlag] = true;
											//	//}
											//	found = true;
											//}
											//if (!populatedGridTwo[cellX][cellY + yFlag][cellZ + zFlag].empty()) {
											//	//if (!cellsDone[cellX][cellY + yFlag][cellZ + zFlag]) {
											//		for (int p = 0; p < populatedGridTwo[cellX][cellY + yFlag][cellZ + zFlag].size(); p++) {
											//			setGridContact(2, populatedGridTwo[cellX][cellY + yFlag][cellZ + zFlag].at(p));
											//		}
											//		//cellsDone[cellX][cellY + yFlag][cellZ + zFlag] = true;
											//	//}
											//	found = true;
											//}
											//if (!populatedGridTwo[cellX + xFlag][cellY + yFlag][cellZ + zFlag].empty()) {
											//	//if (!cellsDone[cellX + xFlag][cellY + yFlag][cellZ + zFlag]) {
											//		for (int p = 0; p < populatedGridTwo[cellX + yFlag][cellY + xFlag][cellZ + zFlag].size(); p++) {
											//			setGridContact(2, populatedGridTwo[cellX + yFlag][cellY + xFlag][cellZ + zFlag].at(p));
											//		}
											//		//cellsDone[cellX + xFlag][cellY + yFlag][cellZ + zFlag] = true;
											//	//}
											//	found = true;
											//}
											////Set collision in grid 1 cell if appropriate
											//if (found) {
											//	for (int e = 0; e < populatedGrid[i][j][k].size(); e++) {
											//		setGridContact(1, populatedGrid[i][j][k].at(e));
											//	}
											//}


											done = false;
											//Loop through every cell of second grid
											for (int l = 0; l < MAXDEPTH; l++) {
												for (int m = 0; m < MAXDEPTH; m++) {
													for (int n = 0; n < MAXDEPTH; n++) {
														//Nothing to collide with if cell is empty, skip cell if fail
														if (!populatedGridTwo[l][m][n].empty()) {
															//Determine if the two cells under test are colliding with eachother
															if (findCellCollisions(i, j, k, l, m, n, tx1, ty1, tz1, tx2, ty2, tz2)) {
																//If not already done, set grid collision flag of each atom in this cell of first grid to true
																if (!done) {
																	for (int e = 0; e < populatedGrid[i][j][k].size(); e++) {
																		setGridContact(1, populatedGrid[i][j][k].at(e));
																		done = true;
																	}
																}
																//If not already done, set grid collision flag of each atom in this cell of second grid to true
																if (!cellsDone[l][m][n]) {
																	for (int p = 0; p < populatedGridTwo[l][m][n].size(); p++) {
																		setGridContact(2, populatedGridTwo[l][m][n].at(p));
																	}
																	cellsDone[l][m][n] = true;
																}
																colls++;
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}
	return colls;
}

/* Accessor methods */
extern "C" __declspec(dllexport) int __stdcall getpop1() {
	return pop1;
	/*if (populatedGrid[3][2][3].size() != 0) {
	return populatedGrid[3][2][3].at(0);
	}
	else {
	return -100;
	}*/
}

extern "C" __declspec(dllexport) int __stdcall getpop2() {
	return pop2;
}

extern "C" __declspec(dllexport) float __stdcall getxmin() {
	return gridXPos[0][0][0];
	//return gridXmin;
}

extern "C" __declspec(dllexport) float __stdcall getxmax() {
	return gridXPos[MAXDEPTH][MAXDEPTH][MAXDEPTH];
	//return gridXmax;
}

extern "C" __declspec(dllexport) float __stdcall getymin() {
	return gridYPos[0][0][0];
	//return gridYmin;
}

extern "C" __declspec(dllexport) float __stdcall getymax() {
	return gridYPos[MAXDEPTH][MAXDEPTH][MAXDEPTH];
	//return gridYmax;
}

extern "C" __declspec(dllexport) float __stdcall getzmin() {
	return gridZPos[0][0][0];
	//return gridZmin;
}

extern "C" __declspec(dllexport) float __stdcall getzmax() {
	return gridZPos[MAXDEPTH][MAXDEPTH][MAXDEPTH];
	//return gridZmax;
}

extern "C" __declspec(dllexport) float __stdcall getxmin2() {
	return gridXPosTwo[0][0][0];
	//return gridXminTwo;
}

extern "C" __declspec(dllexport) float __stdcall getxmax2() {
	return gridXPosTwo[MAXDEPTH][MAXDEPTH][MAXDEPTH];
	//return gridXmaxTwo;
}

extern "C" __declspec(dllexport) float __stdcall getymin2() {
	return gridYPosTwo[0][0][0];
	//return gridYminTwo;
}

extern "C" __declspec(dllexport) float __stdcall getymax2() {
	return gridYPosTwo[MAXDEPTH][MAXDEPTH][MAXDEPTH];
	//return gridYmaxTwo;
}

extern "C" __declspec(dllexport) float __stdcall getzmin2() {
	return gridZPosTwo[0][0][0];
	//return gridZminTwo;
}

extern "C" __declspec(dllexport) float __stdcall getzmax2() {
	return gridZPosTwo[MAXDEPTH][MAXDEPTH][MAXDEPTH];
	//return gridZmaxTwo;
}

/* DEPRECATED ROTATIONS CODE - ROTATIONS ARE NOW HANDLED IN ATOMSOURCE.CPP */
/*
extern "C" __declspec(dllexport) void __stdcall updateRotMats(int whichMat, int whichVal, float val) {
	if (whichMat == 1) {
		rotMat1[whichVal] = val;
	}
	else {
		rotMat2[whichVal] = val;
	}
}

float findRotatedXmin(int lowerX, int lowerY, int lowerZ, int upperX, int upperY, int upperZ) {
	float min = rotatedGridXPos[lowerX][lowerY][lowerZ];
	if (rotatedGridXPos[upperX][lowerY][lowerZ] > min) {
		min = rotatedGridXPos[upperX][lowerY][lowerZ];
	}
	if (rotatedGridXPos[lowerX][upperY][lowerZ] > min) {
		min = rotatedGridXPos[lowerX][upperY][lowerZ];
	}
	if (rotatedGridXPos[lowerX][lowerY][upperZ] > min) {
		min = rotatedGridXPos[lowerX][lowerY][upperZ];
	}
	if (rotatedGridXPos[upperX][upperY][lowerZ] > min) {
		min = rotatedGridXPos[upperX][upperY][lowerZ];
	}
	if (rotatedGridXPos[upperX][lowerY][upperZ] > min) {
		min = rotatedGridXPos[upperX][lowerY][upperZ];
	}
	if (rotatedGridXPos[upperX][upperY][upperZ] > min) {
		min = rotatedGridXPos[upperX][upperY][upperZ];
	}
	if (rotatedGridXPos[lowerX][upperY][upperZ] > min) {
		min = rotatedGridXPos[lowerX][upperY][upperZ];
	}
	return min;
}

float findRotatedXmax(int lowerX, int lowerY, int lowerZ, int upperX, int upperY, int upperZ) {
	float max = rotatedGridXPos[lowerX][lowerY][lowerZ];
	if (rotatedGridXPos[upperX][lowerY][lowerZ] > max) {
		max = rotatedGridXPos[upperX][lowerY][lowerZ];
	}
	if (rotatedGridXPos[lowerX][upperY][lowerZ] > max) {
		max = rotatedGridXPos[lowerX][upperY][lowerZ];
	}
	if (rotatedGridXPos[lowerX][lowerY][upperZ] > max) {
		max = rotatedGridXPos[lowerX][lowerY][upperZ];
	}
	if (rotatedGridXPos[upperX][upperY][lowerZ] > max) {
		max = rotatedGridXPos[upperX][upperY][lowerZ];
	}
	if (rotatedGridXPos[upperX][lowerY][upperZ] > max) {
		max = rotatedGridXPos[upperX][lowerY][upperZ];
	}
	if (rotatedGridXPos[upperX][upperY][upperZ] > max) {
		max = rotatedGridXPos[upperX][upperY][upperZ];
	}
	if (rotatedGridXPos[lowerX][upperY][upperZ] > max) {
		max = rotatedGridXPos[lowerX][upperY][upperZ];
	}
	return max;
}

float findRotatedYmin(int lowerX, int lowerY, int lowerZ, int upperX, int upperY, int upperZ) {
	float min = rotatedGridYPos[lowerX][lowerY][lowerZ];
	if (rotatedGridYPos[upperX][lowerY][lowerZ] > min) {
		min = rotatedGridYPos[upperX][lowerY][lowerZ];
	}
	if (rotatedGridYPos[lowerX][upperY][lowerZ] > min) {
		min = rotatedGridYPos[lowerX][upperY][lowerZ];
	}
	if (rotatedGridYPos[lowerX][lowerY][upperZ] > min) {
		min = rotatedGridYPos[lowerX][lowerY][upperZ];
	}
	if (rotatedGridYPos[upperX][upperY][lowerZ] > min) {
		min = rotatedGridYPos[upperX][upperY][lowerZ];
	}
	if (rotatedGridYPos[upperX][lowerY][upperZ] > min) {
		min = rotatedGridYPos[upperX][lowerY][upperZ];
	}
	if (rotatedGridYPos[upperX][upperY][upperZ] > min) {
		min = rotatedGridYPos[upperX][upperY][upperZ];
	}
	if (rotatedGridYPos[lowerX][upperY][upperZ] > min) {
		min = rotatedGridYPos[lowerX][upperY][upperZ];
	}
	return min;
}

float findRotatedYmax(int lowerX, int lowerY, int lowerZ, int upperX, int upperY, int upperZ) {
	float max = rotatedGridYPos[lowerX][lowerY][lowerZ];
	if (rotatedGridYPos[upperX][lowerY][lowerZ] > max) {
		max = rotatedGridYPos[upperX][lowerY][lowerZ];
	}
	if (rotatedGridYPos[lowerX][upperY][lowerZ] > max) {
		max = rotatedGridYPos[lowerX][upperY][lowerZ];
	}
	if (rotatedGridYPos[lowerX][lowerY][upperZ] > max) {
		max = rotatedGridYPos[lowerX][lowerY][upperZ];
	}
	if (rotatedGridYPos[upperX][upperY][lowerZ] > max) {
		max = rotatedGridYPos[upperX][upperY][lowerZ];
	}
	if (rotatedGridYPos[upperX][lowerY][upperZ] > max) {
		max = rotatedGridYPos[upperX][lowerY][upperZ];
	}
	if (rotatedGridYPos[upperX][upperY][upperZ] > max) {
		max = rotatedGridYPos[upperX][upperY][upperZ];
	}
	if (rotatedGridYPos[lowerX][upperY][upperZ] > max) {
		max = rotatedGridYPos[lowerX][upperY][upperZ];
	}
	return max;
}

float findRotatedZmin(int lowerX, int lowerY, int lowerZ, int upperX, int upperY, int upperZ) {
	float min = rotatedGridZPos[0][0][0];
	if (rotatedGridZPos[MAXDEPTH+1][0][0] < min) {
		min = rotatedGridZPos[MAXDEPTH+1][0][0];
	}
	if (rotatedGridZPos[0][MAXDEPTH+1][0] < min) {
		min = rotatedGridZPos[0][MAXDEPTH+1][0];
	}
	if (rotatedGridZPos[0][0][MAXDEPTH+1] < min) {
		min = rotatedGridZPos[0][0][MAXDEPTH+1];
	}
	if (rotatedGridZPos[MAXDEPTH+1][MAXDEPTH+1][0] < min) {
		min = rotatedGridZPos[MAXDEPTH+1][MAXDEPTH+1][0];
	}
	if (rotatedGridZPos[MAXDEPTH+1][0][MAXDEPTH+1] < min) {
		min = rotatedGridZPos[MAXDEPTH+1][0][MAXDEPTH+1];
	}
	if (rotatedGridZPos[MAXDEPTH+1][MAXDEPTH+1][MAXDEPTH+1] < min) {
		min = rotatedGridZPos[MAXDEPTH+1][MAXDEPTH+1][MAXDEPTH+1];
	}
	if (rotatedGridZPos[0][MAXDEPTH+1][MAXDEPTH+1] < min) {
		min = rotatedGridZPos[0][MAXDEPTH+1][MAXDEPTH+1];
	}
	return min;
}

float findRotatedZmax(int lowerX, int lowerY, int lowerZ, int upperX, int upperY, int upperZ) {
	float max = rotatedGridZPos[0][0][0];
	if (rotatedGridZPos[MAXDEPTH+1][0][0] > max) {
		max = rotatedGridZPos[MAXDEPTH+1][0][0];
	}
	if (rotatedGridZPos[0][MAXDEPTH+1][0] > max) {
		max = rotatedGridZPos[0][MAXDEPTH+1][0];
	}
	if (rotatedGridZPos[0][0][MAXDEPTH+1] > max) {
		max = rotatedGridZPos[0][0][MAXDEPTH+1];
	}
	if (rotatedGridZPos[MAXDEPTH+1][MAXDEPTH+1][0] > max) {
		max = rotatedGridZPos[MAXDEPTH+1][MAXDEPTH+1][0];
	}
	if (rotatedGridZPos[MAXDEPTH+1][0][MAXDEPTH+1] > max) {
		max = rotatedGridZPos[MAXDEPTH+1][0][MAXDEPTH+1];
	}
	if (rotatedGridZPos[MAXDEPTH+1][MAXDEPTH+1][MAXDEPTH+1] > max) {
		max = rotatedGridZPos[MAXDEPTH+1][MAXDEPTH+1][MAXDEPTH+1];
	}
	if (rotatedGridZPos[0][MAXDEPTH+1][MAXDEPTH+1] > max) {
		max = rotatedGridZPos[0][MAXDEPTH+1][MAXDEPTH+1];
	}
	return max;
}

float findRotatedXminTwo(int lowerX, int lowerY, int lowerZ, int upperX, int upperY, int upperZ) {
	float min = rotatedGridXPosTwo[0][0][0];
	if (rotatedGridXPosTwo[MAXDEPTH+1][0][0] < min) {
		min = rotatedGridXPosTwo[MAXDEPTH+1][0][0];
	}
	if (rotatedGridXPosTwo[0][MAXDEPTH+1][0] < min) {
		min = rotatedGridXPosTwo[0][MAXDEPTH+1][0];
	}
	if (rotatedGridXPosTwo[0][0][MAXDEPTH+1] < min) {
		min = rotatedGridXPosTwo[0][0][MAXDEPTH+1];
	}
	if (rotatedGridXPosTwo[MAXDEPTH+1][MAXDEPTH+1][0] < min) {
		min = rotatedGridXPosTwo[MAXDEPTH+1][MAXDEPTH+1][0];
	}
	if (rotatedGridXPosTwo[MAXDEPTH+1][0][MAXDEPTH+1] < min) {
		min = rotatedGridXPosTwo[MAXDEPTH+1][0][MAXDEPTH+1];
	}
	if (rotatedGridXPosTwo[MAXDEPTH+1][MAXDEPTH+1][MAXDEPTH+1] < min) {
		min = rotatedGridXPosTwo[MAXDEPTH+1][MAXDEPTH+1][MAXDEPTH+1];
	}
	if (rotatedGridXPosTwo[0][MAXDEPTH+1][MAXDEPTH+1] < min) {
		min = rotatedGridXPosTwo[0][MAXDEPTH+1][MAXDEPTH+1];
	}
	return min;
}

float findRotatedXmaxTwo(int lowerX, int lowerY, int lowerZ, int upperX, int upperY, int upperZ) {
	float max = rotatedGridXPosTwo[0][0][0];
	if (rotatedGridXPosTwo[MAXDEPTH+1][0][0] > max) {
		max = rotatedGridXPosTwo[MAXDEPTH+1][0][0];
	}
	if (rotatedGridXPosTwo[0][MAXDEPTH+1][0] > max) {
		max = rotatedGridXPosTwo[0][MAXDEPTH+1][0];
	}
	if (rotatedGridXPosTwo[0][0][MAXDEPTH+1] > max) {
		max = rotatedGridXPosTwo[0][0][MAXDEPTH+1];
	}
	if (rotatedGridXPosTwo[MAXDEPTH+1][MAXDEPTH+1][0] > max) {
		max = rotatedGridXPosTwo[MAXDEPTH+1][MAXDEPTH+1][0];
	}
	if (rotatedGridXPosTwo[MAXDEPTH+1][0][MAXDEPTH+1] > max) {
		max = rotatedGridXPosTwo[MAXDEPTH+1][0][MAXDEPTH+1];
	}
	if (rotatedGridXPosTwo[MAXDEPTH+1][MAXDEPTH+1][MAXDEPTH+1] > max) {
		max = rotatedGridXPosTwo[MAXDEPTH+1][MAXDEPTH+1][MAXDEPTH+1];
	}
	if (rotatedGridXPosTwo[0][MAXDEPTH+1][MAXDEPTH+1] > max) {
		max = rotatedGridXPosTwo[0][MAXDEPTH+1][MAXDEPTH+1];
	}
	return max;
}

float findRotatedYminTwo(int lowerX, int lowerY, int lowerZ, int upperX, int upperY, int upperZ) {
	float min = rotatedGridYPosTwo[0][0][0];
	if (rotatedGridYPosTwo[MAXDEPTH+1][0][0] < min) {
		min = rotatedGridYPosTwo[MAXDEPTH+1][0][0];
	}
	if (rotatedGridYPosTwo[0][MAXDEPTH+1][0] < min) {
		min = rotatedGridYPosTwo[0][MAXDEPTH+1][0];
	}
	if (rotatedGridYPosTwo[0][0][MAXDEPTH+1] < min) {
		min = rotatedGridYPosTwo[0][0][MAXDEPTH+1];
	}
	if (rotatedGridYPosTwo[MAXDEPTH+1][MAXDEPTH+1][0] < min) {
		min = rotatedGridYPosTwo[MAXDEPTH+1][MAXDEPTH+1][0];
	}
	if (rotatedGridYPosTwo[MAXDEPTH+1][0][MAXDEPTH+1] < min) {
		min = rotatedGridYPosTwo[MAXDEPTH+1][0][MAXDEPTH+1];
	}
	if (rotatedGridYPosTwo[MAXDEPTH+1][MAXDEPTH+1][MAXDEPTH+1] < min) {
		min = rotatedGridYPosTwo[MAXDEPTH+1][MAXDEPTH+1][MAXDEPTH+1];
	}
	if (rotatedGridYPosTwo[0][MAXDEPTH+1][MAXDEPTH+1] < min) {
		min = rotatedGridYPosTwo[0][MAXDEPTH+1][MAXDEPTH+1];
	}
	return min;
}

float findRotatedYmaxTwo(int lowerX, int lowerY, int lowerZ, int upperX, int upperY, int upperZ) {
	float max = rotatedGridYPosTwo[0][0][0];
	if (rotatedGridYPosTwo[MAXDEPTH+1][0][0] > max) {
		max = rotatedGridYPosTwo[MAXDEPTH+1][0][0];
	}
	if (rotatedGridYPosTwo[0][MAXDEPTH+1][0] > max) {
		max = rotatedGridYPosTwo[0][MAXDEPTH+1][0];
	}
	if (rotatedGridYPosTwo[0][0][MAXDEPTH+1] > max) {
		max = rotatedGridYPosTwo[0][0][MAXDEPTH+1];
	}
	if (rotatedGridYPosTwo[MAXDEPTH+1][MAXDEPTH+1][0] > max) {
		max = rotatedGridYPosTwo[MAXDEPTH+1][MAXDEPTH+1][0];
	}
	if (rotatedGridYPosTwo[MAXDEPTH+1][0][MAXDEPTH+1] > max) {
		max = rotatedGridYPosTwo[MAXDEPTH+1][0][MAXDEPTH+1];
	}
	if (rotatedGridYPosTwo[MAXDEPTH+1][MAXDEPTH+1][MAXDEPTH+1] > max) {
		max = rotatedGridYPosTwo[MAXDEPTH+1][MAXDEPTH+1][MAXDEPTH+1];
	}
	if (rotatedGridYPosTwo[0][MAXDEPTH+1][MAXDEPTH+1] > max) {
		max = rotatedGridYPosTwo[0][MAXDEPTH+1][MAXDEPTH+1];
	}
	return max;
}

float findRotatedZminTwo(int lowerX, int lowerY, int lowerZ, int upperX, int upperY, int upperZ) {
	float min = rotatedGridZPosTwo[0][0][0];
	if (rotatedGridZPosTwo[MAXDEPTH+1][0][0] < min) {
		min = rotatedGridZPosTwo[MAXDEPTH+1][0][0];
	}
	if (rotatedGridZPosTwo[0][MAXDEPTH+1][0] < min) {
		min = rotatedGridZPosTwo[0][MAXDEPTH+1][0];
	}
	if (rotatedGridZPosTwo[0][0][MAXDEPTH+1] < min) {
		min = rotatedGridZPosTwo[0][0][MAXDEPTH+1];
	}
	if (rotatedGridZPosTwo[MAXDEPTH+1][MAXDEPTH+1][0] < min) {
		min = rotatedGridZPosTwo[MAXDEPTH+1][MAXDEPTH+1][0];
	}
	if (rotatedGridZPosTwo[MAXDEPTH+1][0][MAXDEPTH+1] < min) {
		min = rotatedGridZPosTwo[MAXDEPTH+1][0][MAXDEPTH+1];
	}
	if (rotatedGridZPosTwo[MAXDEPTH+1][MAXDEPTH+1][MAXDEPTH+1] < min) {
		min = rotatedGridZPosTwo[MAXDEPTH+1][MAXDEPTH+1][MAXDEPTH+1];
	}
	if (rotatedGridZPosTwo[0][MAXDEPTH+1][MAXDEPTH+1] < min) {
		min = rotatedGridZPosTwo[0][MAXDEPTH+1][MAXDEPTH+1];
	}
	return min;
}

float findRotatedZmaxTwo(int lowerX, int lowerY, int lowerZ, int upperX, int upperY, int upperZ) {
	float max = rotatedGridZPosTwo[0][0][0];
	if (rotatedGridZPosTwo[MAXDEPTH+1][0][0] > max) {
		max = rotatedGridZPosTwo[MAXDEPTH+1][0][0];
	}
	if (rotatedGridZPosTwo[0][MAXDEPTH+1][0] > max) {
		max = rotatedGridZPosTwo[0][MAXDEPTH+1][0];
	}
	if (rotatedGridZPosTwo[0][0][MAXDEPTH+1] > max) {
		max = rotatedGridZPosTwo[0][0][MAXDEPTH+1];
	}
	if (rotatedGridZPosTwo[MAXDEPTH+1][MAXDEPTH+1][0] > max) {
		max = rotatedGridZPosTwo[MAXDEPTH+1][MAXDEPTH+1][0];
	}
	if (rotatedGridZPosTwo[MAXDEPTH+1][0][MAXDEPTH+1] > max) {
		max = rotatedGridZPosTwo[MAXDEPTH+1][0][MAXDEPTH+1];
	}
	if (rotatedGridZPosTwo[MAXDEPTH+1][MAXDEPTH+1][MAXDEPTH+1] > max) {
		max = rotatedGridZPosTwo[MAXDEPTH+1][MAXDEPTH+1][MAXDEPTH+1];
	}
	if (rotatedGridZPosTwo[0][MAXDEPTH+1][MAXDEPTH+1] > max) {
		max = rotatedGridZPosTwo[0][MAXDEPTH+1][MAXDEPTH+1];
	}
	return max;
}
*/

/* DEPRECATED LARGE GRID CODE, USED TO PLACE BOTH MOLECULES WITHIN THE SAME GRID */
/*
//void getBigGridBoundaries(int id1, int id2) {
//	gridXmin = getAtomX(id1, 0) - getAtomRadius(id1, 0);
//	gridXmax = getAtomX(id1, 0) + getAtomRadius(id1, 0);
//	gridYmin = getAtomY(id1, 0) - getAtomRadius(id1, 0);
//	gridYmax = getAtomY(id1, 0) + getAtomRadius(id1, 0);
//	gridZmin = getAtomZ(id1, 0) - getAtomRadius(id1, 0);
//	gridZmax = getAtomZ(id1, 0) + getAtomRadius(id1, 0);
//	for (int i = 0; i < getProteinSize(id1); i++) {
//		if (getAtomX(id1, i) < gridXmin) {
//			gridXmin = getAtomX(id1, i) - getAtomRadius(id1, i);
//		}
//		else if (getAtomX(id1, i) > gridXmax) {
//			gridXmax = getAtomX(id1, i) + getAtomRadius(id1, i);
//		}
//		if (getAtomY(id1, i) < gridYmin) {
//			gridYmin = getAtomY(id1, i) - getAtomRadius(id1, i);
//		}
//		else if (getAtomY(id1, i) > gridYmax) {
//			gridYmax = getAtomY(id1, i) + getAtomRadius(id1, i);
//		}
//		if (getAtomZ(id1, i) < gridZmin) {
//			gridZmin = getAtomZ(id1, i) - getAtomRadius(id1, i);
//		}
//		else if (getAtomZ(id1, i) > gridZmax) {
//			gridZmax = getAtomZ(id1, i) + getAtomRadius(id1, i);
//		}
//	}
//	for (int i = 0; i < getProteinSize(id2); i++) {
//		if (getAtomX(id2, i) < gridXmin) {
//			gridXmin = getAtomX(id2, i) - getAtomRadius(id2, i);
//		}
//		else if (getAtomX(id2, i) > gridXmax) {
//			gridXmax = getAtomX(id2, i) + getAtomRadius(id2, i);
//		}
//		if (getAtomY(id2, i) < gridYmin) {
//			gridYmin = getAtomY(id2, i) - getAtomRadius(id2, i);
//		}
//		else if (getAtomY(id2, i) > gridYmax) {
//			gridYmax = getAtomY(id2, i) + getAtomRadius(id2, i);
//		}
//		if (getAtomZ(id2, i) < gridZmin) {
//			gridZmin = getAtomZ(id2, i) - getAtomRadius(id2, i);
//		}
//		else if (getAtomZ(id2, i) > gridZmax) {
//			gridZmax = getAtomZ(id2, i) + getAtomRadius(id2, i);
//		}
//	}
//}

//extern "C" __declspec(dllexport) bool __stdcall buildBigGrid(int id1, int id2) {
//	getBigGridBoundaries(id1, id2);
//	sizeX = abs(gridXmin) + abs(gridXmax);
//	sizeY = abs(gridYmin) + abs(gridYmax);
//	sizeZ = abs(gridZmin) + abs(gridZmax);
//	prevMaxSize = maxSize;
//	maxSize = max(max(sizeX, sizeY), sizeZ);
//	if (maxSize <= prevMaxSize) {
//		return false;
//	}
//	gridXcentre = (gridXmin + gridXmax) / 2;
//	gridYcentre = (gridYmin + gridYmax) / 2;
//	gridZcentre = (gridZmin + gridZmax) / 2;
//	quadSize = round(maxSize / MAXDEPTH);
//	int xIndex = 0;
//	int yIndex = 0;
//	int zIndex = 0;
//	for (int z = -(maxSize / 2); z<(maxSize / 2); z += quadSize) {
//		for (int y = -(maxSize / 2); y<(maxSize / 2); y += quadSize) {
//			for (int x = -(maxSize / 2); x<(maxSize / 2); x += quadSize) {
//				gridXPos[xIndex][yIndex][zIndex] = x + gridXcentre;
//				gridYPos[xIndex][yIndex][zIndex] = y + gridYcentre;
//				gridZPos[xIndex][yIndex][zIndex] = z + gridZcentre;
//				xIndex++;
//			}
//			xIndex = 0;
//			yIndex++;
//		}
//		xIndex = 0;
//		yIndex = 0;
//		zIndex++;
//	}
//	numCellsInAng = ceil(ANGSTROM / quadSize);
//	return true;
//}

//extern "C" __declspec(dllexport) void __stdcall populateBigGrid(int id1, int id2) {
//	//populate first grid
//	for (int x = 1; x < MAXDEPTH + 1; x++) {
//		for (int y = 1; y < MAXDEPTH + 1; y++) {
//			for (int z = 1; z < MAXDEPTH + 1; z++) {
//				populatedGrid[x - 1][y - 1][z - 1].clear();
//				for (int i = 0; i < getProteinSize(id1); i++) {
//					if (((getAtomX(id1, i) + getAtomRadius(id1, i)) >= gridXPos[x - 1][y - 1][z - 1]) && ((getAtomX(id1, i) - getAtomRadius(id1, i)) < gridXPos[x][y][z])) {
//						if (((getAtomY(id1, i) + getAtomRadius(id1, i)) >= gridYPos[x - 1][y - 1][z - 1]) && ((getAtomY(id1, i) - getAtomRadius(id1, i)) < gridYPos[x][y][z])) {
//							if (((getAtomZ(id1, i) + getAtomRadius(id1, i)) >= gridZPos[x - 1][y - 1][z - 1]) && ((getAtomZ(id1, i) - getAtomRadius(id1, i)) < gridZPos[x][y][z])) {
//								populatedGrid[x - 1][y - 1][z - 1].push_back(i);
//							}
//						}
//					}
//				}
//				for (int i = 0; i < getProteinSize(id2); i++) {
//					if (((getAtomX(id2, i) + getAtomRadius(id2, i)) >= gridXPos[x - 1][y - 1][z - 1]) && ((getAtomX(id2, i) - getAtomRadius(id2, i)) < gridXPos[x][y][z])) {
//						if (((getAtomY(id2, i) + getAtomRadius(id2, i)) >= gridYPos[x - 1][y - 1][z - 1]) && ((getAtomY(id2, i) - getAtomRadius(id2, i)) < gridYPos[x][y][z])) {
//							if (((getAtomZ(id2, i) + getAtomRadius(id2, i)) >= gridZPos[x - 1][y - 1][z - 1]) && ((getAtomZ(id2, i) - getAtomRadius(id2, i)) < gridZPos[x][y][z])) {
//								populatedGridTwo[x - 1][y - 1][z - 1].push_back(i);
//							}
//						}
//					}
//				}
//			}
//		}
//	}
//
//	//if (id1 != -1) {
//	//	//Clear previous population data
//	//		for (int x = 0; x < MAXDEPTH; x++) {
//	//			for (int y = 0; y < MAXDEPTH; y++) {
//	//				for (int z = 0; z < MAXDEPTH; z++) {
//	//					populatedGrid[x][y][z].clear();
//	//				}
//	//			}
//	//		}
//	//	//Populate grid
//	//	for (int i = 0; i < getProteinSize(id1); i++) {
//	//		//bool done = false;
//	//		int xEdge = 0;
//	//		int yEdge = 0;
//	//		int zEdge = 0;
//	//		int cellX = floor((getAtomX(id1, i) - gridXPos[0][0][0]) / quadSize);
//	//		int cellY = floor((getAtomY(id1, i) - gridYPos[0][0][0]) / quadSize);
//	//		int cellZ = floor((getAtomZ(id1, i) - gridZPos[0][0][0]) / quadSize);
//	//		populatedGrid[cellX][cellY][cellZ].push_back(i);
//	//		if ((cellX > 0) && ((getAtomX(id1, i) - getAtomRadius(id1, i)) < gridXPos[cellX][cellY][cellZ])) {
//	//			populatedGrid[cellX - 1][cellY][cellZ].push_back(i);
//	//			xEdge = -1;
//	//		}
//	//		else if ((cellX < MAXDEPTH + 1) && ((getAtomX(id1, i) + getAtomRadius(id1, i)) > gridXPos[cellX + 1][cellY][cellZ])) {
//	//			populatedGrid[cellX + 1][cellY][cellZ].push_back(i);
//	//			xEdge = 1;
//	//		}
//	//		if ((cellY > 0) && ((getAtomY(id1, i) - getAtomRadius(id1, i)) < gridYPos[cellX][cellY][cellZ])) {
//	//			populatedGrid[cellX][cellY - 1][cellZ].push_back(i);
//	//			yEdge = -1;
//	//		}
//	//		else if ((cellY < MAXDEPTH + 1) && ((getAtomY(id1, i) + getAtomRadius(id1, i)) > gridYPos[cellX][cellY + 1][cellZ])) {
//	//			populatedGrid[cellX][cellY + 1][cellZ].push_back(i);
//	//			yEdge = 1;
//	//		}
//	//		if ((cellZ > 0) && ((getAtomZ(id1, i) - getAtomRadius(id1, i)) < gridZPos[cellX][cellY][cellZ])) {
//	//			populatedGrid[cellX][cellY][cellZ - 1].push_back(i);
//	//			zEdge = -1;
//	//		}
//	//		else if ((cellZ < MAXDEPTH + 1) && ((getAtomZ(id1, i) + getAtomRadius(id1, i)) > gridZPos[cellX][cellY][cellZ + 1])) {
//	//			populatedGrid[cellX][cellY][cellZ + 1].push_back(i);
//	//			zEdge = 1;
//	//		}
//	//		//Infer non-adjacents from edge data gathered above
//	//		if ((xEdge != 0) && (yEdge != 0)) {
//	//			populatedGrid[cellX + xEdge][cellY + yEdge][cellZ].push_back(i);
//	//		}
//	//		if ((xEdge != 0) && (zEdge != 0)) {
//	//			populatedGrid[cellX + xEdge][cellY][cellZ + zEdge].push_back(i);
//	//		}
//	//		if ((yEdge != 0) && (zEdge != 0)) {
//	//			populatedGrid[cellX][cellY + yEdge][cellZ + zEdge].push_back(i);
//	//		}
//	//		if ((xEdge != 0) && (yEdge != 0) && (zEdge != 0)) {
//	//			populatedGrid[cellX + xEdge][cellY + yEdge][cellZ + zEdge].push_back(i);
//	//		}
//	//	}
//	//}
//	//if (id2 != -1) {
//	//	//Clear previous population data
//	//	for (int x = 0; x < MAXDEPTH; x++) {
//	//		for (int y = 0; y < MAXDEPTH; y++) {
//	//			for (int z = 0; z < MAXDEPTH; z++) {
//	//				populatedGridTwo[x][y][z].clear();
//	//			}
//	//		}
//	//	}
//	//	//Populate grid
//	//	for (int i = 0; i < getProteinSize(id2); i++) {
//	//		//bool done = false;
//	//		int xEdge = 0;
//	//		int yEdge = 0;
//	//		int zEdge = 0;
//	//		int cellX = floor((getAtomX(id2, i) - gridXPosTwo[0][0][0]) / quadSizeTwo);
//	//		int cellY = floor((getAtomY(id2, i) - gridYPosTwo[0][0][0]) / quadSizeTwo);
//	//		int cellZ = floor((getAtomZ(id2, i) - gridZPosTwo[0][0][0]) / quadSizeTwo);
//	//		populatedGridTwo[cellX][cellY][cellZ].push_back(i);
//	//		if ((cellX > 0) && ((getAtomX(id2, i) - getAtomRadius(id2, i)) < gridXPosTwo[cellX][cellY][cellZ])) {
//	//			populatedGridTwo[cellX - 1][cellY][cellZ].push_back(i);
//	//			xEdge = -1;
//	//		}
//	//		else if ((cellX < MAXDEPTH + 1) && ((getAtomX(id2, i) + getAtomRadius(id2, i)) > gridXPosTwo[cellX + 1][cellY][cellZ])) {
//	//			populatedGrid[cellX + 1][cellY][cellZ].push_back(i);
//	//			xEdge = 1;
//	//		}
//	//		if ((cellY > 0) && ((getAtomY(id2, i) - getAtomRadius(id2, i)) < gridYPosTwo[cellX][cellY][cellZ])) {
//	//			populatedGridTwo[cellX][cellY - 1][cellZ].push_back(i);
//	//			yEdge = -1;
//	//		}
//	//		else if ((cellY < MAXDEPTH + 1) && ((getAtomY(id2, i) + getAtomRadius(id2, i)) > gridYPosTwo[cellX][cellY + 1][cellZ])) {
//	//			populatedGridTwo[cellX][cellY + 1][cellZ].push_back(i);
//	//			yEdge = 1;
//	//		}
//	//		if ((cellZ > 0) && ((getAtomZ(id2, i) - getAtomRadius(id2, i)) < gridZPosTwo[cellX][cellY][cellZ])) {
//	//			populatedGridTwo[cellX][cellY][cellZ - 1].push_back(i);
//	//			zEdge = -1;
//	//		}
//	//		else if ((cellZ < MAXDEPTH + 1) && ((getAtomZ(id2, i) + getAtomRadius(id2, i)) > gridZPosTwo[cellX][cellY][cellZ + 1])) {
//	//			populatedGridTwo[cellX][cellY][cellZ + 1].push_back(i);
//	//			zEdge = 1;
//	//		}
//	//		//Infer non-adjacents from edge data gathered above
//	//		if ((xEdge != 0) && (yEdge != 0)) {
//	//			populatedGridTwo[cellX + xEdge][cellY + yEdge][cellZ].push_back(i);
//	//		}
//	//		if ((xEdge != 0) && (zEdge != 0)) {
//	//			populatedGridTwo[cellX + xEdge][cellY][cellZ + zEdge].push_back(i);
//	//		}
//	//		if ((yEdge != 0) && (zEdge != 0)) {
//	//			populatedGridTwo[cellX][cellY + yEdge][cellZ + zEdge].push_back(i);
//	//		}
//	//		if ((xEdge != 0) && (yEdge != 0) && (zEdge != 0)) {
//	//			populatedGridTwo[cellX + xEdge][cellY + yEdge][cellZ + zEdge].push_back(i);
//	//		}
//	//	}
//	//}
//}

//extern "C" __declspec(dllexport) void __stdcall findBigCellCollisions() {
//	//Ensures cells aren't registered for collisions multiple times
//	bool hasCollided1[MAXDEPTH][MAXDEPTH][MAXDEPTH];
//	bool hasCollided2[MAXDEPTH][MAXDEPTH][MAXDEPTH];
//	//Initialise collision matrices
//	for (int i = 0; i < MAXDEPTH; i++) {
//		for (int j = 0; j < MAXDEPTH; j++) {
//			for (int k = 0; k < MAXDEPTH; k++) {
//				hasCollided1[i][j][k] = false;
//				hasCollided2[i][j][k] = false;
//			}
//		}
//	}
//	//Find collisions
//	for (int i = 0; i < MAXDEPTH; i++) {
//		for (int j = 0; j < MAXDEPTH; j++) {
//			for (int k = 0; k < MAXDEPTH; k++) {
//				if (!populatedGrid[i][j][k].empty()) {
//					for (int l = 0; l < MAXDEPTH; l++) {
//						if (abs(l - i) <= numCellsInAng) {
//							for (int m = 0; m < MAXDEPTH; m++) {
//								if (abs(j - m) <= numCellsInAng) {
//									for (int n = 0; n < MAXDEPTH; n++) {
//										if (abs(n - k) <= numCellsInAng) {
//											if (!populatedGridTwo[l][m][n].empty()) {
//												if (!hasCollided1[i][j][k]) {
//													for (int e = 0; e < populatedGrid[i][j][k].size(); e++) {
//														setCollision(1, populatedGrid[i][j][k].at(e));
//													}
//													hasCollided1[i][j][k] = true;
//												}
//												if (!hasCollided2[l][m][n]) {
//													for (int p = 0; p < populatedGridTwo[l][m][n].size(); p++) {
//														setCollision(2, populatedGridTwo[l][m][n].at(p));
//													}
//													hasCollided2[l][m][n] = true;
//												}
//											}
//										}
//									}
//								}
//							}
//						}
//					}
//				}
//			}
//		}
//	}
//	delete hasCollided1;
//	delete hasCollided2;
//}
*/

/* FURTHER DEPRECATED CODE */
//extern "C" __declspec(dllexport) void __stdcall findOverlap(int t1x, int t1y, int t1z, int t2x, int t2y, int t2z){
//	//collidingAtoms.clear();
//	//get grid cells that the other bb overlaps
//	for(int x1 = 0; x1 < MAXDEPTH; x1++){
//		for(int y1 = 0; y1 < MAXDEPTH; y1++){
//			for(int z1 = 0; z1 < MAXDEPTH; z1++){
//				//if(((x1*quadSize)+gridXmin <= otherXmax) && ((x1*quadSize)+quadSize+gridXmin >= otherXmin) &&
//				//   ((y1*quadSize)+gridYmin <= otherYmax) && ((y1*quadSize)+quadSize+gridYmin >= otherYmin) &&
//				//   ((z1*quadSize)+gridZmin <= otherZmax) && ((z1*quadSize)+quadSize+gridZmin >= otherZmin)){
//				if(gridXPos[x1][y1][z1]+quadSize+t2x >= gridXminTwo+t1x && gridXPos[x1][y1][z1]+t2x <= gridXmaxTwo+t1x &&
//					gridYPos[x1][y1][z1]+quadSize+t2y >= gridYminTwo+t1y && gridYPos[x1][y1][z1]+t2y <= gridYmaxTwo+t1y &&
//					gridZPos[x1][y1][z1]+quadSize+t2z >= gridZminTwo+t1z && gridZPos[x1][y1][z1]+t2z <= gridZmaxTwo+t1z){
//					   for(int e = 0; e < populatedGrid[x1][y1][z1].size(); e++){
//						   setCollision(2, populatedGrid[x1][y1][z1].at(e));
//						   //collidingAtoms.push_back(populatedGrid[x1][y1][z1].at(e));
//					   }
//				}
//			}
//		}
//	}
//	//collidingAtoms.sort();
//	//collidingAtoms.unique();
//}
//
//extern "C" __declspec(dllexport) int __stdcall numberInGridCell(int x, int y, int z){
//	return populatedGrid[x][y][z].size();
//}
//
//extern "C" __declspec(dllexport) int __stdcall getAtomIDfromCell(int x, int y, int z, int i){
//	return populatedGrid[x][y][z].at(i);
//}
//
//extern "C" __declspec(dllexport) int __stdcall totalColliding(void){
//	return collidingAtoms.size();
//}
//
//extern "C" __declspec(dllexport) int __stdcall getColliding(){
//	int atomID = collidingAtoms.back();
//    collidingAtoms.pop_back();
//	return atomID;
//}
//
//extern "C" __declspec(dllexport) int __stdcall testFunction(void){
//	return getAtomX(1,1);
//}

#ifdef __cplusplus
}
#endif
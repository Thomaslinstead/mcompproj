#include "AtomSource.h"

//Atom Data Variables
AtomList list1;
AtomList list2;
Atom** list1Ptr;
Atom** list2Ptr;
int listSize = 0;
int listSize2 = 0;

float trans1x = 0, trans1y = 0, trans1z = 0, trans2x = 0, trans2y = 0, trans2z = 0; //Global Translations of Proteins

//Force Collision Variables
Vector3d totalForce;              //Total force vector between molecules
float paramA = 97022500, paramB = 6068.4; //Force calculation parameters
bool inCollision = false;         //Molecule is in collision or not
bool drawCollision = true;        //Colour colliding atoms
double ang = 1000000;             //Angstrom Conversion 
bool drawHydro = false;			  //Render Hyrdrogen?~
float rotMat1[16] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }; //Rotation Matrix for protein 1
float rotMat2[16] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }; //Rotation Matrix for protein 2

///////////////////////* Protein Loading Methods *////////////////////////////////////

/** Initialisation new protein for file data loading
*
* @param id - Which Protein to load
* @param p1 - Protein File to load
*/
bool __stdcall loadAtom(int id, char* p1)
{
	FileHandle file_handle;
	try{
		file_handle.openFile(p1);
	}
	catch (exception e) {
		return false;
	}
	if (id == 1){
		list1 = file_handle.getAtomList();
		list1Ptr = &list1[0];
		listSize = list1.size();
		return true;
	}

	if (id == 2){
		list2 = file_handle.getAtomList();
		list2Ptr = &list2[0];
		listSize2 = list2.size();
		return true;
	}
	return false;
}

/** Initialisation function for file data loading (Leave both as empty to load default files)
*
* @param p1 - Protein File to load
* @param p2 - Other Protein File to load
*/
bool __stdcall init(char* p1, char* p2)
{
	FileHandle file_handle;
	FileHandle file_handle2;

	try{
		if (p1 == ""){
			loadAtom(1, "PDB Test Files\\1CRN(327).pdb");
		}
		else {
			loadAtom(1, p1);
		}
		if (p2 == ""){
			loadAtom(2, "PDB Test Files\\5ADH(3127).pdb");
		}
		else{
			loadAtom(2, p2);
		}
	}
	catch (exception e) {
		return false;
	}
	return true;
}

/////////////////////////////////////////////////////////////////////////////////////

////////////////////////////* Getter Methods *///////////////////////////////////////

/** Function to get the list size of protein identified by id
*
* @param id - Which Protein to load
* @return Integer of list size, -1 if not a valid id
*/
int __stdcall getProteinSize(int id){
	if (id == 1)
		return listSize;
	if (id == 2)
		return listSize2;
	return -1;
}

/** Function to get the X position of an atom
*
* @param id - Which molecule to load (1 or 2)
* @param i - Which atom to load
* @return Float of X position
*/
float __stdcall getAtomX(int id, int i){
	Atom* temp;
	float px = 0;
	if (id == 1){
		temp = list1Ptr[i];
		px = (rotMat1[0] * temp->getMyLocalPosition().x + rotMat1[4] * temp->getMyLocalPosition().y + rotMat1[8] * temp->getMyLocalPosition().z) + trans1x;
	}
	if (id == 2){
		temp = list2Ptr[i];
		px = (rotMat2[0] * temp->getMyLocalPosition().x + rotMat2[4] * temp->getMyLocalPosition().y + rotMat2[8] * temp->getMyLocalPosition().z) + trans2x;
	}


	return px;
}

/** Function to get the Y position of an atom
*
* @param id - Which molecule to load (1 or 2)
* @param i - Which atom to load
* @return Float of Y position
*/
float __stdcall getAtomY(int id, int i){
	Atom* temp;
	float py = 0;
	if (id == 1){
		temp = list1Ptr[i];
		py = (rotMat1[1] * temp->getMyLocalPosition().x + rotMat1[5] * temp->getMyLocalPosition().y + rotMat1[9] * temp->getMyLocalPosition().z) + trans1y;
	}
	if (id == 2){
		temp = list2Ptr[i];
		py = (rotMat2[1] * temp->getMyLocalPosition().x + rotMat2[5] * temp->getMyLocalPosition().y + rotMat2[9] * temp->getMyLocalPosition().z) + trans2y;
	}
	return py;
}

/** Function to get the Z position of an atom
*
* @param id - Which molecule to load (1 or 2)
* @param i - Which atom to load
* @return Float of Z position
*/
float __stdcall getAtomZ(int id, int i){
	Atom* temp;
	float pz = 0;
	if (id == 1){
		temp = list1Ptr[i];
		pz = (rotMat1[2] * temp->getMyLocalPosition().x + rotMat1[6] * temp->getMyLocalPosition().y + rotMat1[10] * temp->getMyLocalPosition().z) + trans1z;
	}
	if (id == 2){
		temp = list2Ptr[i];
		pz = (rotMat2[2] * temp->getMyLocalPosition().x + rotMat2[6] * temp->getMyLocalPosition().y + rotMat2[10] * temp->getMyLocalPosition().z) + trans2z;
	}
	return pz;
}

/** Function to get the R Colour Value of an atom
*
* @param id - Which molecule to load (1 or 2)
* @param i - Which atom to load
* @return Float of R value
*/
float __stdcall getAtomR(int id, int i){
	Atom* temp;
	if (id == 1)
		temp = list1Ptr[i];
	if (id == 2)
		temp = list2Ptr[i];
	if (temp->inContact == true && drawCollision == true) return 0.0;
	Vector3d colour = temp->getMyColour();
	return colour.x;
}

/** Function to get the G Colour Value of an atom
*
* @param id - Which molecule to load (1 or 2)
* @param i - Which atom to load
* @return Float of G value
*/
float __stdcall getAtomG(int id, int i){
	Atom* temp;
	if (id == 1)
		temp = list1Ptr[i];
	if (id == 2)
		temp = list2Ptr[i];
	if (temp->inContact == true && drawCollision == true) return 255;
	Vector3d colour = temp->getMyColour();
	return colour.y;
}

/** Function to get the B Colour Value of an atom
*
* @param id - Which molecule to load (1 or 2)
* @param i - Which atom to load
* @return Float of B value
*/
float __stdcall getAtomB(int id, int i){
	Atom* temp;
	if (id == 1)
		temp = list1Ptr[i];
	if (id == 2)
		temp = list2Ptr[i];
	if (temp->inContact == true && drawCollision == true) return 0.0;
	Vector3d colour = temp->getMyColour();
	return colour.z;
}

/** Function to get the Radius Value of an atom
*
* @param id - Which molecule to load (1 or 2)
* @param i - Which atom to load
* @return Float of Radius value
*/
float __stdcall getAtomRadius(int id, int i){
	Atom* temp;
	if (id == 1)
		temp = list1Ptr[i];
	if (id == 2)
		temp = list2Ptr[i];
	return temp->getRadius();
}

/** Function to get the force vector X component 
* @return Float of value
*/
float __stdcall getForceVectorX(){
	return totalForce.x;
}

/** Function to get the force vector Y component
* @return Float of value
*/
float __stdcall getForceVectorY(){
	return totalForce.y;
}

/** Function to get the force vector Z component
* @return Float of value
*/
float __stdcall getForceVectorZ(){
	return totalForce.z;
}

/** Function to get if in collision
* @return Boolean colliding
*/
bool __stdcall moleculeColliding(){
	return inCollision;
}

/** Get if atom specific atom should be rendered
*
* @param id - Which molecule to load (1 or 2)
* @param i - Which atom to load
* @return Boolean should render atom
*/
bool __stdcall canRender(int id, int i)
{
	Atom* temp;
	if (id == 1)
		temp = list1Ptr[i];
	if (id == 2)
		temp = list2Ptr[i];

	if ((!temp->isWater()) && (temp->getEle().compare("H") != 0)) //not water or hydrogen
	{
		return true;
	}
	else if ((temp->getEle().compare("H") == 0) && drawHydro) //can add hydrogen here!
	{
		return true;
	}

	return false;
}

/////////////////////////////////////////////////////////////////////////////////////

////////////////////////////* Parameter Setter Methods */////////////////////////////

/* Set atom in */
void __stdcall setCollision(int id, int i){
	Atom* temp;
	if (id == 1)
		temp = list1Ptr[i];
	if (id == 2)
		temp = list2Ptr[i];
	temp->setCollision(true);
}

/* Set atom in collision with grid */
void __stdcall setGridContact(int id, int i) {
	Atom* temp;
	if (id == 1)
		temp = list1Ptr[i];
	if (id == 2)
		temp = list2Ptr[i];
	temp->setGridCollision(true);
}

/* Set force calculation parameter A */
void __stdcall setParamA(float A){
	paramA = A;
}

/* Set force calculation parameter B */
void __stdcall setParamB(float B){
	paramB = B;
}

/* Set a value in one of this classes matrices */
void __stdcall setMatrix(int whichMat, int whichVal, float val) {
	if (whichMat == 1) {
		rotMat1[whichVal] = val;
	}
	else {
		rotMat2[whichVal] = val;
	}
}

/* Set atom to be recoloured */
void __stdcall recolourAtom(int r, int g, int b, int id, int i){
	Atom* temp;
	if (id == 1)
		temp = list1Ptr[i];
	if (id == 2)
		temp = list2Ptr[i];
	temp->setRecoloured(true);
	temp->setColour(r, g, b);
}

/* Update the protein translations */
void __stdcall applyTranslation(int proteinID, float x, float y, float z){
	if (proteinID == 1){
		trans1x = x;
		trans1y = y;
		trans1z = z;
	}
	if (proteinID == 2){
		trans2x = x;
		trans2y = y;
		trans2z = z;
	}
}

/* Set whether collisions should be recoloured */
void __stdcall highlightCollisions(bool r){
	drawCollision = r;
}

/* Set whether hydrogens and other surrounding materials should be rendered */
void __stdcall renderHydrogen(bool f){
	drawHydro = f;
}

/////////////////////////////////////////////////////////////////////////////////////

////////////////////////////* Reset Methods *////////////////////////////////////////

/* Reset current force value */
void __stdcall resetForceValue(){
	totalForce = Vector3d(0, 0, 0);
	inCollision = false;
}

/* Reset current colliding status for atom */
void __stdcall resetCollision(int id, int i){
	Atom* temp;
	if (id == 1)
		temp = list1Ptr[i];
	if (id == 2)
		temp = list2Ptr[i];
	temp->setCollision(false);
	temp->setGridCollision(false);
}

/* Reset coloured atoms to original colour */
void __stdcall resetAtomColour(int id, int i){
	Atom* temp;
	if (id == 1)
		temp = list1Ptr[i];
	if (id == 2)
		temp = list2Ptr[i];
	temp->setRecoloured(false);
}

/* Cleanup method for when loading new proteins */
bool __stdcall ReleaseMemory(int id)
{
	if (id == 1) {
		list1.clear();
		delete &list1;
		return true;
	}

	if (id == 2){
		list2.clear();
		delete &list2;
		return true;
	}

	return false;
}


/////////////////////////////////////////////////////////////////////////////////////

////////////////////////////* Force Collision Methods *//////////////////////////////


/** Function check for collisions and calculate the force for a single pair of atoms
*
* @param atom1 - id of atom from protein 1
* @param paramB - id of atom from protein 2
*/
void __stdcall atomCollisionID(int atom1, int atom2){
	//Get the Local Coordinates
	Atom* temp = list1Ptr[atom1];
	Atom* temp2 = list2Ptr[atom2];

	//Get position with translations and rotations.
	float p1x = getAtomX(1, atom1), p1y = getAtomY(1, atom1), p1z = getAtomZ(1, atom1);
	float p2x = getAtomX(2, atom2), p2y = getAtomY(2, atom2), p2z = getAtomZ(2, atom2);

	//Check For Collision
	float d = sqrt(((p1x - p2x)*(p1x - p2x)) + ((p1y - p2y)*(p1y - p2y)) + ((p1z - p2z)*(p1z - p2z)));
	float r1 = 40; //80 Angstroms Radius that the force applies.
	float r2 = 40;
	if (d <= r1 + r2)
	{
		//Check for hard collision
		if (d < temp->getRadius() + temp2->getRadius())
		{
			temp->setCollision(true);
			temp2->setCollision(true);
		}

		///////////////***  Calculate the Force Feedback ***//////////////

		//Firstly find the vector between atoms B-A 
		Vector3d v = Vector3d((p1x - p2x)*0.1, (p1y - p2y)*0.1, (p1z - p2z)*0.1);
		//Then calculate the Magnitude = |m|
		float m = sqrt((v.x * v.x) + (v.y * v.y) + (v.z * v.z));

		//Get the Unit Vector (Rij^) U^ = V/|m|
		Vector3d u = Vector3d(v.x / m, v.y / m, v.z / m);


		//Calculate the force equation (Rij is the magnitude m)
		float leftsidenum = 12 * (paramA / pow(m, 13)); //Left Side of equation
		float rightsidenum = 6 * (paramB / pow(m, 7)); //Right Side of equation
		float r = leftsidenum - rightsidenum; //Main part of equation result
		Vector3d molecularForce = Vector3d(u.x*r*-1, u.y*r*-1, u.z*r*-1); //     multiVecByValue((leftsidenum-rightsidenum), u); //Final molecule force

		//Add the force to the TotalForce
		totalForce = totalForce + molecularForce;

		////////////////////////////////////////////////////////////////////
	}
}

/** Function check for collisions and calculate the total force
*
* @param paramA - Constant A for force equation
* @param paramB - Constant B for force equation
* @param x...z2 - Translations of two proteins  (Deprecated)
*/
int __stdcall BruteForceCollision(float paramA, float paramB, float x, float y, float z, float x2, float y2, float z2)
{
	totalForce = Vector3d(0, 0, 0);
	int coll = 0;
	for (int i = 0; i < listSize; i++)
	{
		if (canRender(1, i) || drawHydro){
			for (int j = 0; j < listSize2; j++)
			{
				if (canRender(2, j) || drawHydro){
					//Get the Local Coordinates
					Atom* temp = list1Ptr[i];
					Atom* temp2 = list2Ptr[j];

					//Get position with translations and rotations.
					float p1x = getAtomX(1, i), p1y = getAtomY(1, i), p1z = getAtomZ(1, i);
					float p2x = getAtomX(2, j), p2y = getAtomY(2, j), p2z = getAtomZ(2, j);

					//Check For Collision
					float d = sqrt(((p1x - p2x)*(p1x - p2x)) + ((p1y - p2y)*(p1y - p2y)) + ((p1z - p2z)*(p1z - p2z)));
					float r1 = 40; //80 Angstroms Radius that the force applies.
					float r2 = 40;
					if (d <= r1 + r2)
					{
						//Check for hard collision
						if (d < temp->getRadius() + temp2->getRadius())
						{
							temp->setCollision(true);
							temp2->setCollision(true);
							coll++;
						}

						///////////////***  Calculate the Force Feedback ***//////////////

						//Firstly find the vector between atoms B-A 
						Vector3d v = Vector3d((p1x - p2x)*0.1, (p1y - p2y)*0.1, (p1z - p2z)*0.1);
						//Then calculate the Magnitude = |m|
						float m = sqrt((v.x * v.x) + (v.y * v.y) + (v.z * v.z));

						//Get the Unit Vector (Rij^) U^ = V/|m|
						Vector3d u = Vector3d(v.x / m, v.y / m, v.z / m);


						//Calculate the force equation (Rij is the magnitude m)
						float leftsidenum = 12 * (paramA / pow(m, 13)); //Left Side of equation
						float rightsidenum = 6 * (paramB / pow(m, 7)); //Right Side of equation
						float r = leftsidenum - rightsidenum; //Main part of equation result
						Vector3d molecularForce = Vector3d(u.x*r*-1, u.y*r*-1, u.z*r*-1); //     multiVecByValue((leftsidenum-rightsidenum), u); //Final molecule force

						//Add the force to the TotalForce
						totalForce = totalForce + molecularForce;

						////////////////////////////////////////////////////////////////////
					}
				}
			}
		}
	}
	return coll;
}

/** Function check for collisions and calculate the total force with the Regular Grid data structure.
*
* @param x...z2 - Translations of two proteins (Deprecated)
*/
int __stdcall calculateForce(float x, float y, float z, float x2, float y2, float z2){
	totalForce = Vector3d(0, 0, 0);
	int col2 = 0;
	for (int i = 0; i < listSize; i++)
	{
		Atom* temp = list1Ptr[i];
		if (temp->inGridContact == true && ((canRender(1, i) || drawHydro))){ //Colliding with grid and set to render.
			for (int j = 0; j < listSize2; j++)
			{
				//Get the Local Coordinates
				Atom* temp2 = list2Ptr[j];
				if (temp2->inGridContact == true && ((canRender(2, j) || drawHydro))){

					float p1x = getAtomX(1, i), p1y = getAtomY(1, i), p1z = getAtomZ(1, i);
					float p2x = getAtomX(2, j), p2y = getAtomY(2, j), p2z = getAtomZ(2, j);

					//Check For Collision within 8 angstroms
					float d = sqrt(((p1x - p2x)*(p1x - p2x)) + ((p1y - p2y)*(p1y - p2y)) + ((p1z - p2z)*(p1z - p2z)));
					float r1 = 40; //80 Angstroms Radius that the force applies (each has 40 radius so distance between centers is 80).
					float r2 = 40;
					if (d <= r1 + r2)
					{
						//Check for hard collision
						if (d <= temp->getRadius() + temp2->getRadius())
						{
							temp->setCollision(true);
							temp2->setCollision(true);
							col2++;
						}

						///////////////***  Calculate the Force Feedback ***//////////////

						//Firstly find the vector between atoms B-A 
						Vector3d v = Vector3d((p1x - p2x)*0.1, (p1y - p2y)*0.1, (p1z - p2z)*0.1);
						//Then calculate the Magnitude = |m|
						float m = sqrt((v.x * v.x) + (v.y * v.y) + (v.z * v.z));

						//Get the Unit Vector (Rij^) U^ = V/|m|
						Vector3d u = Vector3d(v.x / m, v.y / m, v.z / m);


						//Calculate the force equation (Rij is the magnitude m)
						float leftsidenum = 12 * (paramA / pow(m, 13)); //Left Side of equation
						float rightsidenum = 6 * (paramB / pow(m, 7)); //Right Side of equation
						float r = leftsidenum - rightsidenum; //Main part of equation result
						Vector3d molecularForce = Vector3d(u.x*r*-1, u.y*r*-1, u.z*r*-1); //     multiVecByValue((leftsidenum-rightsidenum), u); //Final molecule force

						//Add the force to the TotalForce
						totalForce = totalForce + molecularForce;

						////////////////////////////////////////////////////////////////////

					}
				}
			}
		}
	}
	return col2;
}

////////////////////////////////////////////////////////////////////////////////
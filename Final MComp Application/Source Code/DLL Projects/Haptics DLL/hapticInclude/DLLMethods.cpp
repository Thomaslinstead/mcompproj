#include "OpenHaptics.h"

#ifdef __cplusplus    // If used by C++ code, 
extern "C" {          // we need to export the C interface
#endif

Haptics * hapticDevice;


	//DLL Methods
	extern "C" __declspec(dllexport) void __stdcall initHaptics(){
		//set up haptics
		hapticDevice = new Haptics();

		//initialise haptics
		hapticDevice->initHD();

		//set forces to true - no forces returned when false
		hapticDevice->setForces(true);
	}

	extern "C" __declspec(dllexport) float __stdcall getForceXHaptics() {
		return hapticDevice->getForceX();
	}
	extern "C" __declspec(dllexport) float __stdcall getForceYHaptics() {
		return hapticDevice->getForceY();
	}
	extern "C" __declspec(dllexport) float __stdcall getForceZHaptics() {
		return hapticDevice->getForceZ();
	}
	extern "C" __declspec(dllexport) void __stdcall setForceXHaptics(float x) {
		hapticDevice->setForceX(x);
	}
	extern "C" __declspec(dllexport) void __stdcall setForceYHaptics(float y) {
		hapticDevice->setForceY(y);
	}
	extern "C" __declspec(dllexport) void __stdcall setForceZHaptics(float z) {
		hapticDevice->setForceZ(z);
	}
	extern "C" __declspec(dllexport) float __stdcall getPositionXHaptics() {
		return hapticDevice->getPositionX();
	}
	extern "C" __declspec(dllexport) float __stdcall getPositionYHaptics() {
		return hapticDevice->getPositionY();
	}
	extern "C" __declspec(dllexport) float __stdcall getPositionZHaptics() {
		return hapticDevice->getPositionZ();
	}
	extern "C" __declspec(dllexport) void __stdcall enableForcesHaptics(bool active) {
		hapticDevice->setForces(active);
	}
	extern "C" __declspec(dllexport) int __stdcall getBottomButtonDownHaptics() {
		return hapticDevice->getBottomButton();
	}
	extern "C" __declspec(dllexport) int __stdcall getTopButtonDownHaptics() {
		return hapticDevice->getTopButton();
	}


#ifdef __cplusplus
}
#endif
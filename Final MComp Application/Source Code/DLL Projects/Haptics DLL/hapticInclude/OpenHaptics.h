

#ifndef _OPEN_HAPTICS_H
#define _OPEN_HAPTICS_H

#include <HD/hd.h>
#include <HDU/hdu.h>
#include <HDU/hduVector.h>
#include <gl/glut.h>

#include "HapticDisplayState.h"

class Haptics
{
private:
	HHD ghHD;
	HDSchedulerHandle hUpdateDeviceCallback;

	HapticDisplayState* pState;

public:
	bool COLLISION_FORCEFEEDBACK;
	
	Haptics();
	void obtainHapticState();
	void loadDevices();
	void touchTool();
	
	bool toggleForces();
	double* getTransform() { return pState->transform;}

	//DLL Methods
	void initHD();
	void setForces(bool c);
	 float  getForceX();
	 float  getForceY();
	 float  getForceZ();
	 void  setForceX(float x);
	 void  setForceY(float y);
	 void  setForceZ(float z);
	 float  getPositionX();
	 float  getPositionY();
	 float  getPositionZ();
	 int getTopButton();
	 int getBottomButton();

};

//OpenHaptics callback functions
HDCallbackCode HDCALLBACK touchMesh(void *pUserData);
HDCallbackCode HDCALLBACK copyHapticDisplayState(void *pUserData);

#endif _OPEN_HAPTICS_H
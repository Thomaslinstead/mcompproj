﻿using System;

namespace Molecular_Docking_Application
{

    // Structures for vectors and matrices

    public struct Vector2x1
    {
        private float x1y1,
                      x1y2;

        public Vector2x1(float X,
                         float Y)
        {
            x1y1 = X;
            x1y2 = Y;
        }

        public float X
        {
            get { return x1y1; }
            set { x1y1 = value; }
        }

        public float Y
        {
            get { return x1y2; }
            set { x1y2 = value; }
        }
    }

    public struct Vector3x1
    {
        private float x1y1,
                      x1y2,
                      x1y3;

        public Vector3x1(float X,
                         float Y,
                         float Z)
        {
            x1y1 = X;
            x1y2 = Y;
            x1y3 = Z;
        }

        public float X
        {
            get { return x1y1; }
            set { x1y1 = value; }
        }

        public float Y
        {
            get { return x1y2; }
            set { x1y2 = value; }
        }

        public float Z
        {
            get { return x1y3; }
            set { x1y3 = value; }
        }
    }

    public struct Vector4x1
    {
        private float x1y1,
                      x1y2,
                      x1y3,
                      x1y4;

        public Vector4x1(float X,
                         float Y,
                         float Z,
                         float W)
        {
            x1y1 = X; x1y2 = Y; x1y3 = Z; x1y4 = W;
        }

        public float X
        {
            get { return x1y1; }
            set { x1y1 = value; }
        }

        public float Y
        {
            get { return x1y2; }
            set { x1y2 = value; }
        }

        public float Z
        {
            get { return x1y3; }
            set { x1y3 = value; }
        }

        public float W
        {
            get { return x1y4; }
            set { x1y4 = value; }
        }
    }

    public struct Matrix3x3
    {
        private float x1y1, x2y1, x3y1,
                      x1y2, x2y2, x3y2,
                      x1y3, x2y3, x3y3;

        public Matrix3x3(float R1C1, float R1C2, float R1C3,
                         float R2C1, float R2C2, float R2C3,
                         float R3C1, float R3C2, float R3C3)
        {
            x1y1 = R1C1; x2y1 = R1C2; x3y1 = R1C3;
            x1y2 = R2C1; x2y2 = R2C2; x3y2 = R2C3;
            x1y3 = R3C1; x2y3 = R3C2; x3y3 = R3C3;
        }

        public float R1C1
        {
            get { return x1y1; }
            set { x1y1 = value; }
        }

        public float R1C2
        {
            get { return x2y1; }
            set { x2y1 = value; }
        }

        public float R1C3
        {
            get { return x3y1; }
            set { x3y1 = value; }
        }

        public float R2C1
        {
            get { return x1y2; }
            set { x1y2 = value; }
        }

        public float R2C2
        {
            get { return x2y2; }
            set { x2y2 = value; }
        }

        public float R2C3
        {
            get { return x3y2; }
            set { x3y2 = value; }
        }

        public float R3C1
        {
            get { return x1y3; }
            set { x1y3 = value; }
        }

        public float R3C2
        {
            get { return x2y3; }
            set { x2y3 = value; }
        }

        public float R3C3
        {
            get { return x3y3; }
            set { x3y3 = value; }
        }
    }

    public struct Matrix4x4
    {
        private float x1y1, x2y1, x3y1, x4y1,
                      x1y2, x2y2, x3y2, x4y2,
                      x1y3, x2y3, x3y3, x4y3,
                      x1y4, x2y4, x3y4, x4y4;

        public Matrix4x4(float R1C1, float R1C2, float R1C3, float R1C4,
                         float R2C1, float R2C2, float R2C3, float R2C4,
                         float R3C1, float R3C2, float R3C3, float R3C4,
                         float R4C1, float R4C2, float R4C3, float R4C4)
        {
            x1y1 = R1C1; x2y1 = R1C2; x3y1 = R1C3; x4y1 = R1C4;
            x1y2 = R2C1; x2y2 = R2C2; x3y2 = R2C3; x4y2 = R2C4;
            x1y3 = R3C1; x2y3 = R3C2; x3y3 = R3C3; x4y3 = R3C4;
            x1y4 = R4C1; x2y4 = R4C2; x3y4 = R4C3; x4y4 = R4C4;
        }

        public float R1C1
        {
            get { return x1y1; }
            set { x1y1 = value; }
        }

        public float R1C2
        {
            get { return x2y1; }
            set { x2y1 = value; }
        }

        public float R1C3
        {
            get { return x3y1; }
            set { x3y1 = value; }
        }

        public float R1C4
        {
            get { return x4y1; }
            set { x4y1 = value; }
        }

        public float R2C1
        {
            get { return x1y2; }
            set { x1y2 = value; }
        }

        public float R2C2
        {
            get { return x2y2; }
            set { x2y2 = value; }
        }

        public float R2C3
        {
            get { return x3y2; }
            set { x3y2 = value; }
        }

        public float R2C4
        {
            get { return x4y2; }
            set { x4y2 = value; }
        }

        public float R3C1
        {
            get { return x1y3; }
            set { x1y3 = value; }
        }

        public float R3C2
        {
            get { return x2y3; }
            set { x2y3 = value; }
        }

        public float R3C3
        {
            get { return x3y3; }
            set { x3y3 = value; }
        }

        public float R3C4
        {
            get { return x4y3; }
            set { x4y3 = value; }
        }

        public float R4C1
        {
            get { return x1y4; }
            set { x1y4 = value; }
        }

        public float R4C2
        {
            get { return x2y4; }
            set { x2y4 = value; }
        }

        public float R4C3
        {
            get { return x3y4; }
            set { x3y4 = value; }
        }

        public float R4C4
        {
            get { return x4y4; }
            set { x4y4 = value; }
        }

        public float[] toArray()
        {
            float[] result = new float[16];
            result[0] = x1y1; result[1] = x2y1; result[2] = x3y1; result[3] = x4y1;
            result[4] = x1y2; result[5] = x2y2; result[6] = x3y2; result[7] = x4y2;
            result[8] = x1y3; result[9] = x2y3; result[10] = x3y3; result[11] = x4y3;
            result[12] = x1y4; result[13] = x2y4; result[14] = x3y4; result[15] = x4y4;
            return result;
        }
    }

    public class Arcball
    {
        // Arcball Variables
        private Vector3x1 clickVec;
        private Vector3x1 dragVec;
        private float width;
        private float height;
        private float depth;

        public Arcball(float width, float height, float depth)
        {
            clickVec = new Vector3x1(0.0f, 0.0f, 0.0f);
            dragVec = new Vector3x1(0.0f, 0.0f, 0.0f);
            setBounds(width, height, depth);
        }

        // Wikipedia, calculation epsilon in C#
        private static float calcEpsilon()
        {
            float machineEpsilon = 1.0f;

            while ((float)(1.0 + (machineEpsilon / 2.0)) != 1.0f)
            {
                machineEpsilon /= 2.0f;
            }

            return machineEpsilon;
        }

        // Basic Math Functions

        private Vector2x1 Vector2x1Add(Vector2x1 v1, Vector2x1 v2)
        {
            return new Vector2x1(v1.X + v2.X, v1.Y + v2.Y);
        }

        private Vector2x1 Vector2x1Sub(Vector2x1 v1, Vector2x1 v2)
        {
            return new Vector2x1(v1.X - v2.X, v1.Y - v2.Y);
        }

        private Vector3x1 Vector3x1CrossProd(Vector3x1 v1, Vector3x1 v2)
        {
            return new Vector3x1(v1.Y * v2.Z - v1.Z * v2.Y, v1.Z * v2.X - v1.X * v2.Z, v1.X * v2.Y - v1.Y * v2.X);
        }

        private float Vector3x1DotProd(Vector3x1 v1, Vector3x1 v2)
        {
            return v1.X * v2.X + v1.Y * v2.Y + v1.Z * v2.Z;
        }

        private float Vector3x1LengthSquared(Vector3x1 v)
        {
            return v.X * v.X + v.Y * v.Y + v.Z * v.Z;
        }

        private float Vector3x1Length(Vector3x1 v)
        {
            return (float)Math.Sqrt(Vector3x1LengthSquared(v));
        }

        private Matrix3x3 Matrix3x3Zeros()
        {
            return new Matrix3x3(0.0f, 0.0f, 0.0f,
                                 0.0f, 0.0f, 0.0f,
                                 0.0f, 0.0f, 0.0f);
        }

        public Matrix3x3 Matrix3x3Identity()
        {
            Matrix3x3 result = Matrix3x3Zeros();
            result.R1C1 = result.R2C2 = result.R3C3 = 1.0f;
            return result;
        }

        private Matrix4x4 Matrix4x4Zeros()
        {
            return new Matrix4x4(0.0f, 0.0f, 0.0f, 0.0f,
                                 0.0f, 0.0f, 0.0f, 0.0f,
                                 0.0f, 0.0f, 0.0f, 0.0f,
                                 0.0f, 0.0f, 0.0f, 0.0f);
        }



        public Matrix4x4 Matrix4x4Identity()
        {
            Matrix4x4 result = Matrix4x4Zeros();
            result.R1C1 = result.R2C2 = result.R3C3 = result.R4C4 = 1.0f;
            return result;
        }

        public Matrix3x3 Matrix3x3Mult(Matrix3x3 m1, Matrix3x3 m2)
        {
            Matrix3x3 result = new Matrix3x3();
            result.R1C1 = m1.R1C1 * m2.R1C1 + m1.R1C2 * m2.R2C1 + m1.R1C3 * m2.R3C1;
            result.R1C2 = m1.R1C1 * m2.R1C2 + m1.R1C2 * m2.R2C2 + m1.R1C3 * m2.R3C2;
            result.R1C3 = m1.R1C1 * m2.R1C3 + m1.R1C2 * m2.R2C3 + m1.R1C3 * m2.R3C3;
            result.R2C1 = m1.R2C1 * m2.R1C1 + m1.R2C2 * m2.R2C1 + m1.R2C3 * m2.R3C1;
            result.R2C2 = m1.R2C1 * m2.R1C2 + m1.R2C2 * m2.R2C2 + m1.R2C3 * m2.R3C2;
            result.R2C3 = m1.R2C1 * m2.R1C3 + m1.R2C2 * m2.R2C3 + m1.R2C3 * m2.R3C3;
            result.R3C1 = m1.R3C1 * m2.R1C1 + m1.R3C2 * m2.R2C1 + m1.R3C3 * m2.R3C1;
            result.R3C2 = m1.R3C1 * m2.R1C2 + m1.R3C2 * m2.R2C2 + m1.R3C3 * m2.R3C2;
            result.R3C3 = m1.R3C1 * m2.R1C3 + m1.R3C2 * m2.R2C3 + m1.R3C3 * m2.R3C3;
            return result;
        }

        public Matrix4x4 Matrix4x4Mult(Matrix4x4 m1, Matrix4x4 m2)
        {
            Matrix4x4 result = new Matrix4x4();
            result.R1C1 = m1.R1C1 * m2.R1C1 + m1.R1C2 * m2.R2C1 + m1.R1C3 * m2.R3C1 + m1.R1C4 * m2.R4C1;
            result.R1C2 = m1.R1C1 * m2.R1C2 + m1.R1C2 * m2.R2C2 + m1.R1C3 * m2.R3C2 + m1.R1C4 * m2.R4C2;
            result.R1C3 = m1.R1C1 * m2.R1C3 + m1.R1C2 * m2.R2C3 + m1.R1C3 * m2.R3C3 + m1.R1C4 * m2.R4C3;
            result.R1C4 = m1.R1C1 * m2.R1C4 + m1.R1C2 * m2.R2C4 + m1.R1C3 * m2.R3C4 + m1.R1C4 * m2.R4C4;
            result.R2C1 = m1.R2C1 * m2.R1C1 + m1.R2C2 * m2.R2C1 + m1.R2C3 * m2.R3C1 + m1.R2C4 * m2.R4C1;
            result.R2C2 = m1.R2C1 * m2.R1C2 + m1.R2C2 * m2.R2C2 + m1.R2C3 * m2.R3C2 + m1.R2C4 * m2.R4C2;
            result.R2C3 = m1.R2C1 * m2.R1C3 + m1.R2C2 * m2.R2C3 + m1.R2C3 * m2.R3C3 + m1.R2C4 * m2.R4C3;
            result.R2C4 = m1.R2C1 * m2.R1C4 + m1.R2C2 * m2.R2C4 + m1.R2C3 * m2.R3C4 + m1.R2C4 * m2.R4C4;
            result.R3C1 = m1.R3C1 * m2.R1C1 + m1.R3C2 * m2.R2C1 + m1.R3C3 * m2.R3C1 + m1.R3C4 * m2.R4C1;
            result.R3C2 = m1.R3C1 * m2.R1C2 + m1.R3C2 * m2.R2C2 + m1.R3C3 * m2.R3C2 + m1.R3C4 * m2.R4C2;
            result.R3C3 = m1.R3C1 * m2.R1C3 + m1.R3C2 * m2.R2C3 + m1.R3C3 * m2.R3C3 + m1.R3C4 * m2.R4C3;
            result.R3C4 = m1.R3C1 * m2.R1C4 + m1.R3C2 * m2.R2C4 + m1.R3C3 * m2.R3C4 + m1.R3C4 * m2.R4C4;
            result.R4C1 = m1.R4C1 * m2.R1C1 + m1.R4C2 * m2.R2C1 + m1.R4C3 * m2.R3C1 + m1.R4C4 * m2.R4C1;
            result.R4C2 = m1.R4C1 * m2.R1C2 + m1.R4C2 * m2.R2C2 + m1.R4C3 * m2.R3C2 + m1.R4C4 * m2.R4C2;
            result.R4C3 = m1.R4C1 * m2.R1C3 + m1.R4C2 * m2.R2C3 + m1.R4C3 * m2.R3C3 + m1.R4C4 * m2.R4C3;
            result.R4C4 = m1.R4C1 * m2.R1C4 + m1.R4C2 * m2.R2C4 + m1.R4C3 * m2.R3C4 + m1.R4C4 * m2.R4C4;
            return result;
        }

        // Construct 3x3 Matrix from Quaternion
        public Matrix3x3 Matrix3x3SetRotationFromVector4x1(Vector4x1 v)
        {
            float n, s;
            float xs, ys, zs;
            float wx, wy, wz;
            float xx, xy, xz;
            float yy, yz, zz;

            n = (v.X * v.X) + (v.Y * v.Y) + (v.Z * v.Z) + (v.W * v.W);
            s = (n > 0.0f) ? (2.0f / n) : 0.0f;

            xs = v.X * s; ys = v.Y * s; zs = v.Z * s;
            wx = v.W * xs; wy = v.W * ys; wz = v.W * zs;
            xx = v.X * xs; xy = v.X * ys; xz = v.X * zs;
            yy = v.Y * ys; yz = v.Y * zs; zz = v.Z * zs;

            Matrix3x3 result = new Matrix3x3();
            result.R1C1 = 1.0f - (yy + zz); result.R1C2 = xy - wz; result.R1C3 = xz + wy;
            result.R2C1 = xy + wz; result.R2C2 = 1.0f - (xx + zz); result.R2C3 = yz - wx;
            result.R3C1 = xz - wy; result.R3C2 = yz + wx; result.R3C3 = 1.0f - (xx + yy);
            return result;
        }

        // Copy Rotational Components From One 4x4 Matrix to Another
        private Matrix4x4 Matrix4x4SetRotationScaleFromMatrix4x4(Matrix4x4 m1, Matrix4x4 m2)
        {
            Matrix4x4 result = m1;
            result.R1C1 = m2.R1C1;
            result.R1C2 = m2.R1C2;
            result.R1C3 = m2.R1C3;
            result.R2C1 = m2.R2C1;
            result.R2C2 = m2.R2C2;
            result.R2C3 = m2.R2C3;
            result.R3C1 = m2.R3C1;
            result.R3C2 = m2.R3C2;
            result.R3C3 = m2.R3C3;
            return result;
        }

        // Copy Rotational Components From a 3x3 Matrix to a 4x4 Matrix
        private Matrix4x4 Matrix4x4SetRotationScaleFromMatrix3x3(Matrix4x4 m1, Matrix3x3 m2)
        {
            Matrix4x4 result = m1;
            result.R1C1 = m2.R1C1;
            result.R1C2 = m2.R1C2;
            result.R1C3 = m2.R1C3;
            result.R2C1 = m2.R2C1;
            result.R2C2 = m2.R2C2;
            result.R2C3 = m2.R2C3;
            result.R3C1 = m2.R3C1;
            result.R3C2 = m2.R3C2;
            result.R3C3 = m2.R3C3;
            return result;
        }

        // Multiply a 4x4 Matrix By a Scale Factor
        private Matrix4x4 Matrix4x4MultRotationScale(Matrix4x4 m, float scale)
        {
            Matrix4x4 result = m;
            result.R1C1 *= scale; result.R1C2 *= scale; result.R1C3 *= scale;
            result.R2C1 *= scale; result.R2C2 *= scale; result.R2C3 *= scale;
            result.R3C1 *= scale; result.R3C2 *= scale; result.R3C3 *= scale;
            return result;
        }

        // Basic Singular Value Decomposition (SVD) of a 4x4 Matrix - Enough for this Application
        private float Matrix4x4SVD(Matrix4x4 m)
        {
            return (float)Math.Sqrt((m.R1C1 * m.R1C1 + m.R1C2 * m.R1C2 + m.R1C3 * m.R1C3 + m.R2C1 * m.R2C1 + m.R2C2 * m.R2C2 + m.R2C3 * m.R2C3 + m.R3C1 * m.R3C1 + m.R3C2 * m.R3C2 + m.R3C3 * m.R3C3) / 3.0);
        }

        // Set Rotation Components of 4x4 Matrix from 3x3 Matrix Using SVD to Scale
        public Matrix4x4 Matrix4x4SetRotationFromMatrix3x3(Matrix4x4 m1, Matrix3x3 m2)
        {
            Matrix4x4 result = m1;
            float scale = Matrix4x4SVD(m1);
            result = Matrix4x4SetRotationScaleFromMatrix3x3(result, m2);
            result = Matrix4x4MultRotationScale(result, scale);
            return result;
        }

        // Transform a Vector by a 4x4 Matrix
        public Vector4x1 MatrixTransformedVector(Matrix4x4 m, Vector4x1 v)
        {
            Vector4x1 result = new Vector4x1();
            result.X = m.R1C1 * v.X + m.R1C2 * v.Y + m.R1C3 * v.Z + m.R1C4 * v.W;
            result.Y = m.R2C1 * v.X + m.R2C2 * v.Y + m.R2C3 * v.Z + m.R2C4 * v.W;
            result.Z = m.R3C1 * v.X + m.R3C2 * v.Y + m.R3C3 * v.Z + m.R3C4 * v.W;
            result.W = m.R4C1 * v.X + m.R4C2 * v.Y + m.R4C3 * v.Z + m.R4C4 * v.W;
            return result;
        }

        // Set Bounds of Arcball
        public void setBounds(float width, float height, float depth)
        {
            this.width = 1.0f / ((width - 1.0f) * 0.5f);
            this.height = 1.0f / ((height - 1.0f) * 0.5f);
            this.depth = 1.0f / ((depth - 1.0f) * 0.5f);
        }

        // Map Vector to Arcball Sphere
        private Vector3x1 mapToSphere(Vector3x1 p)
        {
            Vector3x1 result = new Vector3x1();
            Vector3x1 temp = p;
            // Map X and Y Coordinates into Range
            temp.X = (temp.X * width) - 1.0f;
            temp.Y = 1.0f - (temp.Y * height);
            float lengthSquared, norm;
            // If Mouse Coordinate, Z-value will Be 10000
            if (temp.Z == 10000)
            {
                lengthSquared = temp.X * temp.X + temp.Y * temp.Y;
                // If Point Lies Outside Sphere
                if (lengthSquared > 1.0f)
                {
                    // Normalise Vector
                    norm = 1.0f / (float)Math.Sqrt(lengthSquared);
                    result.X = temp.X * norm;
                    result.Y = temp.Y * norm;
                    result.Z = 0.0f;
                }
                else
                {
                    // Do Not Normalise Vector, Interpolate Z-value
                    result.X = temp.X * -1;
                    result.Y = temp.Y;
                    result.Z = (float)Math.Sqrt(1.0f - lengthSquared);
                }
            }
            else
            {
                temp.Z = 1.0f - (temp.Z * depth);
                lengthSquared = temp.X * temp.X + temp.Y * temp.Y + temp.Z * temp.Z;
                if (lengthSquared > 1.0f)
                {
                    norm = 1.0f / (float)Math.Sqrt(lengthSquared);
                    result.X = temp.X * norm;
                    result.Y = temp.Y * norm;
                    result.Z = temp.Z * norm;
                }
                else
                {
                    result.X = temp.X * -1;
                    result.Y = temp.Y;
                    result.Z = temp.Z;
                }
            }
            return result;
        }

        // Map the Starting Vector to Sphere
        public void click(Vector3x1 v)
        {
            clickVec = mapToSphere(v);
        }

        // Map the Current Vector to Sphere and Calculate Quaternion for Rotation of Start Vector onto Current Vector
        public Vector4x1 drag(Vector3x1 p)
        {
            dragVec = mapToSphere(p);
            Vector4x1 result = new Vector4x1();
            Vector3x1 perpendicular = Vector3x1CrossProd(clickVec, dragVec);
            if (Vector3x1Length(perpendicular) > calcEpsilon())
            {
                result.X = perpendicular.X;
                result.Y = perpendicular.Y;
                result.Z = perpendicular.Z;
                result.W = Vector3x1DotProd(clickVec, dragVec);
            }
            else
            {
                result.X = result.Y = result.Z = result.W = 0.0f;
            }
            return result;
        }
    }
}
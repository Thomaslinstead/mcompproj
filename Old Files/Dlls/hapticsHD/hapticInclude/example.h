
#ifndef _EXAMPLE_H
#define _EXAMPLE_H

//init functions
void initGraphics();
void initWorld();
void initHaptics();

//OpenGL functions
void display();
void reshape(int wid, int hei);
void idle();

#endif _EXAMPLE_H

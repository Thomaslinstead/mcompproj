
#ifndef _HAPTIC_DISPLAY_STATE_H
#define _HAPTIC_DISPLAY_STATE_H

#include <HD/hd.h>
#include <HDU/hdu.h>
#include <HDU/hduVector.h>

//A struct to store some properties of the haptic device
typedef struct
{
    hduVector3Dd position;
	hduVector3Dd velocity;
    HDdouble transform[16];
	HDint UpdateRate;
} HapticDisplayState;

#endif _HAPTIC_DISPLAY_STATE_H
#include "OpenHaptics.h"

#include <iostream>
using namespace std;
#include <Windows.h>
#include <gl/glut.h>

__int64 startTime, now;
__int64 ticksPerSecond;
bool first = false;
int count = 0;
hduVector3Dd force( 0,0,0 );
float positions[3];
int topBtnDown = 0;
int bottomBtnDown = 0;

Haptics::Haptics()
{
	ghHD = HD_INVALID_HANDLE;

	COLLISION_FORCEFEEDBACK = false;
	pState = new HapticDisplayState();
}


void Haptics::setForces(bool c)
{
	COLLISION_FORCEFEEDBACK = c;
}

float Haptics::getForceX() {
	return force[0];
}

float Haptics::getForceY() {
	return force[1];
}

float Haptics::getForceZ() {
	return force[2];
}

void Haptics::setForceX(float x) {
	force[0] = x;
}

void Haptics::setForceY(float y) {
	force[1] = y;
}

void Haptics::setForceZ(float z) {
	force[2] = z;
}

float Haptics::getPositionX() {
	return positions[0];
}

float Haptics::getPositionY() {
	return positions[1];
}

float Haptics::getPositionZ() {
	return positions[2];
}

int Haptics::getTopButton() {
	return topBtnDown;
}

int Haptics::getBottomButton() {
	return bottomBtnDown;
}

void Haptics::loadDevices()
{
	HDErrorInfo error;

	ghHD = hdInitDevice(HD_DEFAULT_DEVICE);
	if (HD_DEVICE_ERROR(error = hdGetError()))
	{
		//no default device - test another name for the device.
		ghHD = hdInitDevice("Omni1");
		if (HD_DEVICE_ERROR(error = hdGetError()))
		{	
		}
	}
}

void Haptics::initHD()
{	
	QueryPerformanceFrequency((LARGE_INTEGER*)&ticksPerSecond);

	loadDevices();

	hdMakeCurrentDevice(ghHD);
	hdEnable(HD_FORCE_OUTPUT);

	hUpdateDeviceCallback = hdScheduleAsynchronous(
		touchMesh, this, HD_MAX_SCHEDULER_PRIORITY);

	hdStartScheduler();
}

void Haptics::touchTool()
{
	if(first)
		QueryPerformanceCounter((LARGE_INTEGER*)&startTime);

	/* Obtain a thread-safe copy of the current haptic display state. */
	hdScheduleSynchronous(copyHapticDisplayState, pState,
		HD_DEFAULT_SCHEDULER_PRIORITY);

	int currentButtons;
	hduVector3Dd position;
	HDdouble forceClamp;
	HDErrorInfo error;

	hdBeginFrame(ghHD);

	hdGetIntegerv(HD_CURRENT_BUTTONS, &currentButtons);


	if(COLLISION_FORCEFEEDBACK) {
		//calculate the force
		float pos[3] = {-pState->position[0],-pState->position[1],-pState->position[2]};
		positions[0] = pos[0];
		positions[1] = pos[1];
		positions[2] = pos[2];
		float f[3]={0,0,0};

		//check which buttons are down
		if(currentButtons & HD_DEVICE_BUTTON_1)
			bottomBtnDown = 1;
		else
			bottomBtnDown = 0;
		if(currentButtons & HD_DEVICE_BUTTON_2)
			topBtnDown = 1;
		else
			topBtnDown = 0;
		
		hdSetDoublev(HD_CURRENT_FORCE, force);
	} else {
		hduVector3Dd force2( 0,0,0 );
		force2[0] = 0;
		force2[1] = 0;
		force2[2] = 0;
		hdSetDoublev(HD_CURRENT_FORCE, force2);
	}
	hdEndFrame(ghHD);

	QueryPerformanceCounter((LARGE_INTEGER*)&now);
	if(((float(now - startTime) / float(ticksPerSecond))) > 1) {
		cout << "Haptic frames per second: " << count << endl;
		count = 0;
		QueryPerformanceCounter((LARGE_INTEGER*)&startTime);
	}
	count++;
}

bool Haptics::toggleForces() {
	COLLISION_FORCEFEEDBACK = !COLLISION_FORCEFEEDBACK;
	return COLLISION_FORCEFEEDBACK;
}

/******************** OPEN HAPTICS CALBACK FUNCTIONS **************/

//this function copies the data to the haptic device, like position, velocity etc.
HDCallbackCode HDCALLBACK copyHapticDisplayState(void *pUserData) {
	HapticDisplayState *pState = (HapticDisplayState *) pUserData;

	hdGetDoublev(HD_CURRENT_POSITION, pState->position);
	hdGetDoublev(HD_CURRENT_TRANSFORM, pState->transform);
	hdGetDoublev(HD_CURRENT_VELOCITY, pState->velocity);
	hdGetIntegerv(HD_UPDATE_RATE, &pState->UpdateRate);

	return HD_CALLBACK_DONE;
}

//this is the main haptics thread. Calls touchTool each frame.
HDCallbackCode HDCALLBACK touchMesh(void *pUserData) {
	Haptics* haptic = (Haptics*) pUserData;

	haptic->touchTool();

	return HD_CALLBACK_CONTINUE;
}

/****************************************************************/

#include "example.h"
#include <math.h>

#include "OpenHaptics.h"

#include <gl/glut.h>

#include <iostream>
using namespace std;

//haptics system - used for calculation of forces
//and all routines associated with haptic feedback.
Haptics* haptics;
bool hapticsEnabled = true;

//general variables used by opengl
int ScreenWidth=1024;
int ScreenHeight=768;

//specifies the id for the current window
int main_window;

void display()
{
	glutSetWindow(main_window);	

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glLoadIdentity();

	if(hapticsEnabled) {

		glTranslatef( 0,0,-150); //just translate back to that we can see the haptic cursor

		haptics->drawEndPoints(); //draw haptic cursor

		glPushMatrix();
		double* matrix = haptics->getTransform();
		glDisable(GL_LIGHTING);
		glBegin(GL_LINES);
			glVertex3f(matrix[12], matrix[13], matrix[14]);
			glVertex3f(matrix[12]-matrix[8]*20, matrix[13]-matrix[9]*20, matrix[14]-matrix[10]*20);
		glEnd();
		glPopMatrix();
	} else {
		cout << "No haptics" << endl;
	}

	glutSolidSphere(10,16,16);

	glutSwapBuffers();
}

void reshape(int wid, int hei)
{	
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	glViewport (0, 0, wid, hei);

	gluPerspective(90, (GLfloat)wid/(GLfloat)hei, 0.9,1500.0);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}

void idle()
{
	glutSetWindow(main_window);

	//cout << "hello" << endl;
	

	glutPostRedisplay();
}

void keys(unsigned char p, int x, int y) 
{
	int n=p;
	if(n==27)
	{
		exit(0);
	}
	glutPostRedisplay();
}

void initGraphics() 
{
	glClearColor (0.15,0.05, 0.05, 0.0);

	glEnable(GL_DEPTH_TEST);

	//LIGHTS
	GLfloat white_light1[] = {0.9, 0.9, 0.7, 0.0};		//color of light source
	GLfloat light0_position[] = {0.0, 0.0, 150.0, 0.0};
	
	glLightfv(GL_LIGHT0, GL_DIFFUSE, white_light1);
	glLightfv(GL_LIGHT0, GL_SPECULAR, white_light1);

	glLightfv(GL_LIGHT0, GL_POSITION, light0_position);
	float light0_direction[3] = {0,0,-20};
	glLightf(GL_LIGHT0, GL_SPOT_CUTOFF, 2500.0);
	glLightf(GL_LIGHT0, GL_CONSTANT_ATTENUATION, 2.0);
	glLightf(GL_LIGHT0, GL_LINEAR_ATTENUATION, 0.2);
	glLightf(GL_LIGHT0, GL_QUADRATIC_ATTENUATION, 0.5);

	glLightfv(GL_LIGHT0, GL_SPOT_DIRECTION, light0_direction);

	glEnable(GL_LIGHTING);					//allow lighting calculations
	glEnable(GL_LIGHT0);
	
	//glEnable(GL_POLYGON_OFFSET_FILL);  //used to enable wireframe to be drawn over the polygon
	//glPolygonOffset(1.0,1.0);

	glEnable(GL_CULL_FACE);  //turns backface culling on
}

void initHaptics()
{
	//set up haptics
	haptics = new Haptics();

	//initialise haptics
	haptics->initHD();

	//set forces to true - no forces returned when false
	haptics->setForces(true);
}


int main(int argc, char *argv[])
{	
	glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH );

	/****** SET UP WINDOW ****/
	glutInitWindowPosition(0, 0);
	glutInitWindowSize(1024, 768);
	main_window = glutCreateWindow("Sample Haptics Program");
	
	initGraphics();
	if(hapticsEnabled)
		initHaptics();

	/****** OPENGL CALLBACKS ***********/
	glutKeyboardFunc(keys);
	glutDisplayFunc(display);
	glutReshapeFunc(reshape);
	glutIdleFunc(idle);

	//glutFullScreen();

	glutMainLoop();

	return 0;
}
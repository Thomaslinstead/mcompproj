
//OpenGL 2.0 - Loads Proteins from Files on the Protein Data Bank

//Author: Stephen Laycock
//Copyright (C) 2011 - Stephen Laycock, University of East Anglia
//Source Code For Educational Purposes


//////////////////////////////////////////////////////
//////////////////////////////////////////////////////
//													//
//	W/S: Zoom in/out								//
//	A/D: Move second protein						//
//	Arrow keys: Rotate proteins						//
//													//
//////////////////////////////////////////////////////
//////////////////////////////////////////////////////




#include <windows.h>		// Header File For Windows
#include "gl/glew.h"

#include "console.h"

#include "PDBLoading\FileHandle.h"
#include "ProteinRendering\Atoms.h"
#include <vector>
#include <math.h>           // Header file for C++ Math Libary 

#include <boost/numeric/ublas/vector.hpp>

#include <boost/numeric/ublas/io.hpp>

AtomList theList;
AtomList theList2;
Atom** myListPtr;
Atom** myListPtr2;
int myListSize=0;
int myListSize2=0;
bool drawHydrogens = false;

ConsoleWindow console;

#include <iostream>
using namespace std;

__int64 startTime, now;
__int64 ticksPerSecond;

int screenWidth=640, screenHeight=480;
bool keys[256];
double spinx=0;
double spiny=0;
double mouse_x, mouse_y;
bool LeftPressed = false;
bool collision = false;
float ZOOM = 500.0;
float MOVE = 0.0;
float speed = 1.0;
double paramA = 10000000000;
double paramB = 10000000000;
Vector3d totalForce;

//OPENGL FUNCTION PROTOTYPES
void display();				//called in winmain to draw everything to the screen
void reshape();				//called when the window is resized
void init();				//called in winmain when the program starts.
void processKeys();         //called in winmain to process keyboard input
void update();				//called in winmain to update variables

void drawCube();			//draws a cube of unit size to the screen, with centre at the origin.
void renderProtein();
void renderProtein2();

Vector3d vectorPow(Vector3d v, int pow);
Vector3d vectorAdd(Vector3d V1, Vector3d V2);
Vector3d vectorMinus(Vector3d V1, Vector3d V2);
Vector3d vectorDiv(double d, Vector3d V2);
Vector3d multiVecByVal(float f, Vector3d V2);
Vector3d cross(Vector3d a, Vector3d b);
Vector3d dot(Vector3d V1, Vector3d V2);

/*************    START OF OPENGL FUNCTIONS   ****************/

GLfloat position[] = { 0.0f, 0.0f, 0.0f, 1.0f };

bool first = false;

void display()									
{
	static int count = 0;

	if(first)
		QueryPerformanceCounter((LARGE_INTEGER*)&startTime);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	gluLookAt(0,0,ZOOM,0,0,0,0,1,0);

	glPushMatrix();

	//set light positions for scene
	glPushMatrix();
	//glLightfv(GL_LIGHT0, GL_POSITION, position);

	//glCallList(SuperAtom::sphereDisplayList);
	glPopMatrix();

	//spin the molecule
	//glRotated(spiny,0,1,0);
	//glRotated(spinx,1,0,0);

	//render the protein
	renderProtein();

	glPopMatrix();

	glPushMatrix();

	/*Atom* temp = myListPtr[0];
	int a, b, c;
	temp->readMyPosition(a,b,c); 
	cout << &a << &b << &c << endl;*/

	//spin the molecule

	glTranslatef(750+MOVE,0,0);
	//glRotated(spiny,0,1,0);
	//glRotated(spinx,1,0,0);
	renderProtein2();

	glPopMatrix();

	glPushMatrix();
	glBegin(GL_LINES);
	glLineWidth(5.0f);
	glVertex3f(0,0,220);
	//glVertex3f(100,100,220);
	glVertex3f(totalForce.x,totalForce.y,220+totalForce.z);
	glEnd();
	glPopMatrix();

	glFlush();	

	QueryPerformanceCounter((LARGE_INTEGER*)&now);
	if(((float(now - startTime) / float(ticksPerSecond))) > 1)
	{
		cout << "Frames per second: " << count << endl;
		count = 0;
		QueryPerformanceCounter((LARGE_INTEGER*)&startTime);
	}
	count++;
	int sizeX = 1024, sizeY = 1024;
	int numberOfBytes = 4 * 1024 * 1024; 
	unsigned char *pPixelData = new unsigned char[numberOfBytes];
	::glReadPixels(0, 0, sizeX, sizeY, GL_BGRA, GL_UNSIGNED_BYTE, pPixelData);

}

void renderProtein()
{
	glPushMatrix();
	glEnable(GL_NORMALIZE);
	for(int i = 0; i < myListSize; i++)
	{
		Atom* temp = myListPtr[i];

		glPushMatrix();

		if( (!temp->isWater()) && (temp->getEle().compare("H") != 0) ) //not water or hydrogen
		{
			temp->renderSimple();
		}
		else if((temp->getEle().compare("H") == 0) && drawHydrogens) //can add hydrogen here!
		{
			temp->renderSimple();
		}
		glPopMatrix();

		//POSITION OF ATOM: temp->getMyLocalPosition().x, temp->getMyLocalPosition().y, temp->getMyLocalPosition().z
		//RADIUS OF ATOM: temp->getRadius();
		temp->setCollision(false);
	}
	glDisable(GL_NORMALIZE);
	glPopMatrix();
}

void getCollision()
{
	totalForce = Vector3d(0,0,0);
	glPushMatrix();
	glEnable(GL_NORMALIZE);
	for(int i = 0; i < myListSize; i++)
	{
		for(int j = 0; j < myListSize2; j++)
		{
			//Get the Local Coordinates
			Atom* temp = myListPtr[i];
			Atom* temp2 = myListPtr2[j];
			float p1x = temp->getMyLocalPosition().x, p1y = temp->getMyLocalPosition().y, p1z = temp->getMyLocalPosition().z;
			float p2x = temp2->getMyLocalPosition().x, p2y = temp2->getMyLocalPosition().y, p2z = temp2->getMyLocalPosition().z;

			//Apply Tranformations to the coordinates
			p2x+=750+MOVE;

			//Check For Collision
			float d = sqrt(((p1x-p2x)*(p1x-p2x)) + ((p1y-p2y)*(p1y-p2y)) + ((p1z-p2z)*(p1z-p2z)));
			float r1 = temp->getRadius()*2;
			float r2 = temp2->getRadius()*2;
			if (d <= r1 + r2)
			{
				//The Spheres Overlap
				temp->setCollision(true);
				temp2->setCollision(true);
				collision = true;
			

				/***  Calculate the Force Feedback ***/

				//Firstly find the vector B-A 
				Vector3d v = Vector3d((p1x-p2x),(p1y-p2y),(p1z-p2z));		
				//Then calculate the Magnitude = |m|
				double m = sqrt((v.x * v.x) + (v.y * v.y) + (v.z * v.z));
				//Get the Unit Vector U^ = V/|m|
				Vector3d u = Vector3d(v.x/m,v.y/m,v.z/m);


				//Version 1// - Calculate the force equation (if Rij is the vector???)  //NOT USED//
				Vector3d leftside = multiVecByVal(12,vectorDiv(paramA,(vectorPow(v,13)))); //Left Side of equation
				Vector3d rightside = multiVecByVal(6,vectorDiv(paramB,(vectorPow(v,7)))); //Right Side of equation
				Vector3d innerForce = dot(vectorMinus(leftside,rightside),u);  //Final Force

				//Version 2// - Calculate the force equation (if Rij is the magnitude???)  //IN USE//
				float leftsidenum = 12*(paramA/pow(m,13)); //Left Side of equation
				float rightsidenum = 6*(paramB/pow(m,7)); //Right Side of equation
				Vector3d molecularForce = multiVecByVal((leftsidenum-rightsidenum), u); //Final molecule force

				//Add the force to the TotalForce
				totalForce = totalForce + molecularForce;
			}
		}
	}
	glDisable(GL_NORMALIZE);
	glPopMatrix();
	cout << "TotalForce: " << totalForce.x << ", " << totalForce.y << ", " << totalForce.z << endl;
}

Vector3d vectorPow(Vector3d v, int pow){
	Vector3d result = v;
	for(int i = 0; i<pow; i++){
		result = dot(result,v);
	}
	return result;
}

Vector3d vectorAdd(Vector3d V1, Vector3d V2){
	return Vector3d(V1.x+V2.x,V1.y+V2.y,V1.z+V2.z);
}

Vector3d vectorMinus(Vector3d V1, Vector3d V2){
	return Vector3d(V1.x-V2.x,V1.y-V2.y,V1.z-V2.z);
}

Vector3d vectorDiv(double t, Vector3d V2){
	return Vector3d(t/V2.x,t/V2.y,t/V2.z);
}

Vector3d multiVecByVal(float f, Vector3d V2){
	return Vector3d(f*V2.x,f*V2.y,f*V2.z);
}

Vector3d dot(Vector3d V1, Vector3d V2){
	return Vector3d(V1.x*V2.x,V1.y*V2.y,V1.z*V2.z);
}

Vector3d cross(Vector3d a, Vector3d b){
	Vector3d r;
	r.x = a.y*b.z - a.z*b.y;
	r.y = a.z*b.x - a.x*b.z;
	r.z = a.x*b.y - a.y*b.x;
	return r;
}

void renderProtein2()
{
	glPushMatrix();
	glEnable(GL_NORMALIZE);
	for(int i = 0; i < myListSize2; i++)
	{
		Atom* temp = myListPtr2[i];

		glPushMatrix();

		if( (!temp->isWater()) && (temp->getEle().compare("H") != 0) ) //not water or hydrogen
		{
			temp->renderSimple();
		}
		else if((temp->getEle().compare("H") == 0) && drawHydrogens) //can add hydrogen here!
		{
			temp->renderSimple();
		}
		glPopMatrix();
		temp->setCollision(false);
		//POSITION OF ATOM: temp->getMyLocalPosition().x, temp->getMyLocalPosition().y, temp->getMyLocalPosition().z
		//RADIUS OF ATOM: temp->getRadius();
	}
	glDisable(GL_NORMALIZE);
	glPopMatrix();
}

void reshape(int width, int height)		// Resize the OpenGL window
{
	screenWidth=width; screenHeight = height;           // to ensure the mouse coordinates match 
	// we will use these values to set the coordinate system

	glViewport(0,0,width,height);						// Reset The Current Viewport

	glMatrixMode(GL_PROJECTION);						// Select The Projection Matrix
	glLoadIdentity();									// Reset The Projection Matrix

	//set up a perspective view (fov, aspect ratio, near, far)
	gluPerspective(45.0f,(GLfloat)width/(GLfloat)height,0.1f,4000.0f);

	glMatrixMode(GL_MODELVIEW);							// Select The Modelview Matrix
	glLoadIdentity();									// Reset The Modelview Matrix
}
void init()
{
	QueryPerformanceFrequency((LARGE_INTEGER*)&ticksPerSecond);

	glClearColor(0.0,0.0,0.0,0.0);						//sets the clear colour to yellow
	glEnable(GL_DEPTH_TEST);				//Enables depth testing - z-buffer is used to store depths of pixels.

	//cout << "Loading file..." <<endl;
	FileHandle file_handle;
	FileHandle file_handle2;
	file_handle.openFile("PDB Test Files\\1CRN(327).pdb");
	file_handle2.openFile("PDB Test Files\\5ADH(3127).pdb");
	//cout << "PDB loaded" << endl;


	SuperAtom::hapticRadius = 0;

	theList = file_handle.getAtomList();
	theList2 = file_handle2.getAtomList();
	myListPtr = &theList[0];
	myListPtr2 = &theList2[0];
	myListSize = theList.size();
	myListSize2 = theList2.size();

	cout << "Thie first file contains "<< myListSize << " atoms. " << endl;
	cout << "Thie second file contains "<< myListSize2 << " atoms. " << endl;

	//ZOOM = file_handle.getFurthestDistanceToMidPoint();
	//cout << "RADIUS OF BOUNDING CIRCLE " << ZOOM << endl;

	//find radius of largest atom
	float radius=0;
	for(int i = 0; i < myListSize; i++)
	{
		Atom* temp = myListPtr[i];
		float r = temp->getRadius();
		if(r > radius)
			radius = r;
	}

	//ZOOM the molecule back 
	//ZOOM = (ZOOM / tan((45*0.5)*(3.141/180.0))) + radius;

	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);

	glShadeModel(GL_SMOOTH);							// Enable smooth shading
	glClearColor(1.0f, 1.0f, 1.0f, 1.0f);				// White background
	glEnable(GL_DEPTH_TEST);							// Enables depth testing
	glDepthFunc(GL_LEQUAL);								// The type of depth testing to do


	GLfloat ambientLight[] = { 0.3f, 0.3f, 0.3f, 1.0f };
	GLfloat diffuseLight[] = { 0.6,0.6,0.6, 1.0f };
	GLfloat specularLight[] = { 1.0f, 1.0f, 1.0f, 1.0f };

	glLightfv(GL_LIGHT0, GL_AMBIENT, ambientLight);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, diffuseLight);
	glLightfv(GL_LIGHT0, GL_SPECULAR, specularLight);

	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);

	glMaterialf(GL_FRONT, GL_SHININESS, 90);

	SuperAtom::makeList();
}
void processKeys()
{	
	if(keys['W'])
	{
		ZOOM = ZOOM - 5;
		keys['W'] = false;
	}
	else if(keys['S'])
	{
		ZOOM = ZOOM + 5;
		keys['S'] = false;
	}
	if(keys['A'])
	{
		MOVE = MOVE - 5;
		keys['A'] = false;
	}
	else if(keys['D'])
	{
		MOVE = MOVE + 5;
		keys['D'] = false;
	}
	if(keys[VK_UP])
	{
		spinx --;
		keys[VK_UP] = false;
	}
	else if(keys[VK_DOWN])
	{
		spinx ++;
		keys[VK_DOWN] = false;
	}
	if(keys[VK_LEFT])
	{
		spiny --;
		keys[VK_LEFT] = false;
	}
	else if(keys[VK_RIGHT])
	{
		spiny ++;
		keys[VK_RIGHT] = false;
	}
}
void update()
{
}

/**************** END OPENGL FUNCTIONS *************************/
//WIN32 functions
LRESULT	CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);	// Declaration For WndProc
void KillGLWindow();									// releases and destroys the window
bool CreateGLWindow(char* title, int width, int height); //creates the window
int WINAPI WinMain(	HINSTANCE, HINSTANCE, LPSTR, int);  // Win32 main function

//win32 global variabless
HDC			hDC=NULL;		// Private GDI Device Context
HGLRC		hRC=NULL;		// Permanent Rendering Context
HWND		hWnd=NULL;		// Holds Our Window Handle
HINSTANCE	hInstance;		// Holds The Instance Of The Application


/******************* WIN32 FUNCTIONS ***************************/
int WINAPI WinMain(	HINSTANCE	hInstance,			// Instance
				   HINSTANCE	hPrevInstance,		// Previous Instance
				   LPSTR		lpCmdLine,			// Command Line Parameters
				   int			nCmdShow)			// Window Show State
{
	console.Open();

	MSG		msg;									// Windows Message Structure
	bool	done=false;								// Bool Variable To Exit Loop

	// Create Our OpenGL Window
	if (!CreateGLWindow("OpenGL Win32 Example",screenWidth,screenHeight))
	{
		return 0;									// Quit If Window Was Not Created
	}

	while(!done)									// Loop That Runs While done=FALSE
	{
		if (PeekMessage(&msg,NULL,0,0,PM_REMOVE))	// Is There A Message Waiting?
		{
			if (msg.message==WM_QUIT)				// Have We Received A Quit Message?
			{
				done=true;							// If So done=TRUE
			}
			else									// If Not, Deal With Window Messages
			{
				TranslateMessage(&msg);				// Translate The Message
				DispatchMessage(&msg);				// Dispatch The Message
			}
		}
		else										// If There Are No Messages
		{
			if(keys[VK_ESCAPE])
				done = true;

			getCollision();
			processKeys();			//process keyboard

			display();					// Draw The Scene
			update();					// update variables
			SwapBuffers(hDC);				// Swap Buffers (Double Buffering)
		}
	}

	console.Close();

	// Shutdown
	KillGLWindow();									// Kill The Window
	return (int)(msg.wParam);						// Exit The Program
}

//WIN32 Processes function - useful for responding to user inputs or other events.
LRESULT CALLBACK WndProc(	HWND	hWnd,			// Handle For This Window
						 UINT	uMsg,			// Message For This Window
						 WPARAM	wParam,			// Additional Message Information
						 LPARAM	lParam)			// Additional Message Information
{
	switch (uMsg)									// Check For Windows Messages
	{
	case WM_CLOSE:								// Did We Receive A Close Message?
		{
			PostQuitMessage(0);						// Send A Quit Message
			return 0;								// Jump Back
		}
		break;

	case WM_SIZE:								// Resize The OpenGL Window
		{
			reshape(LOWORD(lParam),HIWORD(lParam));  // LoWord=Width, HiWord=Height
			return 0;								// Jump Back
		}
		break;

	case WM_LBUTTONDOWN:
		{
			mouse_x = LOWORD(lParam);          
			mouse_y = screenHeight - HIWORD(lParam);
			LeftPressed = true;
		}
		break;

	case WM_LBUTTONUP:
		{
			LeftPressed = false;
		}
		break;

	case WM_MOUSEMOVE:
		{
			mouse_x = LOWORD(lParam);          
			mouse_y = screenHeight  - HIWORD(lParam);
		}
		break;
	case WM_KEYDOWN:							// Is A Key Being Held Down?
		{
			keys[wParam] = true;					// If So, Mark It As TRUE
			return 0;								// Jump Back
		}
		break;
	case WM_KEYUP:								// Has A Key Been Released?
		{
			keys[wParam] = false;					// If So, Mark It As FALSE
			return 0;								// Jump Back
		}
		break;
	}

	// Pass All Unhandled Messages To DefWindowProc
	return DefWindowProc(hWnd,uMsg,wParam,lParam);
}

void KillGLWindow()								// Properly Kill The Window
{
	if (hRC)											// Do We Have A Rendering Context?
	{
		if (!wglMakeCurrent(NULL,NULL))					// Are We Able To Release The DC And RC Contexts?
		{
			MessageBox(NULL,"Release Of DC And RC Failed.","SHUTDOWN ERROR",MB_OK | MB_ICONINFORMATION);
		}

		if (!wglDeleteContext(hRC))						// Are We Able To Delete The RC?
		{
			MessageBox(NULL,"Release Rendering Context Failed.","SHUTDOWN ERROR",MB_OK | MB_ICONINFORMATION);
		}
		hRC=NULL;										// Set RC To NULL
	}

	if (hDC && !ReleaseDC(hWnd,hDC))					// Are We Able To Release The DC
	{
		MessageBox(NULL,"Release Device Context Failed.","SHUTDOWN ERROR",MB_OK | MB_ICONINFORMATION);
		hDC=NULL;										// Set DC To NULL
	}

	if (hWnd && !DestroyWindow(hWnd))					// Are We Able To Destroy The Window?
	{
		MessageBox(NULL,"Could Not Release hWnd.","SHUTDOWN ERROR",MB_OK | MB_ICONINFORMATION);
		hWnd=NULL;										// Set hWnd To NULL
	}

	if (!UnregisterClass("OpenGL",hInstance))			// Are We Able To Unregister Class
	{
		MessageBox(NULL,"Could Not Unregister Class.","SHUTDOWN ERROR",MB_OK | MB_ICONINFORMATION);
		hInstance=NULL;									// Set hInstance To NULL
	}
}

/*	This Code Creates Our OpenGL Window.  Parameters Are:					*
*	title			- Title To Appear At The Top Of The Window				*
*	width			- Width Of The GL Window Or Fullscreen Mode				*
*	height			- Height Of The GL Window Or Fullscreen Mode			*/

bool CreateGLWindow(char* title, int width, int height)
{
	GLuint		PixelFormat;			// Holds The Results After Searching For A Match
	WNDCLASS	wc;						// Windows Class Structure
	DWORD		dwExStyle;				// Window Extended Style
	DWORD		dwStyle;				// Window Style
	RECT		WindowRect;				// Grabs Rectangle Upper Left / Lower Right Values
	WindowRect.left=(long)0;			// Set Left Value To 0
	WindowRect.right=(long)width;		// Set Right Value To Requested Width
	WindowRect.top=(long)0;				// Set Top Value To 0
	WindowRect.bottom=(long)height;		// Set Bottom Value To Requested Height

	hInstance			= GetModuleHandle(NULL);				// Grab An Instance For Our Window
	wc.style			= CS_HREDRAW | CS_VREDRAW | CS_OWNDC;	// Redraw On Size, And Own DC For Window.
	wc.lpfnWndProc		= (WNDPROC) WndProc;					// WndProc Handles Messages
	wc.cbClsExtra		= 0;									// No Extra Window Data
	wc.cbWndExtra		= 0;									// No Extra Window Data
	wc.hInstance		= hInstance;							// Set The Instance
	wc.hIcon			= LoadIcon(NULL, IDI_WINLOGO);			// Load The Default Icon
	wc.hCursor			= LoadCursor(NULL, IDC_ARROW);			// Load The Arrow Pointer
	wc.hbrBackground	= NULL;									// No Background Required For GL
	wc.lpszMenuName		= NULL;									// We Don't Want A Menu
	wc.lpszClassName	= "OpenGL";								// Set The Class Name

	if (!RegisterClass(&wc))									// Attempt To Register The Window Class
	{
		MessageBox(NULL,"Failed To Register The Window Class.","ERROR",MB_OK|MB_ICONEXCLAMATION);
		return false;											// Return FALSE
	}

	dwExStyle=WS_EX_APPWINDOW | WS_EX_WINDOWEDGE;			// Window Extended Style
	dwStyle=WS_OVERLAPPEDWINDOW;							// Windows Style

	AdjustWindowRectEx(&WindowRect, dwStyle, FALSE, dwExStyle);		// Adjust Window To True Requested Size

	// Create The Window
	if (!(hWnd=CreateWindowEx(	dwExStyle,							// Extended Style For The Window
		"OpenGL",							// Class Name
		title,								// Window Title
		dwStyle |							// Defined Window Style
		WS_CLIPSIBLINGS |					// Required Window Style
		WS_CLIPCHILDREN,					// Required Window Style
		0, 0,								// Window Position
		WindowRect.right-WindowRect.left,	// Calculate Window Width
		WindowRect.bottom-WindowRect.top,	// Calculate Window Height
		NULL,								// No Parent Window
		NULL,								// No Menu
		hInstance,							// Instance
		NULL)))								// Dont Pass Anything To WM_CREATE
	{
		KillGLWindow();								// Reset The Display
		MessageBox(NULL,"Window Creation Error.","ERROR",MB_OK|MB_ICONEXCLAMATION);
		return false;								// Return FALSE
	}

	static	PIXELFORMATDESCRIPTOR pfd=				// pfd Tells Windows How We Want Things To Be
	{
		sizeof(PIXELFORMATDESCRIPTOR),				// Size Of This Pixel Format Descriptor
		1,											// Version Number
		PFD_DRAW_TO_WINDOW |						// Format Must Support Window
		PFD_SUPPORT_OPENGL |						// Format Must Support OpenGL
		PFD_DOUBLEBUFFER,							// Must Support Double Buffering
		PFD_TYPE_RGBA,								// Request An RGBA Format
		24,										// Select Our Color Depth
		0, 0, 0, 0, 0, 0,							// Color Bits Ignored
		0,											// No Alpha Buffer
		0,											// Shift Bit Ignored
		0,											// No Accumulation Buffer
		0, 0, 0, 0,									// Accumulation Bits Ignored
		24,											// 24Bit Z-Buffer (Depth Buffer)  
		0,											// No Stencil Buffer
		0,											// No Auxiliary Buffer
		PFD_MAIN_PLANE,								// Main Drawing Layer
		0,											// Reserved
		0, 0, 0										// Layer Masks Ignored
	};

	if (!(hDC=GetDC(hWnd)))							// Did We Get A Device Context?
	{
		KillGLWindow();								// Reset The Display
		MessageBox(NULL,"Can't Create A GL Device Context.","ERROR",MB_OK|MB_ICONEXCLAMATION);
		return false;								// Return FALSE
	}

	if (!(PixelFormat=ChoosePixelFormat(hDC,&pfd)))	// Did Windows Find A Matching Pixel Format?
	{
		KillGLWindow();								// Reset The Display
		MessageBox(NULL,"Can't Find A Suitable PixelFormat.","ERROR",MB_OK|MB_ICONEXCLAMATION);
		return false;								// Return FALSE
	}

	if(!SetPixelFormat(hDC,PixelFormat,&pfd))		// Are We Able To Set The Pixel Format?
	{
		KillGLWindow();								// Reset The Display
		MessageBox(NULL,"Can't Set The PixelFormat.","ERROR",MB_OK|MB_ICONEXCLAMATION);
		return false;								// Return FALSE
	}

	if (!(hRC=wglCreateContext(hDC)))				// Are We Able To Get A Rendering Context?
	{
		KillGLWindow();								// Reset The Display
		MessageBox(NULL,"Can't Create A GL Rendering Context.","ERROR",MB_OK|MB_ICONEXCLAMATION);
		return false;								// Return FALSE
	}

	if(!wglMakeCurrent(hDC,hRC))					// Try To Activate The Rendering Context
	{
		KillGLWindow();								// Reset The Display
		MessageBox(NULL,"Can't Activate The GL Rendering Context.","ERROR",MB_OK|MB_ICONEXCLAMATION);
		return false;								// Return FALSE
	}

	ShowWindow(hWnd,SW_SHOW);						// Show The Window
	SetForegroundWindow(hWnd);						// Slightly Higher Priority
	SetFocus(hWnd);									// Sets Keyboard Focus To The Window
	reshape(width, height);					// Set Up Our Perspective GL Screen

	init();

	return true;									// Success
}




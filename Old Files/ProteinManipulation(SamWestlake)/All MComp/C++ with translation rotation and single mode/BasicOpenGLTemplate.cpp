
//OpenGL 2.0 - Loads Proteins from Files on the Protein Data Bank

//Author: Stephen Laycock
//Copyright (C) 2011 - Stephen Laycock, University of East Anglia
//Source Code For Educational Purposes


#include <windows.h>		// Header File For Windows
#include "gl/glew.h"
#include <math.h>
#include "console.h"

#include "PDBLoading\FileHandle.h"
#include "ProteinRendering\Atoms.h"

#include "ArcBall.h"

AtomList theList;
AtomList theList2;
Atom** myListPtr;
Atom** myListPtr2;
int myListSize=0;
int myListSize2=0;
float radius_p1;
float radius_p2;
bool drawHydrogens = false;

ConsoleWindow console;

#include <iostream>
using namespace std;

__int64 startTime, now;
__int64 ticksPerSecond;

int screenWidth=640, screenHeight=480;
bool keys[256];
double mouse_x, mouse_y;
int clickHold = 0;
float ZOOM = -200.0;
float MOVE = 0.0;

// ARCBALL VARIABLES

// Protein 1
// Final Rotation
Matrix4fT Rotation_p1 = {1.0f, 0.0f, 0.0f, 0.0f,
                          0.0f, 1.0f, 0.0f, 0.0f,
                          0.0f, 0.0f, 1.0f, 0.0f,
                          0.0f, 0.0f, 0.0f, 1.0f };
 
// Last Rotation
Matrix3fT LastRot_p1 = {1.0f, 0.0f, 0.0f,
                        0.0f, 1.0f, 0.0f,
                        0.0f, 0.0f, 1.0f};
 
// This Rotation
Matrix3fT ThisRot_p1 = {1.0f, 0.0f, 0.0f,                  
                        0.0f, 1.0f, 0.0f,
						0.0f, 0.0f, 1.0f};

// Protein 2
// Final Rotation
Matrix4fT Rotation_p2 = {1.0f, 0.0f, 0.0f, 0.0f,
                          0.0f, 1.0f, 0.0f, 0.0f,
                          0.0f, 0.0f, 1.0f, 0.0f,
                          0.0f, 0.0f, 0.0f, 1.0f };
 
// Last Rotation
Matrix3fT LastRot_p2 = {1.0f, 0.0f, 0.0f,
                        0.0f, 1.0f, 0.0f,
                        0.0f, 0.0f, 1.0f};
 
// This Rotation
Matrix3fT ThisRot_p2 = {1.0f, 0.0f, 0.0f,                  
                        0.0f, 1.0f, 0.0f,
						0.0f, 0.0f, 1.0f};
 
ArcBallT    ArcBall(640.0f, 480.0f);                        // ArcBall Instance
Point2fT    MousePt;                                // Current Mouse Point
bool        isClicked  = false;                         // Clicking The Mouse?
bool        isRClicked = false;                         // Clicking The Right Mouse Button?
bool        isDragging = false;                         // Dragging The Mouse?

// TRANSLATION VARIABLES

//Protein 1
Matrix4fT Translation_p1 = {1.0f, 0.0f, 0.0f, 0.0f,
                            0.0f, 1.0f, 0.0f, 0.0f,
							0.0f, 0.0f, 1.0f, 0.0f,
							-400.0f, 0.0f, ZOOM, 1.0f };

Matrix3fT LastTrans_p1 = {1.0f, 0.0f, 0.0f,
                          0.0f, 1.0f, 0.0f,
                          0.0f, 0.0f, 1.0f };
 
Matrix3fT ThisTrans_p1 = {1.0f, 0.0f, 0.0f,                  
                          0.0f, 1.0f, 0.0f,
                          0.0f, 0.0f, 1.0f };

//Protein 2
Matrix4fT Translation_p2 = {1.0f, 0.0f, 0.0f, 0.0f,
							0.0f, 1.0f, 0.0f, 0.0f,
							0.0f, 0.0f, 1.0f, 0.0f,
							400.0f, 0.0f, ZOOM, 1.0f };

Matrix3fT LastTrans_p2 = {1.0f, 0.0f, 0.0f,
                          0.0f, 1.0f, 0.0f,
                          0.0f, 0.0f, 1.0f };
 
Matrix3fT ThisTrans_p2 = {1.0f, 0.0f, 0.0f,                  
                          0.0f, 1.0f, 0.0f,
                          0.0f, 0.0f, 1.0f };

float begin_mouse_x;
float begin_mouse_y;
int viewport[4];
double modelviewmatrix[16];
double projectionmatrix[16];
double projX;
double projY;
double projZ;
double unprojX;
double unprojY;
double unprojZ;

//OPENGL FUNCTION PROTOTYPES
void display();				//called in winmain to draw everything to the screen
void reshape();				//called when the window is resized
void init();				//called in winmain when the program starts.
void processKeys();         //called in winmain to process keyboard input
void update();				//called in winmain to update variables

void drawCube();			//draws a cube of unit size to the screen, with centre at the origin.
void renderProtein(int p);
bool closerToP1 (void);
void project(GLdouble x, GLdouble y, GLdouble z);
void unproject(GLdouble x, GLdouble y, GLdouble z);
void singleProteinMode();

/*************    START OF OPENGL FUNCTIONS   ****************/

GLfloat position[] = { 0.0f, 0.0f, 0.0f, 1.0f };

bool first = false;

void display()									
{
	/*
	static int count = 0;
	
	if(first)
		QueryPerformanceCounter((LARGE_INTEGER*)&startTime);
	*/

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	//gluLookAt(0,0,ZOOM,0,0,0,0,1,0);

	/*
	glPushMatrix();
	//set light positions for scene
	glLightfv(GL_LIGHT0, GL_POSITION, position);
	//glCallList(SuperAtom::sphereDisplayList);
	glPopMatrix();
	*/

	glPushMatrix();
	//move molecule
	glMultMatrixf(Translation_p1.M);
	//spin molecule
	glMultMatrixf(Rotation_p1.M);
	//render protein
	renderProtein(1);
	glPopMatrix();

	glPushMatrix();
	//move molecule
	glMultMatrixf(Translation_p2.M);
	//spin molecule
	glMultMatrixf(Rotation_p2.M);
	//render molecule
	renderProtein(2);
	glPopMatrix();

	//unproject(0,0,0);
	//cout << unprojX << ", " << unprojY << ", " << unprojZ << endl;

	glFlush();	
	
	/*
	QueryPerformanceCounter((LARGE_INTEGER*)&now);
	if(((float(now - startTime) / float(ticksPerSecond))) > 1)
	{
		cout << "Frames per second: " << count << endl;
		count = 0;
		QueryPerformanceCounter((LARGE_INTEGER*)&startTime);
	}
	count++;
	*/
}

void renderProtein(int p)
{
	glPushMatrix();
	glEnable(GL_NORMALIZE);
	int listSize;
	if(p == 1)
		listSize = myListSize;
	else
		listSize = myListSize2;
	for(int i = 0; i < listSize; i++)
	{
		Atom* temp;
		if(p == 1)
			temp = myListPtr[i];
		else
			temp = myListPtr2[i];

		glPushMatrix();

		if( (!temp->isWater()) && (temp->getEle().compare("H") != 0) ) //not water or hydrogen
		{
			temp->renderSimple();
		}
		else if((temp->getEle().compare("H") == 0) && drawHydrogens) //can add hydrogen here!
		{
			temp->renderSimple();
		}
		glPopMatrix();

		//POSITION OF ATOM: temp->getMyLocalPosition().x, temp->getMyLocalPosition().y, temp->getMyLocalPosition().z
		//RADIUS OF ATOM: temp->getRadius();
	}
	glDisable(GL_NORMALIZE);
	glPopMatrix();
}

void reshape(int width, int height)		// Resize the OpenGL window
{
	screenWidth=width; screenHeight = height;           // to ensure the mouse coordinates match 
	// we will use these values to set the coordinate system

	ArcBall.setBounds((GLfloat)width, (GLfloat)height);

	glViewport(0,0,width,height);						// Reset The Current Viewport

	glMatrixMode(GL_PROJECTION);						// Select The Projection Matrix
	glLoadIdentity();									// Reset The Projection Matrix

	//set up a perspective view (fov, aspect ratio, near, far)
	gluPerspective(45.0f,(GLfloat)width/(GLfloat)height,0.1f,4000.0f);

	glMatrixMode(GL_MODELVIEW);							// Select The Modelview Matrix
	glLoadIdentity();									// Reset The Modelview Matrix
}
void init()
{
	console.Open();

	QueryPerformanceFrequency((LARGE_INTEGER*)&ticksPerSecond);

	glClearColor(0.0,0.0,0.0,0.0);						//sets the clear colour to yellow
	glEnable(GL_DEPTH_TEST);				//Enables depth testing - z-buffer is used to store depths of pixels.
	
	//cout << "Loading file..." <<endl;
	FileHandle file_handle;
	FileHandle file_handle2;
	file_handle.openFile("PDB Test Files\\1CRN(327).pdb");
	file_handle2.openFile("PDB Test Files\\5ADH(3127).pdb");
	//cout << "PDB loaded" << endl;

	
	SuperAtom::hapticRadius = 0;

	theList = file_handle.getAtomList();
	theList2 = file_handle2.getAtomList();
	myListPtr = &theList[0];
	myListPtr2 = &theList2[0];
	myListSize = theList.size();
	myListSize2 = theList2.size();

	cout << "The first file contains "<< myListSize << " atoms. " << endl;
	cout << "The second file contains "<< myListSize2 << " atoms. " << endl;

	//ZOOM = file_handle.getFurthestDistanceToMidPoint();
	//cout << "RADIUS OF BOUNDING CIRCLE " << ZOOM << endl;

	radius_p1 = file_handle.getFurthestDistanceToMidPoint();
	radius_p2 = file_handle2.getFurthestDistanceToMidPoint();

	cout << "The radius of the first molecule is " << radius_p1 << endl;
	cout << "The radius of the second molecule is " << radius_p2 << endl;

	//find radius of largest atom
	float radius=0;
	for(int i = 0; i < myListSize; i++)
	{
		Atom* temp = myListPtr[i];
		float r = temp->getRadius();
		if(r > radius)
			radius = r;
	}

	//ZOOM the molecule back 
	//ZOOM = (ZOOM / tan((45*0.5)*(3.141/180.0))) + radius;

	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);

	glShadeModel(GL_SMOOTH);							// Enable smooth shading
	glClearColor(1.0f, 1.0f, 1.0f, 1.0f);				// White background
	glEnable(GL_DEPTH_TEST);							// Enables depth testing
	glDepthFunc(GL_LEQUAL);								// The type of depth testing to do
	
	
	GLfloat ambientLight[] = { 0.3f, 0.3f, 0.3f, 1.0f };
	GLfloat diffuseLight[] = { 0.6,0.6,0.6, 1.0f };
	GLfloat specularLight[] = { 1.0f, 1.0f, 1.0f, 1.0f };

	glLightfv(GL_LIGHT0, GL_AMBIENT, ambientLight);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, diffuseLight);
	glLightfv(GL_LIGHT0, GL_SPECULAR, specularLight);
	
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);

	glMaterialf(GL_FRONT, GL_SHININESS, 80);

	SuperAtom::makeList();
}
void processKeys()
{	
	if (keys[VK_SPACE])                                     // If Space is pressed, reset all rotations
	{
	    // Reset Rotation
		Matrix3fSetIdentity(&LastRot_p1);
		Matrix3fSetIdentity(&LastRot_p2);
 
	    // Reset Rotation
	    Matrix3fSetIdentity(&ThisRot_p1);
		Matrix3fSetIdentity(&ThisRot_p2);
	 
	    // Reset Rotation
	    Matrix4fSetRotationFromMatrix3f(&Rotation_p1, &ThisRot_p1);
		Matrix4fSetRotationFromMatrix3f(&Rotation_p2, &ThisRot_p2);

		// Reset Translation
		LastTrans_p1.M[2] = 0;
		LastTrans_p1.M[5] = 0;
		LastTrans_p2.M[2] = 0;
		LastTrans_p2.M[5] = 0;
 
	    // Reset Translation
	    ThisTrans_p1.M[2] = 0;
		ThisTrans_p1.M[5] = 0;
		ThisTrans_p2.M[2] = 0;
		ThisTrans_p2.M[5] = 0;
	 
	    // Reset Translation
	    Translation_p1.M[3] = 0;
		Translation_p1.M[7] = 0;
		Translation_p2.M[3] = 0;
		Translation_p2.M[7] = 0;
	}
}
void update() {												// Rotation not Translation
		if(closerToP1() == true) {
			if (!isDragging)                                    // Not Dragging
			{
			    if (isClicked)                                  // First Click
			    {
			        isDragging = true;                          // Prepare For Dragging
			        LastRot_p1 = ThisRot_p1;                          // Set Last Static Rotation To Last Dynamic One
			        ArcBall.click(&MousePt);                        // Update Start Vector And Prepare For Dragging
			    }
			}
			else
			{
			    if (isClicked)  //Still clicked, so still dragging
			    {
			        Quat4fT     ThisQuat;
			 
			        ArcBall.drag(&MousePt, &ThisQuat);                  // Update End Vector And Get Rotation As Quaternion
			        Matrix3fSetRotationFromQuat4f(&ThisRot_p1, &ThisQuat);         // Convert Quaternion Into Matrix3fT
			        Matrix3fMulMatrix3f(&ThisRot_p1, &LastRot_p1);                // Accumulate Last Rotation Into This One
			        Matrix4fSetRotationFromMatrix3f(&Rotation_p1, &ThisRot_p1);          // Set Our Final Transform's Rotation From This One
			    }
			    else                                        // No Longer Dragging
			        isDragging = false;
			}
		} else {
			if (!isDragging)                                    // Not Dragging
			{
			    if (isClicked)                                  // First Click
			    {
			        isDragging = true;                          // Prepare For Dragging
			        LastRot_p2 = ThisRot_p2;                          // Set Last Static Rotation To Last Dynamic One
			        ArcBall.click(&MousePt);                        // Update Start Vector And Prepare For Dragging
			    }
			}
			else
			{
			    if (isClicked)  //Still clicked, so still dragging
			    {
			        Quat4fT     ThisQuat;
			 
			        ArcBall.drag(&MousePt, &ThisQuat);                  // Update End Vector And Get Rotation As Quaternion
			        Matrix3fSetRotationFromQuat4f(&ThisRot_p2, &ThisQuat);         // Convert Quaternion Into Matrix3fT
			        Matrix3fMulMatrix3f(&ThisRot_p2, &LastRot_p2);                // Accumulate Last Rotation Into This One
			        Matrix4fSetRotationFromMatrix3f(&Rotation_p2, &ThisRot_p2);          // Set Our Final Transform's Rotation From This One
			    }
			    else                                        // No Longer Dragging
			        isDragging = false;
			}
		}

	//Update Z-pos of proteins
	Translation_p1.M[14] = ZOOM;
	Translation_p2.M[14] = ZOOM;

	if(isClicked && isRClicked)
		clickHold++;
	else
		clickHold = 0;

	if(clickHold > 99)
		singleProteinMode();
}

bool closerToP1(void) {
	project(Translation_p1.M[12], Translation_p1.M[13], Translation_p1.M[14]);
	double p1_x = projX;
	double p1_y = projY;
	project(Translation_p2.M[12], Translation_p2.M[13], Translation_p2.M[14]);
	double p2_x = projX;
	double p2_y = projY;
	float l1 = (mouse_x - p1_x) * (mouse_x - p1_x) + (mouse_y - p1_y) * (mouse_y - p1_y);
	float l2 = (mouse_x - p2_x) * (mouse_x - p2_x) + (mouse_y - p2_y) * (mouse_y - p2_y);

	//cout << "Mouse x: " << mouse_x << ", y: " << mouse_y << ".    l1: " << l1 << ", l2 " << l2 << endl;
	//cout << "Mouse: " << mouse_x << ", " << mouse_y << ".  P1: " << p1_x << ", " << p1_y << ".  P2: " << p2_x << ", " << p2_y<< endl;

	if(l1 < l2) {
		return true;
	} else {
		return false;
	}
}

void project(double x, double y, double z) {
	glGetIntegerv(GL_VIEWPORT, viewport);
	glGetDoublev(GL_MODELVIEW_MATRIX, modelviewmatrix);
	glGetDoublev(GL_PROJECTION_MATRIX, projectionmatrix);
	gluProject(x, y, z, modelviewmatrix, projectionmatrix, viewport, &projX, &projY, &projZ);
}

void unproject(double x, double y, double z) {
	glGetIntegerv(GL_VIEWPORT, viewport);
	glGetDoublev(GL_MODELVIEW_MATRIX, modelviewmatrix);
	glGetDoublev(GL_PROJECTION_MATRIX, projectionmatrix);
	gluUnProject(x, y, z, modelviewmatrix, projectionmatrix, viewport, &unprojX, &unprojY, &unprojZ);
}

void singleProteinMode() {
	cout << "Enter Single Protein Mode!" << endl;
	while(isRClicked) {}
	bool p1 = closerToP1();
	if(p1) {
		//Zoom into p1
	} else {
		//Zoom into p2
	}
	while(!isRClicked) {
		//Allow rotation of p1 or p2
		//Allow for marking amino chains
	}
	// Reset protein positions
	cout << "Exit Single Protein Mode!" << endl;
}

/**************** END OPENGL FUNCTIONS *************************/
//WIN32 functions
LRESULT	CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);	// Declaration For WndProc
void KillGLWindow();									// releases and destroys the window
bool CreateGLWindow(char* title, int width, int height); //creates the window
int WINAPI WinMain(	HINSTANCE, HINSTANCE, LPSTR, int);  // Win32 main function

//win32 global variabless
HDC			hDC=NULL;		// Private GDI Device Context
HGLRC		hRC=NULL;		// Permanent Rendering Context
HWND		hWnd=NULL;		// Holds Our Window Handle
HINSTANCE	hInstance;		// Holds The Instance Of The Application


/******************* WIN32 FUNCTIONS ***************************/
int WINAPI WinMain(	HINSTANCE	hInstance,			// Instance
	HINSTANCE	hPrevInstance,		// Previous Instance
	LPSTR		lpCmdLine,			// Command Line Parameters
	int			nCmdShow)			// Window Show State
{
	console.Open();

	MSG		msg;									// Windows Message Structure
	bool	done=false;								// Bool Variable To Exit Loop

	// Create Our OpenGL Window
	if (!CreateGLWindow("OpenGL Win32 Example",screenWidth,screenHeight))
	{
		return 0;									// Quit If Window Was Not Created
	}

	while(!done)									// Loop That Runs While done=FALSE
	{
		if (PeekMessage(&msg,NULL,0,0,PM_REMOVE))	// Is There A Message Waiting?
		{
			if (msg.message==WM_QUIT)				// Have We Received A Quit Message?
			{
				done=true;							// If So done=TRUE
			}
			else									// If Not, Deal With Window Messages
			{
				TranslateMessage(&msg);				// Translate The Message
				DispatchMessage(&msg);				// Dispatch The Message
			}
		}
		else										// If There Are No Messages
		{
			if(keys[VK_ESCAPE])
				done = true;

			processKeys();			//process keyboard

			display();					// Draw The Scene
			update();					// update variables
			SwapBuffers(hDC);				// Swap Buffers (Double Buffering)
		}
	}

	console.Close();

	// Shutdown
	KillGLWindow();									// Kill The Window
	return (int)(msg.wParam);						// Exit The Program
}

//WIN32 Processes function - useful for responding to user inputs or other events.
LRESULT CALLBACK WndProc(	HWND	hWnd,			// Handle For This Window
	UINT	uMsg,			// Message For This Window
	WPARAM	wParam,			// Additional Message Information
	LPARAM	lParam)			// Additional Message Information
{
	switch (uMsg)									// Check For Windows Messages
	{
	case WM_CLOSE:								// Did We Receive A Close Message?
		{
			PostQuitMessage(0);						// Send A Quit Message
			return 0;								// Jump Back
		}
		break;

	case WM_SIZE:								// Resize The OpenGL Window
		{
			reshape(LOWORD(lParam),HIWORD(lParam));  // LoWord=Width, HiWord=Height
			return 0;								// Jump Back
		}
		break;

	case WM_LBUTTONDOWN:
		{
	        mouse_x = LOWORD(lParam);          
			mouse_y = screenHeight - HIWORD(lParam);
			isClicked = true;
		}
		break;

	case WM_LBUTTONUP:
		{
		    isClicked = false;
		}
		break;

	case WM_RBUTTONDOWN:
		{
			isRClicked = true;
		}
		break;

	case WM_RBUTTONUP:
		{
			isRClicked = false;
		}
		break;
	case WM_MOUSEMOVE:
		{
			mouse_x = LOWORD(lParam);          
			mouse_y = screenHeight  - HIWORD(lParam);
			MousePt.s.X = LOWORD(lParam);
			MousePt.s.Y = HIWORD(lParam);
			if(isRClicked) {
				unproject(mouse_x, mouse_y, projZ);
				if(closerToP1() == true) {
					Translation_p1.M[12] = unprojX;
					Translation_p1.M[13] = unprojY;
				} else {
					Translation_p2.M[12] = unprojX;
					Translation_p2.M[13] = unprojY;
				}
			}
		}
	break;
	case WM_KEYDOWN:							// Is A Key Being Held Down?
		{
			keys[wParam] = true;					// If So, Mark It As TRUE
			return 0;								// Jump Back
		}
		break;
	case WM_KEYUP:								// Has A Key Been Released?
		{
			keys[wParam] = false;					// If So, Mark It As FALSE
			return 0;								// Jump Back
		}
		break;
	case WM_MOUSEWHEEL:
		{
			if(GET_WHEEL_DELTA_WPARAM(wParam) > 0)
					ZOOM += 200;
				else
					ZOOM -= 200;
		}
	}

	// Pass All Unhandled Messages To DefWindowProc
	return DefWindowProc(hWnd,uMsg,wParam,lParam);
}

void KillGLWindow()								// Properly Kill The Window
{
	if (hRC)											// Do We Have A Rendering Context?
	{
		if (!wglMakeCurrent(NULL,NULL))					// Are We Able To Release The DC And RC Contexts?
		{
			MessageBox(NULL,"Release Of DC And RC Failed.","SHUTDOWN ERROR",MB_OK | MB_ICONINFORMATION);
		}

		if (!wglDeleteContext(hRC))						// Are We Able To Delete The RC?
		{
			MessageBox(NULL,"Release Rendering Context Failed.","SHUTDOWN ERROR",MB_OK | MB_ICONINFORMATION);
		}
		hRC=NULL;										// Set RC To NULL
	}

	if (hDC && !ReleaseDC(hWnd,hDC))					// Are We Able To Release The DC
	{
		MessageBox(NULL,"Release Device Context Failed.","SHUTDOWN ERROR",MB_OK | MB_ICONINFORMATION);
		hDC=NULL;										// Set DC To NULL
	}

	if (hWnd && !DestroyWindow(hWnd))					// Are We Able To Destroy The Window?
	{
		MessageBox(NULL,"Could Not Release hWnd.","SHUTDOWN ERROR",MB_OK | MB_ICONINFORMATION);
		hWnd=NULL;										// Set hWnd To NULL
	}

	if (!UnregisterClass("OpenGL",hInstance))			// Are We Able To Unregister Class
	{
		MessageBox(NULL,"Could Not Unregister Class.","SHUTDOWN ERROR",MB_OK | MB_ICONINFORMATION);
		hInstance=NULL;									// Set hInstance To NULL
	}
}

/*	This Code Creates Our OpenGL Window.  Parameters Are:					*
*	title			- Title To Appear At The Top Of The Window				*
*	width			- Width Of The GL Window Or Fullscreen Mode				*
*	height			- Height Of The GL Window Or Fullscreen Mode			*/

bool CreateGLWindow(char* title, int width, int height)
{
	GLuint		PixelFormat;			// Holds The Results After Searching For A Match
	WNDCLASS	wc;						// Windows Class Structure
	DWORD		dwExStyle;				// Window Extended Style
	DWORD		dwStyle;				// Window Style
	RECT		WindowRect;				// Grabs Rectangle Upper Left / Lower Right Values
	WindowRect.left=(long)0;			// Set Left Value To 0
	WindowRect.right=(long)width;		// Set Right Value To Requested Width
	WindowRect.top=(long)0;				// Set Top Value To 0
	WindowRect.bottom=(long)height;		// Set Bottom Value To Requested Height

	hInstance			= GetModuleHandle(NULL);				// Grab An Instance For Our Window
	wc.style			= CS_HREDRAW | CS_VREDRAW | CS_OWNDC;	// Redraw On Size, And Own DC For Window.
	wc.lpfnWndProc		= (WNDPROC) WndProc;					// WndProc Handles Messages
	wc.cbClsExtra		= 0;									// No Extra Window Data
	wc.cbWndExtra		= 0;									// No Extra Window Data
	wc.hInstance		= hInstance;							// Set The Instance
	wc.hIcon			= LoadIcon(NULL, IDI_WINLOGO);			// Load The Default Icon
	wc.hCursor			= LoadCursor(NULL, IDC_ARROW);			// Load The Arrow Pointer
	wc.hbrBackground	= NULL;									// No Background Required For GL
	wc.lpszMenuName		= NULL;									// We Don't Want A Menu
	wc.lpszClassName	= "OpenGL";								// Set The Class Name

	if (!RegisterClass(&wc))									// Attempt To Register The Window Class
	{
		MessageBox(NULL,"Failed To Register The Window Class.","ERROR",MB_OK|MB_ICONEXCLAMATION);
		return false;											// Return FALSE
	}

	dwExStyle=WS_EX_APPWINDOW | WS_EX_WINDOWEDGE;			// Window Extended Style
	dwStyle=WS_OVERLAPPEDWINDOW;							// Windows Style

	AdjustWindowRectEx(&WindowRect, dwStyle, FALSE, dwExStyle);		// Adjust Window To True Requested Size

	// Create The Window
	if (!(hWnd=CreateWindowEx(	dwExStyle,							// Extended Style For The Window
		"OpenGL",							// Class Name
		title,								// Window Title
		dwStyle |							// Defined Window Style
		WS_CLIPSIBLINGS |					// Required Window Style
		WS_CLIPCHILDREN,					// Required Window Style
		0, 0,								// Window Position
		WindowRect.right-WindowRect.left,	// Calculate Window Width
		WindowRect.bottom-WindowRect.top,	// Calculate Window Height
		NULL,								// No Parent Window
		NULL,								// No Menu
		hInstance,							// Instance
		NULL)))								// Dont Pass Anything To WM_CREATE
	{
		KillGLWindow();								// Reset The Display
		MessageBox(NULL,"Window Creation Error.","ERROR",MB_OK|MB_ICONEXCLAMATION);
		return false;								// Return FALSE
	}

	static	PIXELFORMATDESCRIPTOR pfd=				// pfd Tells Windows How We Want Things To Be
	{
		sizeof(PIXELFORMATDESCRIPTOR),				// Size Of This Pixel Format Descriptor
		1,											// Version Number
		PFD_DRAW_TO_WINDOW |						// Format Must Support Window
		PFD_SUPPORT_OPENGL |						// Format Must Support OpenGL
		PFD_DOUBLEBUFFER,							// Must Support Double Buffering
		PFD_TYPE_RGBA,								// Request An RGBA Format
		24,										// Select Our Color Depth
		0, 0, 0, 0, 0, 0,							// Color Bits Ignored
		0,											// No Alpha Buffer
		0,											// Shift Bit Ignored
		0,											// No Accumulation Buffer
		0, 0, 0, 0,									// Accumulation Bits Ignored
		24,											// 24Bit Z-Buffer (Depth Buffer)  
		0,											// No Stencil Buffer
		0,											// No Auxiliary Buffer
		PFD_MAIN_PLANE,								// Main Drawing Layer
		0,											// Reserved
		0, 0, 0										// Layer Masks Ignored
	};

	if (!(hDC=GetDC(hWnd)))							// Did We Get A Device Context?
	{
		KillGLWindow();								// Reset The Display
		MessageBox(NULL,"Can't Create A GL Device Context.","ERROR",MB_OK|MB_ICONEXCLAMATION);
		return false;								// Return FALSE
	}

	if (!(PixelFormat=ChoosePixelFormat(hDC,&pfd)))	// Did Windows Find A Matching Pixel Format?
	{
		KillGLWindow();								// Reset The Display
		MessageBox(NULL,"Can't Find A Suitable PixelFormat.","ERROR",MB_OK|MB_ICONEXCLAMATION);
		return false;								// Return FALSE
	}

	if(!SetPixelFormat(hDC,PixelFormat,&pfd))		// Are We Able To Set The Pixel Format?
	{
		KillGLWindow();								// Reset The Display
		MessageBox(NULL,"Can't Set The PixelFormat.","ERROR",MB_OK|MB_ICONEXCLAMATION);
		return false;								// Return FALSE
	}

	if (!(hRC=wglCreateContext(hDC)))				// Are We Able To Get A Rendering Context?
	{
		KillGLWindow();								// Reset The Display
		MessageBox(NULL,"Can't Create A GL Rendering Context.","ERROR",MB_OK|MB_ICONEXCLAMATION);
		return false;								// Return FALSE
	}

	if(!wglMakeCurrent(hDC,hRC))					// Try To Activate The Rendering Context
	{
		KillGLWindow();								// Reset The Display
		MessageBox(NULL,"Can't Activate The GL Rendering Context.","ERROR",MB_OK|MB_ICONEXCLAMATION);
		return false;								// Return FALSE
	}

	ShowWindow(hWnd,SW_SHOW);						// Show The Window
	SetForegroundWindow(hWnd);						// Slightly Higher Priority
	SetFocus(hWnd);									// Sets Keyboard Focus To The Window
	reshape(width, height);					// Set Up Our Perspective GL Screen

	init();

	return true;									// Success
}




﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;

namespace Molecular_Docking_Application
{
    public class RenderSphere
    {
        //Rendering Data
        const float INITIAL_SCALE = 10.0f;
        float hapticRadius = 0;//(1.52f * INITIAL_SCALE);
        int sphereDisplayList = 0;
        bool beenDrawn;

        static float theta1;
        static float theta2;
        static float theta3;

        static float ex;
        static float ey;
        static float ez;

        static float px;
        static float py;
        static float pz;

        float theDarklight;

        int x;
        int y;
        int z;

        float origStartingRadius;

        int myPositionInOriginalList;

        static int[] triIndices;
        static float[] vertices;
        static float[] normals;
        static int NumberOfTriangles;
        const float TWOPI = (float)(2.0*Math.PI);
        const float PIDIV2 = (float)(Math.PI/2.0);

        public RenderSphere()
        {
            theDarklight = 1.0f;
	        beenDrawn = false;
        }

        public void InitialiseLighting()
        {
            GL.Enable(EnableCap.DepthTest);				//Enables depth testing - z-buffer is used to store depths of pixels.
            GL.Enable(EnableCap.CullFace);
            GL.CullFace(CullFaceMode.Back);
            GL.ShadeModel(ShadingModel.Smooth);					// Enable smooth shading
            GL.ClearColor(1.0f, 1.0f, 1.0f, 1.0f);				// White background
            GL.DepthFunc(DepthFunction.Lequal);					// The type of depth testing to do

            float[] ambientLight = new float[] { 0.3f, 0.3f, 0.3f, 1.0f };
            float[] diffuseLight = new float[] { 0.6f, 0.6f, 0.6f, 1.0f };
            float[] specularLight = new float[] { 1.0f, 1.0f, 1.0f, 1.0f };


            GL.Light(LightName.Light0, LightParameter.Ambient, ambientLight);
            GL.Light(LightName.Light0, LightParameter.Diffuse, diffuseLight);
            GL.Light(LightName.Light0, LightParameter.Specular, specularLight);

            GL.Enable(EnableCap.Light0);
            GL.Enable(EnableCap.Lighting);

            GL.Material(MaterialFace.Front, MaterialParameter.Shininess, 90);
        }

        public void makeLists(int quality)
        {
            setUpDrawElements(0.0f, 0.0f, 0.0f, 1.0f, quality);
            sphereDisplayList = GL.GenLists(1);
            GL.NewList(sphereDisplayList, ListMode.Compile);
                renderSphereDrawElements();
            GL.EndList();
            float[] spec = new float[] { 1.0f, 1.0f, 1.0f, 1.0f };
            GL.Material(MaterialFace.FrontAndBack, MaterialParameter.Specular, spec);
        }

        public void renderSphereDrawElements( )
        {
	        GL.EnableClientState(ArrayCap.NormalArray);
	        GL.EnableClientState(ArrayCap.VertexArray);
	        GL.NormalPointer(NormalPointerType.Float, 0, normals);
	        GL.VertexPointer(3, VertexPointerType.Float, 0, vertices);
	        GL.DrawElements(PrimitiveType.TriangleStrip, NumberOfTriangles, DrawElementsType.UnsignedInt, triIndices);
            GL.DisableClientState(ArrayCap.VertexArray);
            GL.DisableClientState(ArrayCap.NormalArray);
        }

        public void renderSphere(float x, float y, float z, float colourR, float colourG, float colourB, float radius)
        {
            //float colourR = 135 / 255f;
            //float colourG = 206 / 255f;
            //float colourB = 250 / 255f;
            float[] myDiffuseColour = new float[4];
            float[] myAmbientColour = new float[4];
            bool inContact = false;

            myAmbientColour[0] = (float)(colourR * 0.6);
            myAmbientColour[1] = (float)(colourG * 0.6);
            myAmbientColour[2] = (float)(colourB * 0.6);
            myAmbientColour[3] = (float)(1);
            myDiffuseColour[0] = colourR;
            myDiffuseColour[1] = colourG;
            myDiffuseColour[2] = colourB;
            myDiffuseColour[3] = 1;

            GL.PushMatrix();
            GL.Material(MaterialFace.FrontAndBack, MaterialParameter.Ambient, myAmbientColour);

            if (inContact == true)
            {
                float[] myColour = new float[3] { 255, 250, 0 };
                GL.Material(MaterialFace.FrontAndBack, MaterialParameter.Diffuse, myColour);
            }
            else
            {
                GL.Material(MaterialFace.FrontAndBack, MaterialParameter.Diffuse, myDiffuseColour);
            }

            GL.Translate(x, y, z);
            GL.Scale(radius-hapticRadius, radius-hapticRadius, radius-hapticRadius);
            GL.CallList(sphereDisplayList);
            GL.PopMatrix();
        }

    public void setUpDrawElements( float cx, float cy, float cz, float r, int p )
    {
	normals = new float[(p/2)*(p+1)*6];
	vertices = new float[(p/2)*(p+1)*6];
	triIndices = new int[(p/2)*(p+1)*2];
	int counter = 0;
	NumberOfTriangles = 0;

	for( int i = 0; i < p/2; ++i )
	{
        theta1 = i * TWOPI / p - PIDIV2;
        theta2 = (i + 1) * TWOPI / p - PIDIV2;

		for( int j = 0; j <= p; ++j )
		{

			theta3 = j * TWOPI / p;

			ex = (float)(Math.Cos(theta1) * Math.Cos(theta3));
			ey = (float)Math.Sin(theta1);
			ez = (float)(Math.Cos(theta1) * Math.Sin(theta3));
			px = cx + r * ex;
			py = cy + r * ey;
			pz = cz + r * ez;

			normals[counter] = ex;
			normals[counter+1] = ey;
			normals[counter+2] = ez;
	
			vertices[counter] = px;
			vertices[counter+1] = py;
			vertices[counter+2] = pz;

			triIndices[NumberOfTriangles++] = counter/3;

			counter += 3;

			ex = (float)(Math.Cos(theta2) * Math.Cos(theta3));
			ey = (float)Math.Sin(theta2);
			ez = (float)(Math.Cos(theta2) * Math.Sin(theta3));
			px = cx + r * ex;
			py = cy + r * ey;
			pz = cz + r * ez;

			normals[counter] = ex;
			normals[counter+1] = ey;
			normals[counter+2] = ez;
			
			vertices[counter] = px;
			vertices[counter+1] = py;
			vertices[counter+2] = pz;

			triIndices[NumberOfTriangles++] = counter/3;

			counter += 3;
		}
	}
}
    }
}

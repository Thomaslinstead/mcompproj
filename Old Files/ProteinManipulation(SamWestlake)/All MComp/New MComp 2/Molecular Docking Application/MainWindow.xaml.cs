﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Runtime.InteropServices;
using System.Drawing;
using System.Drawing.Imaging;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using OpenTK.Input;
using System.Windows.Forms;
using System.Diagnostics;
using System.Threading.Tasks;
using NAudio;
using NAudio.Wave;
using HelixToolkit.Wpf;
using System.Windows.Media.Media3D;

namespace Molecular_Docking_Application
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        //Mouse states.
        MouseState current = OpenTK.Input.Mouse.GetState();
        MouseState previous = OpenTK.Input.Mouse.GetState();

        //GUI Variables
        bool vsync = true;
        int quality = 32;
        bool colliding = false;
        bool qualitychange = false;
        int[] Positions = new int[] { -400, 0, 0, 400, 0, 0, 0 }; // Array of molecule position data, increase for additional molecules
        enum Protein { x, y, z, x2, y2, z2, active };        // Enum type for indexing references into array
        RenderSphere p = new RenderSphere();
        SineWave sineWaveProvider = new SineWave();
        Stopwatch sw = new Stopwatch();
        bool MainWinActive;

        //Force viewer objects.
        //GeometryModel3D mygeometry = new GeometryModel3D();
        Model3DGroup modelGroup = new Model3DGroup();
        

        int t = 0;
        double accumulator = 0;
        int idleCounter = 0;
        double movementSpeed = 5;
        bool showforces = false;
        bool hearforces = false;

        private GLControl glc;
        private WaveOut waveOut; 

        string filepath = "PDB Test Files\\1CRN(327).pdb";
        string filepath2 = "PDB Test Files\\1CRN(327).pdb";
        string filename = "1CRN(327).pdb";
        string filename2 = "1CRN(327).pdb";

        // ARCBALL VARIABLES

        static Arcball arcball = new Arcball(640.0f, 480.0f);  // ArcBall Instance
        MouseState mouse = OpenTK.Input.Mouse.GetState();      // Current Mouse Point
        float[] mousePt = new float[3];                        // Current Mouse Point
        float[] prevMousePt = new float[3];                    // Previous Mouse Point
        bool leftClick = false;                                // Clicking The Left Mouse Button?
        bool rightClick = false;                               // Clicking The Right Mouse Button?
        bool isDragging = false;                               // Dragging The Mouse?
        float mouseWheel = 0.0F;
        float prevMouseWheel = 0.0F;
        int[] viewport = new int[4];
        double[] modelviewmatrix = new double[16];
        double[] projectionmatrix = new double[16];
        Vector2 projected = new Vector2();
        Vector4 unprojected = new Vector4();
        //double projX;
        //double projY;
        //double projZ;
        //double unprojX;
        //double unprojY;
        //double unprojZ;
        static float ZOOM = -400;

        // ROTATION VARIABLES
        // Protein 1
        // Final Rotation
        float[] Rotation_p1 = arcball.mat4x4Identity();
        Matrix4 leftRot = new OpenTK.Matrix4(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1);

        // Last Rotation
        float[] LastRot_p1 = arcball.mat3x3Identity();

        // This Rotation
        float[] ThisRot_p1 = arcball.mat3x3Identity();

        // Protein 2
        // Final Rotation
        float[] Rotation_p2 = arcball.mat4x4Identity();
        Matrix4 rightRot = new Matrix4(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1);

        // Last Rotation
        float[] LastRot_p2 = arcball.mat3x3Identity();

        // This Rotation
        float[] ThisRot_p2 = arcball.mat3x3Identity();


        // TRANSLATION VARIABLES
        // Protein 1
        OpenTK.Matrix4 leftTrans = OpenTK.Matrix4.CreateTranslation(-200, 0, ZOOM);

        // Protein 2
        OpenTK.Matrix4 rightTrans = OpenTK.Matrix4.CreateTranslation(200, 0, ZOOM);

        // END OF ARCBALL VARIABLES

        [DllImport("AtomData.dll", CharSet = CharSet.Ansi)]
        private static extern bool init(string p1, string p2);

        [DllImport("AtomData.dll", CharSet=CharSet.Ansi)]
        private static extern bool loadAtom(int id, string p1);

        [DllImport("AtomData.dll")]
        private static extern int getProteinSize(int id);

        [DllImport("AtomData.dll")]
        private static extern float getAtomX(int id, int i);

        [DllImport("AtomData.dll")]
        private static extern float getAtomY(int id, int i);

        [DllImport("AtomData.dll")]
        private static extern float getAtomZ(int id, int i);

        [DllImport("AtomData.dll")]
        private static extern float getAtomR(int id, int i);

        [DllImport("AtomData.dll")]
        private static extern float getAtomG(int id, int i);

        [DllImport("AtomData.dll")]
        private static extern float getAtomB(int id, int i);

        [DllImport("AtomData.dll")]
        private static extern float getAtomRadius(int id, int i);

        [DllImport("AtomData.dll")]
        private static extern void setCollision(int id, int i);

        [DllImport("AtomData.dll")]
        private static extern void resetCollision(int id, int i);

        [DllImport("AtomData.dll")]
        private static extern bool canRender(int id, int i);

        [DllImport("AtomData.dll")]
        private static extern bool BruteForceCollision(float paramA, float paramB, float x, float y, float z, float x2, float y2, float z2);

        [DllImport("AtomData.dll")]
        private static extern float getForceVectorX();

        [DllImport("AtomData.dll")]
        private static extern float getForceVectorY();

        [DllImport("AtomData.dll")]
        private static extern float getForceVectorZ();

        [DllImport("AtomData.dll")]
        private static extern void buildRegularGrid(int id); 

        [DllImport("AtomData.dll")]
        private static extern void populateGrid(int id);

        [DllImport("AtomData.dll")]
        private static extern void buildRegularGridTwo(int id);

        [DllImport("AtomData.dll")]
        private static extern void populateGridTwo(int id);

        [DllImport("AtomData.dll")]
        private static extern void findOverlap(int t1x, int t1y, int t1z, int t2x, int t2y, int t2z);

        [DllImport("AtomData.dll")]
        private static extern void findGridCollisions(int x1, int y1, int z1, int x2, int y2, int z2);

        [DllImport("AtomData.dll")]
        private static extern bool calculateForce(float x, float y, float z, float x2, float y2, float z2);

        [DllImport("AtomData.dll")]
        private static extern int totalColliding();

        public MainWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            //Force WPF render mode to software only.
            //System.Windows.Interop.HwndSource hwndSource = PresentationSource.FromVisual(this) as System.Windows.Interop.HwndSource;
            //System.Windows.Interop.HwndTarget hwndTarget = hwndSource.CompositionTarget;
            //hwndTarget.RenderMode = System.Windows.Interop.RenderMode.SoftwareOnly;
            
            sw.Start();
            Application_Idle();

            
            // Create the GLControl.
            glc = new GLControl();

            // Assign Load and Paint events of GLControl.
            glc.Load += new EventHandler(glc_Load);
            glc.Paint += new PaintEventHandler(glc_Paint);

            host.Child = glc; // Assign the GLControl as the host control's child.
        }

        void glc_Load(object sender, EventArgs e)
        {
            //Initialise Atom DLL Data
            if (init("PDB Test Files\\1CRN(327).pdb", "PDB Test Files\\1CRN(327).pdb") == false)
            {
                //Error message box
                string messagetext = "Error loading protein file";
                string caption = "Error loading protein file";
                MessageBoxButton button = MessageBoxButton.OK;
                MessageBoxImage icon = MessageBoxImage.Error;
                MessageBoxResult messageBoxResult = System.Windows.MessageBox.Show(messagetext, caption, button, icon);
            }

            p.InitialiseLighting();
            p.makeLists(quality);

            //Antialiasing
            GL.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);
            GL.Enable(EnableCap.Blend);
            GL.Enable(EnableCap.PolygonSmooth);
            GL.Hint(HintTarget.PolygonSmoothHint, HintMode.Nicest);
            
            // Set up initial modes
            int w = glc.Width;
            int h = glc.Height;
            arcball.setBounds((float)w, (float)h);
            GL.Viewport(0, 0, w, h);
            GL.MatrixMode(MatrixMode.Projection);
            GL.LoadIdentity();
            Matrix4 perspective = Matrix4.Perspective(45.0f, (float)w / (float)h, 0.1f, 4000.0f);
            GL.MultMatrix(ref perspective);
            GL.MatrixMode(MatrixMode.Modelview);
            GL.LoadIdentity();

            Load1DataText.Text = "Protein 1 size: " + getProteinSize(1).ToString();
            Load2DataText.Text = "Protein 2 size: " + getProteinSize(2).ToString();

            ConsoleManager.Show();
            buildRegularGrid(1);
            populateGrid(1);
            buildRegularGridTwo(2);
            populateGridTwo(2);

            sineWaveProvider.SetWaveFormat(16000, 1); // 16kHz mono    
            sineWaveProvider.Frequency = 4000;
            sineWaveProvider.Amplitude = (float)(0.25);
            waveOut = new WaveOut();
            waveOut.Init(sineWaveProvider);

            loadText1.Text = filename;
            loadText2.Text = filename2;



            //Old-type sphere rendering.
            //var meshBuilder = new MeshBuilder(true, false);
            //System.Windows.Media.Media3D.Point3D point = new System.Windows.Media.Media3D.Point3D(0, 0, 0);
            //meshBuilder.AddSphere(point, 10f, 10, 10);
            //var mesh = meshBuilder.ToMesh(true);

            //var greenMaterial = MaterialHelper.CreateMaterial(Colors.Green);
            //var insideMaterial = MaterialHelper.CreateMaterial(Colors.Yellow);

            //mygeometry.Material = greenMaterial;
            //mygeometry.BackMaterial = insideMaterial;
            //mygeometry.Geometry = mesh;

            //modelGroup.Children.Add(mygeometry);



            //Set up Force Viewer Camera.
            myView.DefaultCamera = new PerspectiveCamera();
            myView.DefaultCamera.Position = new Point3D(0, 0, 40);
            myView.DefaultCamera.LookDirection = new Vector3D(0, 0, -40);
            myView.DefaultCamera.UpDirection = new Vector3D(0, 1, 0);

            //Create Force Viewer sphere.
            SphereVisual3D sphere = new SphereVisual3D();
            sphere.Radius = 2;
            sphere.Center = new Point3D(0, 0, 0);
            var brush = new SolidColorBrush();
            var color = System.Windows.Media.Color.FromArgb(255, 255, 0, 0);
            brush.Color = color;
            sphere.Fill = brush;
            modelGroup.Children.Add(sphere.Content);

            //Create Force Viewer line (pipe).
            PipeVisual3D pipe = new PipeVisual3D();
            pipe.Point1 = new Point3D(0, 0, 0);
            pipe.Point2 = new Point3D(0, 20, 0);
            pipe.Diameter = 3;
            modelGroup.Children.Add(pipe.Content);
        }


        Vector3 crossprod(float a, float b, float c, float d, float e, float f, float g, float h, float i)
        {
            Vector3 v0 = new Vector3(a, b, c);
            Vector3 v1 = new Vector3(d, e, f);
            Vector3 v2 = new Vector3(g, h, i);

            Vector3 x = new Vector3(v2[0] - v0[0], v2[1] - v0[1], v2[2] - v0[2]);
            Vector3 y = new Vector3(v1[0] - v0[0], v1[1] - v0[1], v1[2] - v0[2]);

            Vector3 normal = new Vector3(
            x[1] * y[2] - y[1] * x[2],
            x[2] * y[0] - y[2] * x[0],
            x[0] * y[1] - y[0] * x[1]);

            return normal;
        }




        void glc_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
        {
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

            GL.MatrixMode(MatrixMode.Modelview);
            GL.LoadIdentity();

            GL.PushMatrix();
            // Translate protein
            GL.MultMatrix(ref leftTrans);
            // Rotate protein
            GL.MultMatrix(ref leftRot);
            // Render protein
            renderProtein(1);
            GL.PopMatrix();

            GL.PushMatrix();
            // Translate protein
            GL.MultMatrix(ref rightTrans);
            // Rotate protein
            GL.MultMatrix(ref rightRot);
            // Render protein
            renderProtein(2);
            GL.PopMatrix();

            /*if (showforces == true)
            {
                Vector3 forcevector = new Vector3(getForceVectorX(), getForceVectorY(), getForceVectorZ()); //Combine force vector
                //Vector3 normalisedforce = Vector3.Normalize(forcevector);

                Vector3 linevectorstart = new Vector3(0, 0, 0); //Start point of force line.

                float[] myAmbientColour = new float[4] { 255, 0, 255, 1 }; //Line colour.

                GL.PushMatrix();
                GL.Material(MaterialFace.FrontAndBack, MaterialParameter.Ambient, myAmbientColour);
                GL.LineWidth(10);
                GL.Begin(PrimitiveType.Lines);

                GL.Normal3(Vector3.Cross(linevectorstart, forcevector)); //Calculate normal for line to avoid lighting issues.
                GL.Vertex3(0, 0, 0);
                GL.Vertex3(forcevector * 20);


                //Old pyramid version.
                //Vector3 normal1 = new Vector3(crossprod(0, 20, -20, 0, -20, -20, getForceVectorX(), getForceVectorY(), getForceVectorZ()));
                //GL.Normal3(normal1[0], normal1[1], normal1[2]);
                //GL.Vertex3(0f, 20f, -20f);
                //GL.Vertex3(0f, -20f, -20f);
                //GL.Vertex3(getForceVectorX(), getForceVectorY(), getForceVectorZ());

                //Vector3 normal2 = new Vector3(crossprod(0, 20, 20, 0, 20, -20, getForceVectorX(), getForceVectorY(), getForceVectorZ()));
                //GL.Normal3(normal2[0], normal2[1], normal2[2]);
                //GL.Vertex3(0f, 20f, 20f);
                //GL.Vertex3(0f, 20f, -20f);
                //GL.Vertex3(getForceVectorX(), getForceVectorY(), getForceVectorZ());

                //Vector3 normal3 = new Vector3(crossprod(0, 20, 20, 0, -20, 20, getForceVectorX(), getForceVectorY(), getForceVectorZ()));
                //GL.Normal3(normal3[0], normal3[1], normal3[2]);
                //GL.Vertex3(0f, 20f, 20f);
                //GL.Vertex3(0f, -20f, 20f);
                //GL.Vertex3(getForceVectorX(), getForceVectorY(), getForceVectorZ());

                //Vector3 normal4 = new Vector3(crossprod(0, -20, 20, 0, -20, -20, getForceVectorX(), getForceVectorY(), getForceVectorZ()));
                //GL.Normal3(normal4[0], normal4[1], normal4[2]);
                //GL.Vertex3(0f, -20f, 20f);
                //GL.Vertex3(0f, -20f, -20f);
                //GL.Vertex3(getForceVectorX(), getForceVectorY(), getForceVectorZ());

                GL.End();
                GL.PopMatrix();
            }*/

            glc.SwapBuffers();
            glc.Invalidate();
            update();
        }

        int clamp(int t)
        {
            if (t > movementSpeed)
            {
                Console.WriteLine(t + " Clamped to: " + movementSpeed);
                return (int)movementSpeed;
            }
            else if (t < -movementSpeed)
            {
                Console.WriteLine(t + " Clamped to: " + -movementSpeed);
                return (int)-movementSpeed;
            } else {
                return (int)t / 10;// / 10;
            }
        }

        void KeyPress()
        {
            int o = Positions[(int)Protein.active] * 3; //Adds an additional offset for indexing additional proteins.

            //Check if Window is active and input text boxes do not have focus.
            if (MainWinActive == true && !loadText1.IsFocused && !loadText2.IsFocused)
            {
                if (Positions[(int)Protein.active] == 0)
                {
                    if (System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.A)) Positions[(int)Protein.x + o] -= (int)movementSpeed + clamp((int)getForceVectorX());
                    if (System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.D)) Positions[(int)Protein.x + o] += (int)movementSpeed - clamp((int)getForceVectorX());
                    if (System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.S)) Positions[(int)Protein.y + o] -= (int)movementSpeed + clamp((int)getForceVectorY());
                    if (System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.W)) Positions[(int)Protein.y + o] += (int)movementSpeed - clamp((int)getForceVectorY());
                    if (System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.Q)) Positions[(int)Protein.z + o] -= (int)movementSpeed + clamp((int)getForceVectorZ());
                    if (System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.E)) Positions[(int)Protein.z + o] += (int)movementSpeed - clamp((int)getForceVectorZ());
                }
                else
                {
                    if (System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.A)) Positions[(int)Protein.x + o] -= (int)movementSpeed - clamp((int)getForceVectorX());
                    if (System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.D)) Positions[(int)Protein.x + o] += (int)movementSpeed + clamp((int)getForceVectorX());
                    if (System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.S)) Positions[(int)Protein.y + o] -= (int)movementSpeed - clamp((int)getForceVectorY());
                    if (System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.W)) Positions[(int)Protein.y + o] += (int)movementSpeed + clamp((int)getForceVectorY());
                    if (System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.Q)) Positions[(int)Protein.z + o] -= (int)movementSpeed - clamp((int)getForceVectorZ());
                    if (System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.E)) Positions[(int)Protein.z + o] += (int)movementSpeed + clamp((int)getForceVectorZ());
                }
            }
        }

        void update()
        {
            current = OpenTK.Input.Mouse.GetState();

            if (hearforces == true)
            {
                waveOut.Play();
            }
            else waveOut.Stop();


            //Check if Windows is active.
            if (MainWinActive == true)
            {
                //Console.Out.WriteLine(getForceVectorX() + " " + getForceVectorY() + " " + getForceVectorZ());

                mouse = OpenTK.Input.Mouse.GetState();
                prevMouseWheel = mouseWheel;
                mouseWheel = mouse.WheelPrecise;
                prevMousePt = mousePt;
                mousePt[0] = mouse.X;
                mousePt[1] = mouse.Y;
                mousePt[2] = 0.0F;

                if (mouse.IsButtonDown(OpenTK.Input.MouseButton.Left))
                {
                    leftClick = true;
                }
                else
                {
                    leftClick = false;
                    isDragging = false;
                }

                if (mouse.IsButtonDown(OpenTK.Input.MouseButton.Right))
                {
                    rightClick = true;
                }
                else
                {
                    rightClick = false;
                    isDragging = false;
                }

                if (leftClick)
                {
                    //rotateProtein();
                    bool closer = closerToP1();
                    if (closer)
                        Console.Out.WriteLine("Closer to P1");
                    else
                        Console.Out.WriteLine("Closer to P2");
                }

                if (rightClick)
                    translateProtein();

                if (mouseWheel < prevMouseWheel)
                {
                    ZOOM += 100;
                }
                else if (mouseWheel > prevMouseWheel)
                {
                    ZOOM -= 100;
                }

                //Check if focus is on OpenTK (graphics area is being clicked).
                if (glc.Focused)
                {
                   

                    //Set focus away if no longer clicked.
                    if (!current.IsButtonDown(OpenTK.Input.MouseButton.Left))
                        GridMain.Focus();
                }
            }

            previous = current;

            glc.VSync = vsync; //Set vertical refresh sync.

            if (qualitychange == true) //If quality has been changed, remake lists.
            {
                p.makeLists(quality);
                qualitychange = false;
            }


            KeyPress();


           


            //findGridCollisions(0,0,0,0,0,0);
            //findGridCollisions(Positions[(int)Protein.x], Positions[(int)Protein.y], Positions[(int)Protein.z], Positions[(int)Protein.x2], Positions[(int)Protein.y2], Positions[(int)Protein.z2]);
            //calculateForce(Positions[(int)Protein.x], Positions[(int)Protein.y], Positions[(int)Protein.z], Positions[(int)Protein.x2], Positions[(int)Protein.y2], Positions[(int)Protein.z2]);
            //findOverlap((int)Positions[(int)Protein.x], (int)Positions[(int)Protein.y], (int)Positions[(int)Protein.z], (int)Positions[(int)Protein.x2], (int)Positions[(int)Protein.y2], (int)Positions[(int)Protein.z2]);
            //testFunction() + " " + totalColliding();
            //Collisions();
            //BruteForceCollision(1.0f, 1.0f, Positions[(int)Protein.x], Positions[(int)Protein.y], Positions[(int)Protein.z], Positions[(int)Protein.x2], Positions[(int)Protein.y2], Positions[(int)Protein.z2]);
            //Console.WriteLine("F: " + getForceVectorX() + "," + getForceVectorY() + "," + getForceVectorZ());
            //Console.WriteLine("Total Colliding: " + totalColliding());


            //LinesVisual3D lv3d = new LinesVisual3D();
            //Point3D p1 = new Point3D(0, 0, 0);
            //lv3d.Points.Add(p1);
            ////Point3D p2 = new Point3D(0, 40, 0);
            //Point3D p2 = new Point3D((double)getForceVectorX(), (double)getForceVectorY(), (double)getForceVectorZ());
            //lv3d.Points.Add(p2);
            //lv3d.Thickness = 20;
            //modelGroup.Children.Add(lv3d.Content);

            /*
            //Remove pipe from modelGroup (Force Viewer models).
            modelGroup.Children.RemoveAt(1);
            //Create new Pipe depending on current force vector.
            PipeVisual3D pipe = new PipeVisual3D();
            pipe.Point1 = new Point3D(0, 0, 0);
            pipe.Point2 = new Point3D((double)getForceVectorX(), (double)getForceVectorY(), (double)getForceVectorZ());
            pipe.Diameter = 0.5;
            modelGroup.Children.Add(pipe.Content);

            //Send modelGroup to ModelViewer3D for Force Viewer.
            MV3D.Content = modelGroup;
            */

            Application_Idle();
        }

        void renderProtein(int id)
        {

            int o = (3 * (id - 1)); //Adds an additional offset for indexing additional proteins.
            int max = getProteinSize(id);
                for (int i = 0; i < max; i++)
                {
                    if (canRender(id, i))
                    {
                        GL.PushMatrix();
                        GL.Enable(EnableCap.Normalize);
                        //GL.Translate(Positions[(int)Protein.x + o], Positions[(int)Protein.y + o], Positions[(int)Protein.z + o]); //Molecule translation
                        p.renderSphere(getAtomX(id, i), getAtomY(id, i), getAtomZ(id, i), getAtomR(id, i), getAtomG(id, i), getAtomB(id, i), getAtomRadius(id, i)); //Draw Atom
                        GL.Disable(EnableCap.Normalize);
                        GL.PopMatrix();
                        //resetCollision(id, i);
                    }
                }
        }

        void rotateProtein()
        {
            if (closerToP1())
            {
                //Rotate protein 1
                if (!isDragging)
                {
                    isDragging = true;
                    LastRot_p1 = ThisRot_p1;
                    arcball.click(mousePt);
                }
                else
                {
                    float[] ThisQuat = new float[4];
                    ThisQuat = arcball.drag(mousePt);
                    ThisRot_p1 = arcball.mat3x3FromQuaternion(ThisQuat);
                    ThisRot_p1 = arcball.mat3x3Multiply(ThisRot_p1, LastRot_p1);
                    Rotation_p1 = arcball.mat4x4SetRotationFromMat3x3(Rotation_p1, ThisRot_p1);
                    convertRotationMatrix();
                }
            }
            else
            {
                //Rotate protein 2
                if (!isDragging)
                {
                    isDragging = true;
                    LastRot_p2 = ThisRot_p2;
                    arcball.click(mousePt);
                }
                else
                {
                    float[] ThisQuat = new float[4];
                    ThisQuat = arcball.drag(mousePt);
                    ThisRot_p2 = arcball.mat3x3FromQuaternion(ThisQuat);
                    ThisRot_p2 = arcball.mat3x3Multiply(ThisRot_p2, LastRot_p2);
                    Rotation_p2 = arcball.mat4x4SetRotationFromMat3x3(Rotation_p2, ThisRot_p2);
                    convertRotationMatrix();
                }
            }
        }

        void translateProtein()
        {
            if (closerToP1())
            {
                //Translate protein 1
            }
            else
            {
                //Translate protein 2
            }
        }

        bool closerToP1()
        {
            Vector3 extractedTrans = leftTrans.ExtractTranslation();
            projected = project(extractedTrans);
            double p1_x = projected.X;
            double p1_y = projected.Y;
            extractedTrans = rightTrans.ExtractTranslation();
            projected = project(extractedTrans);
            double p2_x = projected.X;
            double p2_y = projected.Y;
            float l1 = (float)((mousePt[0] - p1_x) * (mousePt[0] - p1_x) + (mousePt[1] - p1_y) * (mousePt[2] - p1_y));
            float l2 = (float)((mousePt[0] - p2_x) * (mousePt[0] - p2_x) + (mousePt[1] - p2_y) * (mousePt[2] - p2_y));

            //cout << "Mouse x: " << mouse_x << ", y: " << mouse_y << ".    l1: " << l1 << ", l2 " << l2 << endl;
            //cout << "Mouse: " << mouse_x << ", " << mouse_y << ".  P1: " << p1_x << ", " << p1_y << ".  P2: " << p2_x << ", " << p2_y<< endl;

            if (l1 < l2)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        void convertRotationMatrix()
        {
            leftRot.Row0.X = Rotation_p1[0];
            leftRot.Row0.Y = Rotation_p1[1];
            leftRot.Row0.Z = Rotation_p1[2];
            leftRot.Row0.W = Rotation_p1[3];

            leftRot.Row1.X = Rotation_p1[4];
            leftRot.Row1.Y = Rotation_p1[5];
            leftRot.Row1.Z = Rotation_p1[6];
            leftRot.Row1.W = Rotation_p1[7];

            leftRot.Row2.X = Rotation_p1[8];
            leftRot.Row2.Y = Rotation_p1[9];
            leftRot.Row2.Z = Rotation_p1[10];
            leftRot.Row2.W = Rotation_p1[11];

            leftRot.Row3.X = Rotation_p1[12];
            leftRot.Row3.Y = Rotation_p1[13];
            leftRot.Row3.Z = Rotation_p1[14];
            leftRot.Row3.W = Rotation_p1[15];

            rightRot.Row0.X = Rotation_p2[0];
            rightRot.Row0.Y = Rotation_p2[1];
            rightRot.Row0.Z = Rotation_p2[2];
            rightRot.Row0.W = Rotation_p2[3];

            rightRot.Row1.X = Rotation_p2[4];
            rightRot.Row1.Y = Rotation_p2[5];
            rightRot.Row1.Z = Rotation_p2[6];
            rightRot.Row1.W = Rotation_p2[7];

            rightRot.Row2.X = Rotation_p2[8];
            rightRot.Row2.Y = Rotation_p2[9];
            rightRot.Row2.Z = Rotation_p2[10];
            rightRot.Row2.W = Rotation_p2[11];

            rightRot.Row3.X = Rotation_p2[12];
            rightRot.Row3.Y = Rotation_p2[13];
            rightRot.Row3.Z = Rotation_p2[14];
            rightRot.Row3.W = Rotation_p2[15];
        }

        Vector2 project(Vector3 point)
        {
            //Get modelview and projection matrices
            Matrix4 modelview = new Matrix4();
            GL.GetFloat(GetPName.ModelviewMatrix, out modelview);
            Matrix4 projection = new Matrix4();
            GL.GetFloat(GetPName.ProjectionMatrix, out projection);

            Matrix4 viewportProjection = projection * modelview;
            //transform world to clipping coordinates
            //point3D = Matrix4.viewportProjection.multiply(point3D);
            Vector3 newVec = new Vector3();
            newVec.X = viewportProjection.M11 * point.X + viewportProjection.M12 * point.Y + viewportProjection.M13 * point.Z;
            newVec.Y = viewportProjection.M21 * point.X + viewportProjection.M22 * point.Y + viewportProjection.M23 * point.Z;
            newVec.Z = viewportProjection.M31 * point.X + viewportProjection.M32 * point.Y + viewportProjection.M33 * point.Z;
            int screenX = (int)Math.Round(((newVec.X + 1) / 2.0) * glc.Width); 
            int screenY = (int)Math.Round(((1 - newVec.Y) / 2.0) * glc.Height);
            return new Vector2(screenX, screenY);
        }

        Vector4 unProject(Vector2 point)
        {
            //Get modelview and projection matrices
            Matrix4 modelview = new Matrix4();
            GL.GetFloat(GetPName.ModelviewMatrix, out modelview);
            Matrix4 projection = new Matrix4();
            GL.GetFloat(GetPName.ProjectionMatrix, out projection);

            float worldX = (float)(2.0 * point.X / glc.Width - 1);
            float worldY = (float)(-2.0 * point.Y / glc.Height + 1);
            Matrix4 inverseProjection = Matrix4.Invert(projection * modelview);
            Vector4 worldPoint = new Vector4(worldX, worldY, 0, 0);
            return Vector4.Transform(worldPoint, inverseProjection);
        }

        void MainWinActivated(object sender, EventArgs e)
        {
            MainWinActive = true;
        }

        void MainWinDeactivated(object sender, EventArgs e)
        {
            MainWinActive = false;
        }


        private void LoadButton1_Click(object sender, RoutedEventArgs e)
        {
            // Configure open file dialog box
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
            dlg.FileName = ""; // Default file name
            filename = dlg.SafeFileName;//FileName;
            dlg.DefaultExt = ".pdb"; // Default file extension
            dlg.Filter = "Program Debug Database (.pdb)|*.pdb"; // Filter files by extension 

            // Show open file dialog box
            Nullable<bool> result = dlg.ShowDialog();

            // Process open file dialog box results 
            if (result == true)
            {
                // Open document 
                filepath = dlg.FileName;
                filename = dlg.SafeFileName;
                loadAtom(1, filepath);
                buildRegularGrid(1);
                populateGrid(1);
            }
            else TextBoxDataError.Text = "Load Unsuccessful!";

            Load1DataText.Text = "Protein 1 size: " + getProteinSize(1).ToString();
        }

        private void LoadButton2_Click(object sender, RoutedEventArgs e)
        {
            // Configure open file dialog box
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
            dlg.FileName = ""; // Default file name
            filename = dlg.SafeFileName;//FileName;
            dlg.DefaultExt = ".pdb"; // Default file extension
            dlg.Filter = "Program Debug Database (.pdb)|*.pdb"; // Filter files by extension 

            // Show open file dialog box
            Nullable<bool> result = dlg.ShowDialog();

            // Process open file dialog box results 
            if (result == true)
            {
                // Open document 
                filepath2 = dlg.FileName;
                filename2 = dlg.SafeFileName;
                loadAtom(2, filepath2);
                buildRegularGridTwo(2);
                populateGridTwo(2);
            }
            else TextBoxDataError.Text = "Load Unsuccessful!";

            Load2DataText.Text = "Protein 2 size: " + getProteinSize(2).ToString();
        }


        //FPS stuff.
        void Application_Idle()
        {
            double milliseconds = ComputeTimeSlice();
            Accumulate(milliseconds);
        }

        private double ComputeTimeSlice()
        {
            sw.Stop();
            double timeslice = sw.Elapsed.TotalMilliseconds;
            sw.Reset();
            sw.Start();
            return timeslice;
        }

        private void Accumulate(double milliseconds)
        {
            idleCounter++;
            accumulator += milliseconds;
            if (accumulator > 1000)
            {
                FPSTextBlock.Text = idleCounter.ToString();
                accumulator -= 1000;
                idleCounter = 0; // Reset the counter.
            }
        }





        private void FFcheckBox_Checked(object sender, RoutedEventArgs e)
        {
            //System.Threading.Thread.Sleep(16);
        }

        private void FFcheckBox_UnChecked(object sender, RoutedEventArgs e)
        {
            //System.Threading.Thread.Sleep(16.6);
        }

        private void VSyncCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            //System.Threading.Thread.Sleep(16.6);
            vsync = true;
        }

        private void VSyncCheckBox_UnChecked(object sender, RoutedEventArgs e)
        {
            //System.Threading.Thread.Sleep(16.6);
            vsync = false;
        }

        private void qualitySliderChange(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            quality = (int)qualitySlider.Value;
            qualitychange = true;
        }

        private void P1radioButton_Checked(object sender, RoutedEventArgs e)
        {
            //System.Threading.Thread.Sleep(60);
            Positions[(int)Protein.active] = 0;
        }

        private void P2radioButton_Checked(object sender, RoutedEventArgs e)
        {
            //System.Threading.Thread.Sleep(60);
            Positions[(int)Protein.active] = 1;
        }

        private void HradioButton_Checked(object sender, RoutedEventArgs e)
        {

        }

        private void AradioButton_Checked(object sender, RoutedEventArgs e)
        {

        }

        //Menu Items.
        //File menu.
        //'Exit' menu item logic.
        private void ExitClick(object sender, EventArgs e)
        {
            this.Close(); //Close window.
        }



        //View Menu
        //Increase Zoom.
        private void IncreaseZoom(object sender, RoutedEventArgs e)
        {
            ZOOM -= 200;
        }

        //Decrease Zoom.
        private void DecreaseZoom(object sender, RoutedEventArgs e)
        {
            ZOOM += 200;
        }

        private void SFcheckBox_Checked(object sender, RoutedEventArgs e)
        {
            showforces = true;
        }

        private void SFcheckBox_UnChecked(object sender, RoutedEventArgs e)
        {
            showforces = false;
        }

        private void HFcheckBox_Checked(object sender, RoutedEventArgs e)
        {
            hearforces = true;
        }

        private void HFcheckBox_UnChecked(object sender, RoutedEventArgs e)
        {
            hearforces = false;
        }







        //Can be used to determine if any key is pressed.
        bool IsAnyKeyPressed()
        {
            var allPossibleKeys = Enum.GetValues(typeof(System.Windows.Input.Key));
            bool results = false;
            foreach (var currentKey in allPossibleKeys)
            {
                System.Windows.Input.Key key = (System.Windows.Input.Key)currentKey;
                if (key != System.Windows.Input.Key.None)
                    if (System.Windows.Input.Keyboard.IsKeyDown((System.Windows.Input.Key)currentKey)) { results = true; break; }
            }
            return results;
        }
    }
}

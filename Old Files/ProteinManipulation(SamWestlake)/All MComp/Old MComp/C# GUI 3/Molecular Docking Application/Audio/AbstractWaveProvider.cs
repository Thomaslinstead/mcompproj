﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NAudio;
using NAudio.Wave;

namespace Molecular_Docking_Application
{
    public abstract class AbstractWaveProvider : NAudio.Wave.IWaveProvider
    {
        private WaveFormat waveFormat;

        public WaveFormat WaveFormat { 
            get { return waveFormat; } 
        } 

        public AbstractWaveProvider() 
            : this(44000, 1)
        {     
        }

        public AbstractWaveProvider(int sampleRate, int channels) { 
            SetWaveFormat(sampleRate, channels); 
        }

        public void SetWaveFormat(int sampleRate, int channels) { 
            this.waveFormat = WaveFormat.CreateIeeeFloatWaveFormat(sampleRate, channels); 
        }

        public int Read(byte[] buffer, int offset, int count) { 
            WaveBuffer waveBuffer = new WaveBuffer(buffer); 
            int samplesRequired = count / 4; 
            int samplesRead = Read(waveBuffer.FloatBuffer, offset / 4, samplesRequired);
            return samplesRead * 4; 
        }

        public abstract int Read(float[] buffer, int offset, int sampleCount);   
    }
}

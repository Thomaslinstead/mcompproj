﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;

namespace Molecular_Docking_Application
{
    class SineWave : AbstractWaveProvider
    {
        [DllImport("AtomData.dll")]
        private static extern float getForceVectorX();

        [DllImport("AtomData.dll")]
        private static extern float getForceVectorY();

        [DllImport("AtomData.dll")]
        private static extern float getForceVectorZ();

        int sample; 
  
        public SineWave() 
        { 
            Frequency = 1000;
            double m = Math.Sqrt((getForceVectorX() * getForceVectorX()) + (getForceVectorY() * getForceVectorY()) + (getForceVectorZ() * getForceVectorZ()));
            Amplitude = (float)m/100f; // (Not too loud!)           
        } 
  
        public float Frequency { get; set; } 
        public float Amplitude { get; set; } 
  
        //Overloaded Read Function
        public override int Read(float[] buffer, int offset, int sampleCount) 
        { 
            int sampleRate = WaveFormat.SampleRate;
            Amplitude = (float)Math.Sqrt((getForceVectorX() * getForceVectorX()) + (getForceVectorY() * getForceVectorY()) + (getForceVectorZ() * getForceVectorZ())) / 100f;
            for (int n = 0; n < sampleCount; n++) 
            { 
                buffer[n+offset] = (float)(Amplitude * Math.Sin((2 * Math.PI * sample * Frequency) / sampleRate)); 
                sample++; 
                if (sample >= sampleRate) sample = 0; 
            } 
            return sampleCount; 
        } 
    }
}

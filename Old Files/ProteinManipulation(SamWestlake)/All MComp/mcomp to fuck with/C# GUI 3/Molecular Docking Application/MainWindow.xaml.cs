﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Runtime.InteropServices;
using System.Drawing;
using System.Drawing.Imaging;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using OpenTK.Input;
using System.Windows.Forms;
using System.Diagnostics;
using System.Threading.Tasks;

namespace Molecular_Docking_Application
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        //Mouse states.
        MouseState current = OpenTK.Input.Mouse.GetState();
        MouseState previous = OpenTK.Input.Mouse.GetState();

        //GUI Variables
        static float ZOOM = -200;
        bool vsync = true;
        int quality = 32;
        bool colliding = false;
        bool qualitychange = false;
        enum Protein { x, y, z, x2, y2, z2, active };        // Enum type for indexing references into array
        RenderSphere p = new RenderSphere();

        double accumulator = 0;
        int idleCounter = 0;
        double movementSpeed = 5;
        float rotationX = 1.57079633f, rotationY = 0.0f;

        string filepath;
        string filepath2;
        string filename;
        string filename2;

        // ARCBALL VARIABLES

        static Arcball arcball = new Arcball(640.0f, 480.0f);  // ArcBall Instance
        tuple2 MousePt;                                  // Current Mouse Point
        bool isClicked = false;                         // Clicking The Mouse?
        bool isRClicked = false;                         // Clicking The Right Mouse Button?
        bool isDragging = false;                         // Dragging The Mouse?

        // ROTATION VARIABLES

        // Protein 1
        // Final Rotation
        mat4x4 Rotation_p1 = arcball.mat4x4Identity();
 
        // Last Rotation
        mat3x3 LastRot_p1 = arcball.mat3x3Identity();
 
        // This Rotation
        mat3x3 ThisRot_p1 = arcball.mat3x3Identity();

        // Protein 2
        // Final Rotation
        mat4x4 Rotation_p2 = arcball.mat4x4Identity();
 
        // Last Rotation
        mat3x3 LastRot_p2 = arcball.mat3x3Identity();
 
        // This Rotation
        mat3x3 ThisRot_p2 = arcball.mat3x3Identity();

        // TRANSLATION VARIABLES

        // Protein 1
        // Final Translation
        mat4x4 Translation_p1 = new mat4x4() {x1y1=1.0f, x2y1=0.0f, x3y1=0.0f, x4y1=0.0f,
                                              x1y2=0.0f, x2y2=1.0f, x3y2=0.0f, x4y2=0.0f,
	                                          x1y3=0.0f, x2y3=0.0f, x3y3=1.0f, x4y3=0.0f,
	                                          x1y4=-400.0f, x2y4=0.0f, x3y4=ZOOM, x4y4=1.0f };

        // Last Translation
        mat3x3 LastTrans_p1 = arcball.mat3x3Identity();
 
        // This Translation
        mat3x3 ThisTrans_p1 = arcball.mat3x3Identity();

        //Protein 2
        // Final Translation
        mat4x4 Translation_p2 = arcball.mat4x4Identity();
        Translation_p2.x1y4 = 400.0f;
        mat4x4 Translation_p2 = {1.0f, 0.0f, 0.0f, 0.0f,
							        0.0f, 1.0f, 0.0f, 0.0f,
							        0.0f, 0.0f, 1.0f, 0.0f,
							        400.0f, 0.0f, ZOOM, 1.0f };

        // Last Translation
        mat3x3 LastTrans_p2 = arcball.mat3x3Identity();
 
        // This Translation
        mat3x3 ThisTrans_p2 = arcball.mat3x3Identity();

        // END OF ARCBALL VARIABLES

        [DllImport("AtomData.dll", CharSet = CharSet.Ansi)]
        private static extern bool init(string p1, string p2);

        [DllImport("AtomData.dll", CharSet=CharSet.Ansi)]
        private static extern bool loadAtom(int id, string p1);

        [DllImport("AtomData.dll")]
        private static extern int getProteinSize(int id);

        [DllImport("AtomData.dll")]
        private static extern float getAtomX(int id, int i);

        [DllImport("AtomData.dll")]
        private static extern float getAtomY(int id, int i);

        [DllImport("AtomData.dll")]
        private static extern float getAtomZ(int id, int i);

        [DllImport("AtomData.dll")]
        private static extern float getAtomR(int id, int i);

        [DllImport("AtomData.dll")]
        private static extern float getAtomG(int id, int i);

        [DllImport("AtomData.dll")]
        private static extern float getAtomB(int id, int i);

        [DllImport("AtomData.dll")]
        private static extern float getAtomRadius(int id, int i);

        [DllImport("AtomData.dll")]
        private static extern void setCollision(int id, int i);

        [DllImport("AtomData.dll")]
        private static extern void resetCollision(int id, int i);

        [DllImport("AtomData.dll")]
        private static extern bool canRender(int id, int i);

        [DllImport("AtomData.dll")]
        private static extern bool BruteForceCollision(float paramA, float paramB, float x, float y, float z, float x2, float y2, float z2);

        [DllImport("AtomData.dll")]
        private static extern float getForceVectorX();

        [DllImport("AtomData.dll")]
        private static extern float getForceVectorY();

        [DllImport("AtomData.dll")]
        private static extern float getForceVectorZ();

        [DllImport("AtomData.dll")]
        private static extern void buildRegularGrid(int id); 

        [DllImport("AtomData.dll")]
        private static extern void populateGrid(int id);

        [DllImport("AtomData.dll")]
        private static extern void buildRegularGridTwo(int id);

        [DllImport("AtomData.dll")]
        private static extern void populateGridTwo(int id);

        [DllImport("AtomData.dll")]
        private static extern void findOverlap(int t1x, int t1y, int t1z, int t2x, int t2y, int t2z);

        [DllImport("AtomData.dll")]
        private static extern void findGridCollisions(int x1, int y1, int z1, int x2, int y2, int z2);

        [DllImport("AtomData.dll")]
        private static extern bool calculateForce(float x, float y, float z, float x2, float y2, float z2);

        [DllImport("AtomData.dll")]
        private static extern int totalColliding();

        /*[DllImport("AtomData.dll")]
        private static extern int getColliding();

        [DllImport("AtomData.dll")]
        private static extern int numberInGridCell(int x, int y, int z);

        [DllImport("AtomData.dll")]
        private static extern int getAtomIDfromCell(int x, int y, int z, int i);*/

        private GLControl glc;

        Stopwatch sw = new Stopwatch();
        int t = 0;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            //Force WPF render mode to software only.
            //System.Windows.Interop.HwndSource hwndSource = PresentationSource.FromVisual(this) as System.Windows.Interop.HwndSource;
            //System.Windows.Interop.HwndTarget hwndTarget = hwndSource.CompositionTarget;
            //hwndTarget.RenderMode = System.Windows.Interop.RenderMode.SoftwareOnly;
            

            sw.Start();
            Application_Idle();

            
            // Create the GLControl.
            glc = new GLControl();

            // Assign Load and Paint events of GLControl.
            glc.Load += new EventHandler(glc_Load);
            glc.Paint += new PaintEventHandler(glc_Paint);

            host.Child = glc; // Assign the GLControl as the host control's child.
        }

        void glc_Load(object sender, EventArgs e)
        {
            int w = glc.Width;
            int h = glc.Height;

            //Initialise Atom DLL Data
            if (init("PDB Test Files\\1CRN(327).pdb", "PDB Test Files\\1CRN(327).pdb") == false)
            {
                //Error message box
                string messagetext = "Error loading protein file";
                string caption = "Error loading protein file";
                MessageBoxButton button = MessageBoxButton.OK;
                MessageBoxImage icon = MessageBoxImage.Error;
                MessageBoxResult messageBoxResult = System.Windows.MessageBox.Show(messagetext, caption, button, icon);
            }

            p.InitialiseLighting();
            p.makeLists(quality);

            //Antialiasing
            GL.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);
            GL.Enable(EnableCap.Blend);
            GL.Enable(EnableCap.PolygonSmooth);
            GL.Hint(HintTarget.PolygonSmoothHint, HintMode.Nicest);
            
            // Set up initial modes
            GL.MatrixMode(MatrixMode.Projection);
            GL.LoadIdentity();
            GL.Ortho(0, w, 0, h, -1, 1);
            GL.Viewport(0, 0, w, h);

            Load1DataText.Text = "Protein 1 size: " + getProteinSize(1).ToString();
            Load2DataText.Text = "Protein 2 size: " + getProteinSize(2).ToString();

            ConsoleManager.Show();
            buildRegularGrid(1);
            populateGrid(1);
            buildRegularGridTwo(2);
            populateGridTwo(2);
        }

        void glc_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
        {
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

            GL.MatrixMode(MatrixMode.Projection);
            GL.LoadIdentity();
            int w = glc.Width;
            int h = glc.Height;
            GL.Viewport(0, 0, w, h);

            //Change View Mode
            Matrix4 mat = Matrix4.Perspective(60.0f*(float)(Math.PI/180), (float)Width / (float)Height, 0.1f, 4000.0f);
            GL.MultMatrix(ref mat);
            Matrix4 lookat = Matrix4.LookAt((float)((zoom) * Math.Cos(rotationX)), rotationY, (float)((zoom) * Math.Sin(rotationX)), 0, 0, 0, 0, 1, 0); //Use the 'look at' helper function to position and aim the camera.
            GL.MatrixMode(MatrixMode.Modelview);
            GL.LoadIdentity();
            GL.MultMatrix(ref lookat);

            renderProtein(1);
            renderProtein(2);

            glc.SwapBuffers();
            glc.Invalidate();
            update();
        }

        int clamp(int t)
        {
            if (t > movementSpeed)
            {
                Console.WriteLine(t + " Clamped to: " + movementSpeed);
                return (int)movementSpeed;
            }
            else if (t < -movementSpeed)
            {
                Console.WriteLine(t + " Clamped to: " + -movementSpeed);
                return (int)-movementSpeed;
            } else {
                return (int)t / 10;// / 10;
            }
        }

        void KeyPress()
        {
                int o = Positions[(int)Protein.active] * 3; //Adds an additional offset for indexing additional proteins.
                if (System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.A)) Positions[(int)Protein.x + o] -= (int)movementSpeed + clamp((int)getForceVectorX());
                if (System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.D)) Positions[(int)Protein.x + o] += (int)movementSpeed - clamp((int)getForceVectorX());
                if (System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.S)) Positions[(int)Protein.y + o] -= (int)movementSpeed + clamp((int)getForceVectorY());
                if (System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.W)) Positions[(int)Protein.y + o] += (int)movementSpeed - clamp((int)getForceVectorY());
                if (System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.Q)) Positions[(int)Protein.z + o] -= (int)movementSpeed + clamp((int)getForceVectorZ());
                if (System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.E)) Positions[(int)Protein.z + o] += (int)movementSpeed - clamp((int)getForceVectorZ());
        }

        void update()
        {
            //Zoom functionality with mouse wheel.
            current = OpenTK.Input.Mouse.GetState();
            if (current.Wheel < previous.Wheel)
            {
                zoom += 100;
            }
            else if (current.Wheel > previous.Wheel)
            {
                zoom -= 100;
            }

            if (current.X < previous.X && current.IsButtonDown(OpenTK.Input.MouseButton.Left))
            {
                rotationX = rotationX - 0.01f;
            }

            if (current.X > previous.X && current.IsButtonDown(OpenTK.Input.MouseButton.Left))
            {
                rotationX = rotationX + 0.01f;
            }

            if (current.Y < previous.Y && current.IsButtonDown(OpenTK.Input.MouseButton.Left))
            {
                rotationY = rotationY - 0.1f;
            }

            if (current.Y > previous.Y && current.IsButtonDown(OpenTK.Input.MouseButton.Left))
            {
                rotationY = rotationY + 0.1f;
            }

            previous = current;

            glc.VSync = vsync; //Set vertical refresh sync.

            if (qualitychange == true) //If quality has been changed, remake lists.
            {
                p.makeLists(quality);
                qualitychange = false;
            }

            KeyPress();
            rotation += 3.0f; //Nudge the rotation.

            //findGridCollisions(0,0,0,0,0,0);
            findGridCollisions(Positions[(int)Protein.x], Positions[(int)Protein.y], Positions[(int)Protein.z], Positions[(int)Protein.x2], Positions[(int)Protein.y2], Positions[(int)Protein.z2]);
            calculateForce(Positions[(int)Protein.x], Positions[(int)Protein.y], Positions[(int)Protein.z], Positions[(int)Protein.x2], Positions[(int)Protein.y2], Positions[(int)Protein.z2]);
            //findOverlap((int)Positions[(int)Protein.x], (int)Positions[(int)Protein.y], (int)Positions[(int)Protein.z], (int)Positions[(int)Protein.x2], (int)Positions[(int)Protein.y2], (int)Positions[(int)Protein.z2]);
            //testFunction() + " " + totalColliding();
            //Collisions();
            //colliding = BruteForceCollision(1.0f, 1.0f, Positions[(int)Protein.x], Positions[(int)Protein.y], Positions[(int)Protein.z], Positions[(int)Protein.x2], Positions[(int)Protein.y2], Positions[(int)Protein.z2]);
            //Console.WriteLine("F: " + getForceVectorX() + "," + getForceVectorY() + "," + getForceVectorZ());
            //Console.WriteLine("Total Colliding: " + totalColliding());
            Application_Idle();
        }

        void renderProtein(int id)
        {

            int o = (3 * (id - 1)); //Adds an additional offset for indexing additional proteins.
            int max = getProteinSize(id);
                for (int i = 0; i < max; i++)
                {
                    if (canRender(id, i))
                    {
                        GL.PushMatrix();
                        GL.Enable(EnableCap.Normalize);
                        GL.Translate(Positions[(int)Protein.x + o], Positions[(int)Protein.y + o], Positions[(int)Protein.z + o]); //Molecule translation
                        p.renderSphere(getAtomX(id, i), getAtomY(id, i), getAtomZ(id, i), getAtomR(id, i), getAtomG(id, i), getAtomB(id, i), getAtomRadius(id, i)); //Draw Atom
                        GL.Disable(EnableCap.Normalize);
                        GL.PopMatrix();
                        resetCollision(id, i);
                    }
                }
        }

        //Possible future improvement for collision/force calculation
        void Collisions()
        {
            //t++;
            //for (int i = 0; i < totalColliding(); i++)
            //{
            //    setCollision(1, getColliding());
            //}
            //for (int x = 0; x < 3; x++)
            //{
            //    for (int y = 0; y < 3; y++)
            //    {
            //        for (int z = 0; z < 3; z++)
            //        {
            //            for (int j = 0; j < numberInGridCell(x, y, z); j++)
            //            {
            //                setCollision(1, getAtomIDfromCell(x, y, z, j));
            //            }
            //        }
            //   }
            //}

            // Parallelize the outer loop to partition the source array by rows.
            // Parallel.For(0, max, i =>
            //{
            // });
        }

        private void LoadButton1_Click(object sender, RoutedEventArgs e)
        {
            // Configure open file dialog box
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
            dlg.FileName = "Document"; // Default file name
            filename = dlg.SafeFileName;//FileName;
            dlg.DefaultExt = ".pdb"; // Default file extension
            dlg.Filter = "Program Debug Database (.pdb)|*.pdb"; // Filter files by extension 

            // Show open file dialog box
            Nullable<bool> result = dlg.ShowDialog();

            // Process open file dialog box results 
            if (result == true)
            {
                // Open document 
                filepath = dlg.FileName;
                filename = dlg.SafeFileName;
            }

            loadAtom(1, filepath);

            loadText1.Text = filename;
            Load1Menu.Header = filename;

            TextBoxDataError.Text = "Load Successful!";
            Load1DataText.Text = "Protein 1 size: " + getProteinSize(1).ToString();
        }

        private void LoadButton2_Click(object sender, RoutedEventArgs e)
        {
            // Configure open file dialog box
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
            dlg.FileName = "Document"; // Default file name
            filename = dlg.SafeFileName;//FileName;
            dlg.DefaultExt = ".pdb"; // Default file extension
            dlg.Filter = "Program Debug Database (.pdb)|*.pdb"; // Filter files by extension 

            // Show open file dialog box
            Nullable<bool> result = dlg.ShowDialog();

            // Process open file dialog box results 
            if (result == true)
            {
                // Open document 
                filepath2 = dlg.FileName;
                filename2 = dlg.SafeFileName;
            }

            loadAtom(2, filepath2);

            loadText2.Text = filename2;

            TextBoxDataError.Text = "Load Successful!";
            Load2DataText.Text = "Protein 2 size: " + getProteinSize(2).ToString();
        }


        //FPS stuff.
        void Application_Idle()
        {
            double milliseconds = ComputeTimeSlice();
            Accumulate(milliseconds);
        }

        private double ComputeTimeSlice()
        {
            sw.Stop();
            double timeslice = sw.Elapsed.TotalMilliseconds;
            sw.Reset();
            sw.Start();
            return timeslice;
        }

        private void Accumulate(double milliseconds)
        {
            idleCounter++;
            accumulator += milliseconds;
            if (accumulator > 1000)
            {
                //movementSpeed = 1 + 300* (1 / (idleCounter + 0.001));
                FPSTextBlock.Text = idleCounter.ToString();
                accumulator -= 1000;
                idleCounter = 0; // Reset the counter.
            }
        }





        private void FFcheckBox_Checked(object sender, RoutedEventArgs e)
        {
            //System.Threading.Thread.Sleep(16);
        }

        private void FFcheckBox_UnChecked(object sender, RoutedEventArgs e)
        {
            //System.Threading.Thread.Sleep(16.6);
        }

        private void VSyncCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            //System.Threading.Thread.Sleep(16.6);
            vsync = true;
        }

        private void VSyncCheckBox_UnChecked(object sender, RoutedEventArgs e)
        {
            //System.Threading.Thread.Sleep(16.6);
            vsync = false;
        }

        private void qualitySliderChange(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            quality = (int)qualitySlider.Value;
            qualitychange = true;
        }

        private void P1radioButton_Checked(object sender, RoutedEventArgs e)
        {
            //System.Threading.Thread.Sleep(60);
            Positions[(int)Protein.active] = 0;
        }

        private void P2radioButton_Checked(object sender, RoutedEventArgs e)
        {
            //System.Threading.Thread.Sleep(60);
            Positions[(int)Protein.active] = 1;
        }

        private void HradioButton_Checked(object sender, RoutedEventArgs e)
        {

        }

        private void AradioButton_Checked(object sender, RoutedEventArgs e)
        {

        }






        //Menu Items.
        //File menu.
        //'Exit' menu item logic.
        private void ExitClick(object sender, EventArgs e)
        {
            this.Close(); //Close window.
        }



        //View Menu
        //Increase Zoom.
        private void IncreaseZoom(object sender, RoutedEventArgs e)
        {
            zoom -= 100;
        }

        //Decrease Zoom.
        private void DecreaseZoom(object sender, RoutedEventArgs e)
        {
            zoom += 100;
        }


    }
}

﻿using System;

namespace Molecular_Docking_Application
{
    public struct tuple2 //Used synonymously with vector2
    {
        public float x, y;
    };

    public struct tuple3 //Used synonymously with vector3
    {
        public float x, y, z;
    };

    public struct tuple4 //Used synonymously with vector4
    {
        public float x, y, z, w;
    };

    public struct mat3x3
    {
        public float x1y1, x2y1, x3y1,
            x1y2, x2y2, x3y2,
            x1y3, x2y3, x3y3;
    };

    public struct mat4x4
    {
        public float x1y1, x2y1, x3y1, x4y1,
            x1y2, x2y2, x3y2, x4y2,
            x1y3, x2y3, x3y3, x4y3,
            x1y4, x2y4, x3y4, x4y4;
    };

    public class Arcball
    {
        private tuple3 clickVec;
        private tuple3 dragVec;
        private float width;
        private float height;

        public Arcball(float width, float height)
        {
            clickVec.x = clickVec.y = clickVec.z =
                dragVec.x = dragVec.y = dragVec.z = 0.0f;
            setBounds(width, height);
        }

        // Wikipedia, calculation epsilon in C#
        private static float calcEpsilon()
        {
            float machineEpsilon = 1.0f;

            while ((float)(1.0 + (machineEpsilon / 2.0)) != 1.0f)
            {
                machineEpsilon /= 2.0f;
            }

            return machineEpsilon;
        }

        private tuple2 tuple2Add(tuple2 t1, tuple2 t2)
        {
            tuple2 newTuple2;
            newTuple2.x = t1.x + t2.x;
            newTuple2.y = t1.y + t2.y;
            return newTuple2;
        }

        private tuple2 tuple2Sub(tuple2 t1, tuple2 t2)
        {
            tuple2 newTuple2;
            newTuple2.x = t1.x - t2.x;
            newTuple2.y = t1.y - t2.y;
            return newTuple2;
        }

        private tuple3 tuple3CrossProd(tuple3 t1, tuple3 t2)
        {
            tuple3 newTuple3;
            newTuple3.x = t1.y * t2.z - t1.z * t2.y;
            newTuple3.y = t1.z * t2.x - t1.x * t2.z;
            newTuple3.z = t1.x * t2.y - t1.y * t2.x;
            return newTuple3;
        }

        private float tuple3DotProd(tuple3 t1, tuple3 t2)
        {
            return t1.x * t2.x + t1.y * t2.y + t1.z * t2.z;
        }

        private float tuple2LengthSquared(tuple2 t)
        {
            return t.x * t.x + t.y * t.y;
        }

        private float tuple2Length(tuple2 t)
        {
            return (float)Math.Sqrt((double)tuple2LengthSquared(t));
        }

        private float tuple3LengthSquared(tuple3 t)
        {
            return t.x * t.x + t.y * t.y + t.z * t.z;
        }

        private float tuple3Length(tuple3 t)
        {
            return (float)Math.Sqrt((double)tuple3LengthSquared(t));
        }

        private mat3x3 mat3x3Zeros()
        {
            mat3x3 newMat3x3;
            newMat3x3.x1y1 = newMat3x3.x2y1 = newMat3x3.x3y1 =
                newMat3x3.x1y2 = newMat3x3.x2y2 = newMat3x3.x3y2 =
                newMat3x3.x1y3 = newMat3x3.x2y3 = newMat3x3.x3y3 = 0.0f;
            return newMat3x3;
        }

        public mat3x3 mat3x3Identity()
        {
            mat3x3 newMat3x3 = mat3x3Zeros();
            newMat3x3.x1y1 = newMat3x3.x2y2 = newMat3x3.x3y3 = 1.0f;
            return newMat3x3;
        }

        public mat3x3 mat3x3Multiply(mat3x3 m1, mat3x3 m2)
        {
            mat3x3 newMat3x3;
            newMat3x3.x1y1 = m1.x1y1 * m2.x1y1 + m1.x2y1 * m2.x1y2 + m1.x3y1 * m2.x1y3;
            newMat3x3.x2y1 = m1.x1y1 * m2.x2y1 + m1.x2y1 * m2.x2y2 + m1.x3y1 * m2.x2y3;
            newMat3x3.x3y1 = m1.x1y1 * m2.x2y1 + m1.x2y1 * m2.x2y2 + m1.x3y1 * m2.x2y3;
            newMat3x3.x1y2 = m1.x1y2 * m2.x1y1 + m1.x2y2 * m2.x1y2 + m1.x3y2 * m2.x1y3;
            newMat3x3.x2y2 = m1.x1y2 * m2.x2y1 + m1.x2y2 * m2.x2y2 + m1.x3y2 * m2.x2y3;
            newMat3x3.x3y2 = m1.x1y2 * m2.x3y1 + m1.x2y2 * m2.x3y2 + m1.x3y2 * m2.x3y3;
            newMat3x3.x1y3 = m1.x1y3 * m2.x1y1 + m1.x2y3 * m2.x1y2 + m1.x3y3 * m2.x1y3;
            newMat3x3.x2y3 = m1.x1y3 * m2.x2y1 + m1.x2y3 * m2.x2y2 + m1.x3y3 * m2.x2y3;
            newMat3x3.x3y3 = m1.x1y3 * m2.x3y1 + m1.x2y3 * m2.x3y2 + m1.x3y3 * m2.x3y3;
            return newMat3x3;
        }

        private void setBounds(float width, float height)
        {
            this.width = 1.0f / ((width - 1.0f) * 0.5f);
            this.height = 1.0f / ((height - 1.0f) * 0.5f);
        }

        public mat3x3 mat3x3FromQuaternion(tuple4 t)
        {
            float n = (float)Math.Sqrt(t.x * t.x + t.y * t.y + t.z * t.z + t.w * t.w);
            t.x /= n;
            t.y /= n;
            t.z /= n;
            t.w /= n;
            mat3x3 newMat3x3;
            newMat3x3.x1y1 = 1.0f - 2.0f * t.y * t.y - 2.0f * t.z * t.z;
            newMat3x3.x2y1 = 2.0f * t.x * t.y + 2.0f * t.w * t.z;
            newMat3x3.x3y1 = 2.0f * t.x * t.z - 2.0f * t.w * t.y;
            newMat3x3.x1y2 = 2.0f * t.x * t.y - 2.0f * t.w * t.z;
            newMat3x3.x2y2 = 1.0f - 2.0f * t.x * t.x - 2.0f * t.z * t.z;
            newMat3x3.x3y2 = 2.0f * t.y * t.z + 2.0f * t.w * t.x;
            newMat3x3.x1y3 = 2.0f * t.x * t.z + 2.0f * t.w * t.y;
            newMat3x3.x2y3 = 2.0f * t.y * t.z - 2.0f * t.w * t.x;
            newMat3x3.x3y3 = 1.0f - 2.0f * t.x * t.x - 2.0f * t.y * t.y;
            return newMat3x3;
        }

        private mat4x4 mat4x4Zeros() {
            mat4x4 returnMat;
            returnMat.x1y1 = returnMat.x2y1 = returnMat.x3y1 = returnMat.x4y1 =
                returnMat.x1y2 = returnMat.x2y2 = returnMat.x3y2 = returnMat.x4y2 =
                returnMat.x1y3 = returnMat.x2y3 = returnMat.x3y3 = returnMat.x4y3 =
                returnMat.x1y4 = returnMat.x2y4 = returnMat.x3y4 = returnMat.x4y4 = 0.0f;
            return returnMat;
        }

        public mat4x4 mat4x4Identity()
        {
            mat4x4 returnMat = mat4x4Zeros();
            returnMat.x1y1 = returnMat.x2y2 = returnMat.x3y3 = returnMat.x4y4 = 1.0f;
            return returnMat;
        }

        private mat4x4 mat4x4Scale(mat4x4 m, float scale)
        {
            mat4x4 returnMat = m;
            returnMat.x1y1 *= scale;
            returnMat.x2y1 *= scale;
            returnMat.x3y1 *= scale;
            returnMat.x1y2 *= scale;
            returnMat.x2y2 *= scale;
            returnMat.x3y2 *= scale;
            returnMat.x1y3 *= scale;
            returnMat.x2y3 *= scale;
            returnMat.x3y3 *= scale;
            return returnMat;
        }

        private float mat4x4SVD(mat4x4 m)
        {
            return (float)Math.Sqrt((m.x1y1 * m.x1y1 + m.x2y1 * m.x2y1 + m.x3y1 * m.x3y1 + m.x1y2 * m.x1y2 + m.x2y2 * m.x2y2 + m.x3y2 * m.x3y2 + m.x1y3 * m.x1y3 + m.x2y3 * m.x2y3 + m.x3y3 * m.x3y3) / 3.0);
        }

        private mat4x4 mat4x4SetRotationScaleFromMat3x3(mat4x4 m1, mat3x3 m2)
        {
            mat4x4 returnMat = m1;
            returnMat.x1y1 = m2.x1y1;
            returnMat.x2y1 = m2.x2y1;
            returnMat.x3y1 = m2.x3y1;
            returnMat.x1y2 = m2.x1y2;
            returnMat.x2y2 = m2.x2y2;
            returnMat.x3y2 = m2.x3y2;
            returnMat.x1y3 = m2.x1y3;
            returnMat.x2y3 = m2.x2y3;
            returnMat.x3y3 = m2.x3y3;
            return returnMat;
        }

        private mat4x4 mat4x4MultiplyRotationByScale(mat4x4 m, float scale)
        {
            mat4x4 returnMat = m;
            m.x1y1 *= scale;
            m.x2y1 *= scale;
            m.x3y1 *= scale;
            m.x1y2 *= scale;
            m.x2y2 *= scale;
            m.x3y2 *= scale;
            m.x1y3 *= scale;
            m.x2y3 *= scale;
            m.x3y3 *= scale;
            return returnMat;
        }

        public mat4x4 mat4x4SetRotationFromMat3x3(mat4x4 m1, mat3x3 m2)
        {
            mat4x4 returnMat = m1;
            float scale = mat4x4SVD(m1);
            returnMat = mat4x4SetRotationScaleFromMat3x3(returnMat, m2);
            returnMat = mat4x4MultiplyRotationByScale(returnMat, scale);
            return returnMat;
        }

        private tuple3 mapToSphere(tuple2 t)
        {
            tuple3 returnTuple;
            tuple2 temp = t;
            temp.x = temp.x * width - 1.0f;
            temp.y = temp.y * height;
            float lengthSquared = tuple2LengthSquared(t);
            if (lengthSquared > 1.0)
            {
                float norm = 1.0f / tuple2Length(t);
                returnTuple.x = temp.x * norm;
                returnTuple.y = temp.y * norm;
                returnTuple.z = 0.0f;
            }
            else
            {
                returnTuple.x = temp.x;
                returnTuple.y = temp.y;
                returnTuple.z = (float)Math.Sqrt(1.0f - lengthSquared);
            }
            return returnTuple;
        }

        public void click(tuple2 t)
        {
            clickVec = mapToSphere(t);
        }

        public tuple4 drag(tuple2 t)
        {
            dragVec = mapToSphere(t);
            tuple4 returnTuple;
            tuple3 perpendicular = tuple3CrossProd(clickVec, dragVec);
            if (tuple3Length(perpendicular) > calcEpsilon())
            {
                returnTuple.x = perpendicular.x;
                returnTuple.y = perpendicular.y;
                returnTuple.z = perpendicular.z;
                returnTuple.w = tuple3DotProd(clickVec, dragVec);
            }
            else
            {
                returnTuple.x = returnTuple.y = returnTuple.z = returnTuple.w = 0.0f;
            }
            return returnTuple;
        }

        /*public float[] mat3x3ToArray(mat3x3 m)
        {

        }

        public float[] mat4x4ToArray(mat4x4 m)
        { 
        
        }*/
    }
}
﻿using System;

namespace Molecular_Docking_Application
{
    public class Arcball
    {
        private float[] clickVec;
        private float[] dragVec;
        private float width;
        private float height;

        public Arcball(float width, float height)
        {
            clickVec = new float[3]{0.0f,0.0f,0.0f};
            dragVec = new float[3]{0.0f,0.0f,0.0f};
            setBounds(width, height);
        }

        // Wikipedia, calculation epsilon in C#
        private static float calcEpsilon()
        {
            float machineEpsilon = 1.0f;

            while ((float)(1.0 + (machineEpsilon / 2.0)) != 1.0f)
            {
                machineEpsilon /= 2.0f;
            }

            return machineEpsilon;
        }

        private float[] vector3CrossProd(float[] v1, float[] v2)
        {
            return new float[3]{v1[1] * v2[2] - v1[2] * v2[1], v1[2] * v2[0] - v1[0] * v2[2], v1[0] * v2[1] - v1[1] * v2[0]};
        }

        private float vector3DotProd(float[] v1, float[] v2)
        {
            return v1[0] * v2[0] + v1[1] * v2[1] + v1[2] * v2[2];
        }

        private float vector3LengthSquared(float[] v)
        {
            return v[0] * v[0] + v[1] * v[1] + v[2] * v[2];
        }

        private float vector3Length(float[] v)
        {
            return (float)Math.Sqrt((double)vector3LengthSquared(v));
        }

        private float[] mat3x3Zeros()
        {
            return new float[9]{0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f};
        }

        public float[] mat3x3Identity()
        {
            float[] result = mat3x3Zeros();
            result[0] = result[4] = result[8] = 0.0f;
            return result;
        }

        public float[] mat3x3Multiply(float[] v1, float[] v2)
        {
            float[] result = new float[9];
            result[0] = v1[0] * v2[0] + v1[1] * v2[3] + v1[2] * v2[6];
            result[1] = v1[0] * v2[1] + v1[1] * v2[4] + v1[2] * v2[7];
            result[2] = v1[0] * v2[1] + v1[1] * v2[4] + v1[2] * v2[7];
            result[3] = v1[3] * v2[0] + v1[4] * v2[3] + v1[5] * v2[6];
            result[4] = v1[3] * v2[1] + v1[4] * v2[4] + v1[5] * v2[7];
            result[5] = v1[3] * v2[2] + v1[4] * v2[5] + v1[5] * v2[8];
            result[6] = v1[6] * v2[0] + v1[7] * v2[3] + v1[8] * v2[6];
            result[7] = v1[6] * v2[1] + v1[7] * v2[4] + v1[8] * v2[7];
            result[8] = v1[6] * v2[2] + v1[7] * v2[5] + v1[8] * v2[8];
            return result;
        }

        private void setBounds(float width, float height)
        {
            this.width = 1.0f / ((width - 1.0f) * 0.5f);
            this.height = 1.0f / ((height - 1.0f) * 0.5f);
        }

        public float[] mat3x3FromQuaternion(float[] v)
        {
            float n = (float)Math.Sqrt(v[0] * v[0] + v[1] * v[1] + v[2] * v[2] + v[3] * v[3]);
            v[0] /= n;
            v[1] /= n;
            v[2] /= n;
            v[3] /= n;
            float[] result = new float[9];
            result[0] = 1.0f - 2.0f * v[1] * v[1] - 2.0f * v[2] * v[2];
            result[1] = 2.0f * v[0] * v[1] + 2.0f * v[3] * v[2];
            result[2] = 2.0f * v[0] * v[2] - 2.0f * v[3] * v[1];
            result[3] = 2.0f * v[0] * v[1] - 2.0f * v[3] * v[2];
            result[4] = 1.0f - 2.0f * v[0] * v[0] - 2.0f * v[2] * v[2];
            result[5] = 2.0f * v[1] * v[2] + 2.0f * v[3] * v[0];
            result[6] = 2.0f * v[0] * v[2] + 2.0f * v[3] * v[1];
            result[7] = 2.0f * v[1] * v[2] - 2.0f * v[3] * v[0];
            result[8] = 1.0f - 2.0f * v[0] * v[0] - 2.0f * v[1] * v[1];
            return result;
        }

        private float[] mat4x4Zeros()
        {
            return new float[16]{0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f};
        }

        public float[] mat4x4Identity()
        {
            float[] result = mat4x4Zeros();
            result[0] = result[5] = result[10] = result[15] = 0.0f;
            return result;
        }

        private float[] mat4x4Scale(float[] m, float scale)
        {
            float[] result = m;
            result[0] *= scale;
            result[1] *= scale;
            result[2] *= scale;
            result[3] *= scale;
            result[4] *= scale;
            result[5] *= scale;
            result[6] *= scale;
            result[7] *= scale;
            result[8] *= scale;
            return result;
        }

        private float mat4x4SVD(float[] m)
        {
            return (float)Math.Sqrt((m[0] * m[0] + m[1] * m[1] + m[2] * m[2] + m[4] * m[4] + m[5] * m[5] + m[6] * m[6] + m[8] * m[8] + m[9] * m[9] + m[10] * m[10]) / 3.0);
        }

        private float[] mat4x4SetRotationScaleFromMat3x3(float[] m1, float[] m2)
        {
            float[] result = m1;
            result[0] = m2[0];
            result[1] = m2[1];
            result[2] = m2[2];
            result[4] = m2[4];
            result[5] = m2[5];
            result[6] = m2[6];
            result[8] = m2[8];
            result[9] = m2[9];
            result[10] = m2[10];
            return result;
        }

        private float[] mat4x4MultiplyRotationByScale(float[] m, float scale)
        {
            float[] result = m;
            result[0] *= scale;
            result[1] *= scale;
            result[2] *= scale;
            result[4] *= scale;
            result[5] *= scale;
            result[6] *= scale;
            result[8] *= scale;
            result[9] *= scale;
            result[10] *= scale;
            return result;
        }

        public float[] mat4x4SetRotationFromMat3x3(float[] m1, float[] m2)
        {
            float[] result = m1;
            float scale = mat4x4SVD(m1);
            result = mat4x4SetRotationScaleFromMat3x3(result, m2);
            result = mat4x4MultiplyRotationByScale(result, scale);
            return result;
        }

        private float[] mapToSphere(float[] p)
        {
            float[] result = new float[3];
            float[] temp = p;
            p[0] = p[0] * width - 1.0f;
            p[1] = p[1] * height;
            float lengthSquared = vector3LengthSquared(p);
            if (lengthSquared > 1.0)
            {
                float norm = 1.0f / vector3Length(p);
                result[0] = temp[0] * norm;
                result[1] = temp[1] * norm;
                result[2] = 0.0f;
            }
            else
            {
                result[0] = temp[0];
                result[1] = temp[1];
                result[2] = (float)Math.Sqrt(1.0f - lengthSquared);
            }
            return result;
        }

        public void click(float[] v)
        {
            clickVec = mapToSphere(v);
        }

        public float[] drag(float[] p)
        {
            dragVec = mapToSphere(p);
            float[] result = new float[4];
            float[] perpendicular = vector3CrossProd(clickVec, dragVec);
            if (vector3Length(perpendicular) > calcEpsilon())
            {
                result[0] = perpendicular[0];
                result[1] = perpendicular[1];
                result[2] = perpendicular[2];
                result[3] = vector3DotProd(clickVec, dragVec);
            }
            else
            {
                result[0] = result[1] = result[2] = result[3] = 0.0f;
            }
            return result;
        }
    }
}
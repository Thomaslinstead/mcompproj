﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Windows;

namespace Molecular_Docking_Application
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private void Application_Startup(object sender, StartupEventArgs e)
        {
            SplashScreen splash = new SplashScreen("splash 2.png");
            splash.Show(false);
            System.Threading.Thread.Sleep(2000);
            splash.Close(TimeSpan.FromMilliseconds(5));
            InitializeComponent();
        }
    }
}

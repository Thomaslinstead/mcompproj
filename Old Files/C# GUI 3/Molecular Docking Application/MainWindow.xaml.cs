﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Runtime.InteropServices;
using System.Drawing;
using System.IO;
using System.Drawing.Imaging;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using OpenTK.Input;
using System.Windows.Forms;
using System.Diagnostics;
using System.Threading.Tasks;
using NAudio;
using NAudio.Wave;
using HelixToolkit.Wpf;
using System.Windows.Media.Media3D;

namespace Molecular_Docking_Application
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        //Mouse states.
        MouseState current = OpenTK.Input.Mouse.GetState();
        MouseState previous = OpenTK.Input.Mouse.GetState();

        const double movementSpeed = 1; //Global Movement Speed.

        //GUI Variables
        bool consoleon = true;  //Boolean if console is activated.
        bool logger = false;    //Boolean if logging system is activated.
        bool vsync = true;      //Boolean if VSync is activated. 
        bool hydro = false;     //Boolean if drawing hydrogens is activated.
        bool hearforces = false;//Boolean if audio feedback is activated.
        int quality = 32;       //Quality slider number of polygons per sphere.
        bool qualitychange = false;  //Boolean if quality slider has moved
        bool MainWinActive;     //Boolean main window in focus.

        //State Variables
        bool colliding = false; //Boolean if the molecules are in collision.
        bool startedTranslating = false;  //Boolean if molecule is forced translating with mouse. 
        int activeprotein = 1;  //ID for which protein is being controlled.
        bool forcesOff = false; //Disable forces
        bool currentClosest = false; 

        //Class Objects
        RenderSphere p = new RenderSphere();
        SineWave sineWaveProvider = new SineWave();
        Stopwatch sw = new Stopwatch();
        private GLControl glc;
        private WaveOut waveOut; 

        //Force viewer objects.
        Model3DGroup modelGroup = new Model3DGroup();
        PipeVisual3D pipeA = new PipeVisual3D();
        PipeVisual3D pipeR = new PipeVisual3D();
        bool enableFV;
        
        //FPS Variables
        double accumulator = 0;
        int idleCounter = 0;
        static int f = 0;
        static double tot = 0;
        float deltaTime = 0;

        //Haptic Variables
        private bool hapticsEnabled = false;
        private bool hapticsAreInit = false;
        private bool hapticTranslation = false;
        private float hapticMinX = -100;
        private float hapticMaxX = 100;
        private float hapticMinY = -80;
        private float hapticMaxY = 80;
        private float hapticMinZ = -50;
        private float hapticMaxZ = 50;
        private float sceneDepth = 1000;
        private float hapticsClamp = 1.5f;

        //Default Filepaths
        string filepath = "PDB Test Files\\1CRN(327).pdb";
        string filepath2 = "PDB Test Files\\1CRN(327).pdb";
        string filename = "...";
        string filename2 = "...";

        //Arcball Variables
        static Arcball arcball = new Arcball(640.0f, 480.0f, 640.0f);  // ArcBall Instance
        MouseState mouse = OpenTK.Input.Mouse.GetState();              // Current Mouse Point
        Vector3x1 cursorPt = new Vector3x1();                          // Current Mouse or Haptic Device Point
        Vector3x1 prevCursorPt = new Vector3x1();                      // Previous Mouse or Haptic Device Point
        bool leftClick = false;                                        // Clicking The Left Mouse / Top Haptic Button?
        bool rightClick = false;                                       // Clicking The Right Mouse / Bottom Haptic Button?
        bool isRotating = false;                                       // Dragging The Mouse?
        bool isTranslating = false;                                    // Translation Occurring?
        float mouseWheel = 0.0F;                                       // Current Mouse Wheel Position
        float prevMouseWheel = 0.0F;                                   // Previous Mouse Wheel Position
        int[] viewport = new int[4];                                   // To Store Copy of OpenTK Viewport
        double[] modelviewData = new double[16];                       // To Store Copy of OpenTK Modelview Matrix
        double[] projectionData = new double[16];                      // To Store OpenTK Projection Matrix
        Matrix4d modelviewmatrix = new Matrix4d();                     // To Store Modelview Matrix in Matrix Form
        Matrix4d projectionmatrix = new Matrix4d();                    // To Store Projection Matrix in Matrix Form
        double projX, projY, projZ;                                    // Projected X, Y and Z-Coordinates
        double unprojX, unprojY, unprojZ;                              // Unprojected X, Y and Z-Coordinates
        static float ZOOM = 500;                                       // Main Zoom
        float ZOOM_Single = 0;                                         // Single Protein View Zoom
        double transThresh = 150;                                      // Translation Proximity Threshold
        int testColls;
        float rotationX = 1.57079633f, rotationY = 0.0f;

        // ROTATION VARIABLES
        /* Protein 1: */
        Matrix4x4 Rotation_p1 = arcball.Matrix4x4Identity(); // Final Rotation
        Matrix3x3 LastRot_p1 = arcball.Matrix3x3Identity(); // Last Rotation
        Matrix3x3 ThisRot_p1 = arcball.Matrix3x3Identity(); // This Rotation
        Matrix4x4 Rotation_p1_single = arcball.Matrix4x4Identity(); // Final Rotation - SINGLE VIEW
        Matrix3x3 LastRot_p1_single = arcball.Matrix3x3Identity(); // Last Rotation - SINGLE VIEW
        Matrix3x3 ThisRot_p1_single = arcball.Matrix3x3Identity(); // This Rotation - SINGLE VIEW

        /* Protein 2 */
        Matrix4x4 Rotation_p2 = arcball.Matrix4x4Identity(); // Final Rotation
        Matrix3x3 LastRot_p2 = arcball.Matrix3x3Identity(); // Last Rotation
        Matrix3x3 ThisRot_p2 = arcball.Matrix3x3Identity(); // This Rotation
        Matrix4x4 Rotation_p2_single = arcball.Matrix4x4Identity(); // Final Rotation - SINGLE VIEW
        Matrix3x3 LastRot_p2_single = arcball.Matrix3x3Identity(); // Last Rotation - SINGLE VIEW
        Matrix3x3 ThisRot_p2_single = arcball.Matrix3x3Identity(); // This Rotation - SINGLE VIEW


        // TRANSLATION VARIABLES
        /* Protein 1 */
        Matrix4x4 Translation_p1 = arcball.Matrix4x4Identity();

        /* Protein 2 */
        Matrix4x4 Translation_p2 = arcball.Matrix4x4Identity();

        //Clipping Variables
        bool clippingEnabled = false;
        int clippingTarget = 0; //0-both, 1-p1, 2-p2
        static double clipZ = 1.0F;
        float clipIncrement = 5.0F;
        int viewingMode = 0; // 0-both, 1-p1, 2-p1
        float viewingModeCount = 0;
        float viewingModeThreshold = 50;

        //****************DLL Method Imports******************//

        [DllImport("AtomData.dll", CharSet = CharSet.Ansi)]
        private static extern bool init(string p1, string p2);

        [DllImport("AtomData.dll", CharSet=CharSet.Ansi)]
        private static extern bool loadAtom(int id, string p1);

        [DllImport("AtomData.dll")]
        private static extern int getProteinSize(int id);

        [DllImport("AtomData.dll")]
        private static extern float getAtomX(int id, int i);

        [DllImport("AtomData.dll")]
        private static extern float getAtomY(int id, int i);

        [DllImport("AtomData.dll")]
        private static extern float getAtomZ(int id, int i);

        [DllImport("AtomData.dll")]
        private static extern float getAtomR(int id, int i);

        [DllImport("AtomData.dll")]
        private static extern float getAtomG(int id, int i);

        [DllImport("AtomData.dll")]
        private static extern float getAtomB(int id, int i);

        [DllImport("AtomData.dll")]
        private static extern float getAtomRadius(int id, int i);

        [DllImport("AtomData.dll")]
        private static extern void setCollision(int id, int i);

        [DllImport("AtomData.dll")]
        private static extern void resetCollision(int id, int i);

        [DllImport("AtomData.dll")]
         private static extern void resetForceValue();

        [DllImport("AtomData.dll")]
        private static extern bool canRender(int id, int i);

        [DllImport("AtomData.dll")]
        private static extern int BruteForceCollision(float paramA, float paramB, float x, float y, float z, float x2, float y2, float z2);

        [DllImport("AtomData.dll")]
        private static extern float getForceVectorX();

        [DllImport("AtomData.dll")]
        private static extern float getForceVectorY();

        [DllImport("AtomData.dll")]
        private static extern float getForceVectorZ();

        [DllImport("AtomData.dll")]
        private static extern void buildRegularGrid(int id, float tx, float ty, float tz); 

        [DllImport("AtomData.dll")]
        private static extern void populateGrid(int id);

        [DllImport("AtomData.dll")]
        private static extern void buildRegularGridTwo(int id, float tx, float ty, float tz);

        [DllImport("AtomData.dll")]
        private static extern void populateGridTwo(int id);

        [DllImport("AtomData.dll")]
        private static extern float getxmin();

        [DllImport("AtomData.dll")]
        private static extern float getxmax();

        [DllImport("AtomData.dll")]
        private static extern float getymin();

        [DllImport("AtomData.dll")]
        private static extern float getymax();

        [DllImport("AtomData.dll")]
        private static extern float getzmin();

        [DllImport("AtomData.dll")]
        private static extern float getzmax();

        [DllImport("AtomData.dll")]
        private static extern float getxmin2();

        [DllImport("AtomData.dll")]
        private static extern float getxmax2();

        [DllImport("AtomData.dll")]
        private static extern float getymin2();

        [DllImport("AtomData.dll")]
        private static extern float getymax2();

        [DllImport("AtomData.dll")]
        private static extern float getzmin2();

        [DllImport("AtomData.dll")]
        private static extern float getzmax2();

        [DllImport("AtomData.dll")]
        private static extern void findOverlap(int t1x, int t1y, int t1z, int t2x, int t2y, int t2z);

        [DllImport("AtomData.dll")]
        private static extern void updateRotMats(int whichMat, int whichVal, float val);

        [DllImport("AtomData.dll")]
        private static extern int findGridCollisions(float x1, float y1, float z1, float x2, float y2, float z2);

        [DllImport("AtomData.dll")]
        private static extern int calculateForce(float x, float y, float z, float x2, float y2, float z2);

        [DllImport("AtomData.dll")]
        private static extern int totalColliding();

        [DllImport("AtomData.dll")]
        private static extern bool ReleaseMemory(int id);

        [DllImport("AtomData.dll")]
        private static extern void highlightCollisions(bool r);

        [DllImport("AtomData.dll")]
        private static extern void setMatrix(int whichMat, int whichVal, float val);

        [DllImport("AtomData.dll")]
        private static extern void applyTranslation(int proteinID, float x, float y, float z);

        [DllImport("AtomData.dll")]
        private static extern void renderHydrogen(bool f);

        [DllImport("femModel4.dll")]
        private static extern void setForceXHaptics(float x);

        [DllImport("femModel4.dll")]
        private static extern void setForceYHaptics(float y);

        [DllImport("femModel4.dll")]
        private static extern void setForceZHaptics(float z);

        [DllImport("femModel4.dll")]
        private static extern void initHaptics();

        [DllImport("femModel4.dll")]
        private static extern void enableForcesHaptics(bool active);

        [DllImport("femModel4.dll")]
        private static extern float getPositionXHaptics();

        [DllImport("femModel4.dll")]
        private static extern float getPositionYHaptics();

        [DllImport("femModel4.dll")]
        private static extern float getPositionZHaptics();

        [DllImport("femModel4.dll")]
        private static extern int getBottomButtonDownHaptics();

        [DllImport("femModel4.dll")]
        private static extern int getTopButtonDownHaptics();

        [DllImport("AtomData.dll")]
        private static extern int getpop1();

        [DllImport("AtomData.dll")]
        private static extern int getpop2();

        /**********************************************/

        public MainWindow()
        {
            InitializeComponent();
        }

        /* On Window load Method. */
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            glc = new GLControl(); // Create the GLControl.

            // Assign Load and Paint events of GLControl.
            glc.Load += new EventHandler(glc_Load);
            glc.Paint += new PaintEventHandler(glc_Paint);

            host.Child = glc; // Assign the GLControl as the host control's child.
        }

        /* On GLControl load. Init Function */
        void glc_Load(object sender, EventArgs e)
        {
            //Define dimension variables
            int w = glc.Width;
            int h = glc.Height;
            
            //Initialise Atom DLL Data
            if (init("PDB Test Files\\C(1).pdb", "PDB Test Files\\C(1).pdb") == false)
            {
                string messagetext = "Error loading base molecule files.";
                string caption = "Error loading base molecule files.";
                MessageBoxButton button = MessageBoxButton.OK;
                MessageBoxImage icon = MessageBoxImage.Error;
                MessageBoxResult messageBoxResult = System.Windows.MessageBox.Show(messagetext, caption, button, icon);
            }

            p.InitialiseLighting();  //Initialise OpenGL Lighting
            p.makeLists(quality);    //Initialise Sphere Display List

            //Antialiasing setup.
            GL.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);
            GL.Hint(HintTarget.PolygonSmoothHint, HintMode.Nicest);

            //Initial GUI text setup.
            Load1DataText.Text = "Molecule 1 size: " + getProteinSize(1).ToString();
            Load2DataText.Text = "Molecule 2 size: " + getProteinSize(2).ToString();
            loadText1.Text = filename;
            loadText2.Text = filename2;

            //Default Zoom Parameter
            ZOOM = 500 + (getProteinSize(1) + getProteinSize(2)) * 0.15f;

            //Setup Audio Sound Parameters 
            sineWaveProvider.SetWaveFormat(16000, 1); // 16kHz mono    
            sineWaveProvider.Frequency = 600;
            sineWaveProvider.Amplitude = (float)(0.25);
            waveOut = new WaveOut();
            waveOut.Init(sineWaveProvider);

            //Default disable rendering of collisions
            highlightCollisions(false);

            //Set up Force Viewer Camera.
            myView.DefaultCamera = new PerspectiveCamera();
            myView.DefaultCamera.Position = new Point3D(0, 0, 40);
            myView.DefaultCamera.LookDirection = new Vector3D(0, 0, -40);
            myView.DefaultCamera.UpDirection = new Vector3D(0, 1, 0);

            //Create Force Viewer sphere.
            SphereVisual3D sphere = new SphereVisual3D();
            sphere.Radius = 2;
            sphere.Center = new Point3D(0, 0, 0);
            var brush = new SolidColorBrush();
            var color = System.Windows.Media.Color.FromArgb(255, 255, 0, 0);
            brush.Color = color;
            sphere.Fill = brush;
            modelGroup.Children.Add(sphere.Content); //Add sphere to modelGroup (for rendering).

            //Initial protein transforms
            Translation_p1.R4C1 = -200.0f;
            Translation_p1.R4C3 = 0;
            Translation_p2.R4C1 = 200.0f;
            Translation_p2.R4C3 = 0;
        }

        /* Function to Enable Haptics if not Already Enabled */
        void enableHaptics()
        {
            if (!hapticsAreInit)
            {
                initHaptics();
                hapticsAreInit = true;
            }
        }

        /* Cross Product Function */
        Vector3 crossprod(float a, float b, float c, float d, float e, float f, float g, float h, float i)
        {
            Vector3 v0 = new Vector3(a, b, c);
            Vector3 v1 = new Vector3(d, e, f);
            Vector3 v2 = new Vector3(g, h, i);

            Vector3 x = new Vector3(v2[0] - v0[0], v2[1] - v0[1], v2[2] - v0[2]);
            Vector3 y = new Vector3(v1[0] - v0[0], v1[1] - v0[1], v1[2] - v0[2]);

            Vector3 normal = new Vector3(
            x[1] * y[2] - y[1] * x[2],
            x[2] * y[0] - y[2] * x[0],
            x[0] * y[1] - y[0] * x[1]);

            return normal;
        }


        //Create a frame.
        void glc_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
        {
            Application_Idle(); //Calculate FPS
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

            // Set up initial modes
            int w = glc.Width;
            int h = glc.Height;
            // Update arcball variables
            arcball.setBounds((float)w, (float)h, sceneDepth);

            //Camera Projection Matrix Setup
            GL.Viewport(0, 0, w, h);
            GL.MatrixMode(MatrixMode.Projection);
            GL.LoadIdentity();
            Matrix4 perspective = Matrix4.Perspective(35.0f * (float)(Math.PI / 180), (float)w / (float)h, 0.1f, 4000.0f);
            GL.MultMatrix(ref perspective);
            GL.MatrixMode(MatrixMode.Modelview);
            GL.LoadIdentity();
            Matrix4 lookat = Matrix4.LookAt((float)((ZOOM) * Math.Cos(rotationX)), rotationY, (float)((ZOOM) * Math.Sin(rotationX)), 0, 0, 0, 0, 1, 0); //Position and aim the camera.
            GL.MultMatrix(ref lookat);

            // Render Proteins, Viewing Mode: 0 - Both Proteins, 1 - Protein 1 Single Protein View, 2 - Protein 2 Single Protein View
            if (viewingMode == 0)
            {
                GL.PushMatrix();
                renderProtein(1);
                GL.PopMatrix();

                GL.PushMatrix();
                renderProtein(2);
                GL.PopMatrix();
            }
            else if (viewingMode == 1)
            {
                GL.PushMatrix();
                // Translate zoom
                GL.Translate(0, 0, -ZOOM_Single);
                // Rotate protein
                GL.MultMatrix(Rotation_p1_single.toArray());
                // Translate protein to centre of scene
                GL.Translate(-Translation_p1.R4C1, -Translation_p1.R4C2, Translation_p1.R4C3);
                // Render protein
                renderProtein(1);
                GL.PopMatrix();
            }
            else if (viewingMode == 2)
            {
                GL.PushMatrix();
                //Translate zoom
                GL.Translate(0, 0, -ZOOM_Single);
                // Rotate protein
                GL.MultMatrix(Rotation_p2_single.toArray());
                // Translate protein to centre of scene
                GL.Translate(-Translation_p2.R4C1, -Translation_p2.R4C2, ZOOM_Single - Translation_p2.R4C3);
                // Render protein
                renderProtein(2);
                GL.PopMatrix();
            }

            // If haptics are enabled, render haptic cursor
            if (hapticsEnabled)
            {
                GL.PushMatrix();
                if(!hapticTranslation)
                    drawHapticCursor();
                GL.PopMatrix();
            }

            //Swap Buffers and Update
            glc.SwapBuffers();
            glc.Invalidate();
            update();

            //Update Force Viewer if enabled.
            if (enableFV == true)
            {
                FV_Enable();
            }
            else //reset point on FV to ensure no pipes are shown.
            {
                pipeA.Point2 = new Point3D(0, 0, 0);
                pipeR.Point2 = new Point3D(0, 0, 0);
            }
        }

        /* Clamp Movement Values Method. */
        int clamp(int t)
        {
            if (!colliding) //Only Return if molecules are colliding
            {
                return 0;
            }

            if (t > movementSpeed) //If Greater than normal speed, clamp result.
            {
                return (int)movementSpeed;
            }
            else if (t < -movementSpeed) //If less than normal speed, clamp result.
            {
                return (int)-movementSpeed;
            } else {
                return (int)t / 10; //Return result scaled down.
            }
        }

        /* Clamp Force Values Method. */
        float clampForce(float force, float clamp)
        {
            if (force > clamp) //If Greater than normal speed, clamp result.
            {
                return clamp;
            }
            else if (force < -clamp) //If less than normal speed, clamp result.
            {
                return -clamp;
            }
            else
            {
                return force; //Return result scaled down.
            }
        }

        /* Keyboard input method. */
        void KeyPress()
        {
            //Check if Window is active and input text boxes do not have focus.
            if (MainWinActive == true && !loadText1.IsFocused && !loadText2.IsFocused)
            {
                if (activeprotein == 1) //Controls for protein 1
                {
                    if (System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.A)) Translation_p1.R4C1 -= ((int)movementSpeed) + clamp((int)getForceVectorX());
                    if (System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.D)) Translation_p1.R4C1 += ((int)movementSpeed) - clamp((int)getForceVectorX());
                    if (System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.S)) Translation_p1.R4C2 -= ((int)movementSpeed) + clamp((int)getForceVectorY());
                    if (System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.W)) Translation_p1.R4C2 += ((int)movementSpeed) - clamp((int)getForceVectorY());
                    if (System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.Q)) Translation_p1.R4C3 -= ((int)movementSpeed) + clamp((int)getForceVectorZ());
                    if (System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.E)) Translation_p1.R4C3 += ((int)movementSpeed) - clamp((int)getForceVectorZ());
                }
                else  //Controls for protein 2
                {
                    if (System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.A)) Translation_p2.R4C1 -= ((int)movementSpeed) - clamp((int)getForceVectorX());
                    if (System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.D)) Translation_p2.R4C1 += ((int)movementSpeed) + clamp((int)getForceVectorX());
                    if (System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.S)) Translation_p2.R4C2 -= ((int)movementSpeed) - clamp((int)getForceVectorY());
                    if (System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.W)) Translation_p2.R4C2 += ((int)movementSpeed) + clamp((int)getForceVectorY());
                    if (System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.Q)) Translation_p2.R4C3 -= ((int)movementSpeed) - clamp((int)getForceVectorZ());
                    if (System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.E)) Translation_p2.R4C3 += ((int)movementSpeed) + clamp((int)getForceVectorZ());
                }
            }

            if (System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.D1)) //Reset Position of Protein 1
            {
                Translation_p1.R4C1 = -50.0f + -((float)(getxmax() - getxmin()) * 0.5f); 
                Translation_p1.R4C2 = 0;
                Translation_p1.R4C3 = 0;
                startedTranslating = true;
            }

            if (System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.D2)) //Reset Position of Protein 2
            {
                Translation_p2.R4C1 = 50.0f + ((float)(getxmax2() - getxmin2()) * 0.5f);
                Translation_p2.R4C2 = 0;
                Translation_p2.R4C3 = 0;
                startedTranslating = true;
            }


            if (System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.O)) clipZ -= clipIncrement;
            if (System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.L)) clipZ += clipIncrement;
        }

        /*Update Method Called after drawing scene */
        void update()
        {
            //Update X/Y/Z force numbers in text.
            CFX.Text = "X: " + getForceVectorX();
            CFY.Text = "Y: " + getForceVectorY();
            CFZ.Text = "Z: " + getForceVectorZ();

            //CF.Text = "X: " + getForceVectorX() + "Y: " + getForceVectorY() + "Z: " + getForceVectorZ();
            

            if (logger == true)
            {
                using (StreamWriter w = File.AppendText("log.txt"))
                {
                    w.WriteLine("" + getForceVectorX() + " " + getForceVectorY() + " " + getForceVectorZ() + " ");
                }
            }

            //Force update of text elements in GUI.
            loadText1.UpdateLayout();
            loadText2.UpdateLayout();
            TextBoxDataError.UpdateLayout();
            Load1DataText.UpdateLayout();
            Load2DataText.UpdateLayout();
            FPSTextBlock.UpdateLayout();
            myView.Focus();


            current = OpenTK.Input.Mouse.GetState();

            //Play update of audio feedback.
            if (hearforces == true)
            {
                waveOut.Play();
            }
            else waveOut.Stop();


            //Check if Windows is active.
            if (MainWinActive == true)
            {
                //Provide Force To Haptics Device.
                if (hapticsEnabled)
                {
                    // Get the vector between the two proteins and compare to the force vector to identify if the force is attractive or repulsive. 
                    Vector3 protein1 = new Vector3(Translation_p1.R4C1, Translation_p1.R4C2, Translation_p1.R4C3);
                    Vector3 protein2 = new Vector3(Translation_p2.R4C1, Translation_p2.R4C2, Translation_p2.R4C3);
                    Vector3 vec = protein2 - protein1;
                    float dot = (vec.X * getForceVectorX()) + (vec.Y * getForceVectorY()) + (vec.Z * getForceVectorZ());
                    double mag1 = Math.Sqrt((vec.X * vec.X + vec.Y * vec.Y + vec.Z * vec.Z));
                    double mag2 = Math.Sqrt((getForceVectorX() * getForceVectorX() + getForceVectorY() * getForceVectorY() + getForceVectorZ() * getForceVectorZ()));
                    double radianAngle = Math.Acos(dot / (mag1 + mag2));
                    double angle = radianAngle * (180 / Math.PI);

                    if (angle > 90)
                    {
                        if (activeprotein == 1)
                        {
                            setForceXHaptics(clampForce(-getForceVectorX(), hapticsClamp));
                            setForceYHaptics(clampForce(-getForceVectorY(), hapticsClamp));
                            setForceZHaptics(clampForce(-getForceVectorZ(), hapticsClamp));
                        }
                        else
                        {
                            setForceXHaptics(clampForce(getForceVectorX(), hapticsClamp));
                            setForceYHaptics(clampForce(getForceVectorY(), hapticsClamp));
                            setForceZHaptics(clampForce(getForceVectorZ(), hapticsClamp));
                        }
                    }
                    else
                    {
                        if (activeprotein == 1)
                        {
                            setForceXHaptics(clampForce(-getForceVectorX() / 10, hapticsClamp));
                            setForceYHaptics(clampForce(-getForceVectorY() / 10, hapticsClamp));
                            setForceZHaptics(clampForce(-getForceVectorZ() / 10, hapticsClamp));
                        }
                        else
                        {
                            setForceXHaptics(clampForce(getForceVectorX() / 10, hapticsClamp));
                            setForceYHaptics(clampForce(getForceVectorY() / 10, hapticsClamp));
                            setForceZHaptics(clampForce(getForceVectorZ() / 10, hapticsClamp));
                        }
                    }

                }

                //////Update the Mouse Variables////////
                mouse = OpenTK.Input.Mouse.GetState();
                prevMouseWheel = mouseWheel;
                mouseWheel = mouse.WheelPrecise;
                prevCursorPt = cursorPt;

                if (!hapticsEnabled)
                {
                    // Set cursor position to mouse position relative to OpenTK scene with dummy Z-Coordinate
                    cursorPt.X = GLControl.MousePosition.X;
                    cursorPt.Y = GLControl.MousePosition.Y;
                    System.Drawing.Point tempMouse = glc.PointToClient(new System.Drawing.Point((int)cursorPt.X, (int)cursorPt.Y));
                    cursorPt.X = tempMouse.X;
                    cursorPt.Y = glc.Height - tempMouse.Y;
                    cursorPt.Z = 10000;

                    // Check mouse buttons
                    if (mouse.IsButtonDown(OpenTK.Input.MouseButton.Left))
                    {
                        leftClick = true;
                    }
                    else
                    {
                        leftClick = false;
                        isRotating = false;
                    }

                    if (mouse.IsButtonDown(OpenTK.Input.MouseButton.Right))
                    {
                        rightClick = true;
                    }
                    else
                    {
                        rightClick = false;
                        isTranslating = false;
                    }
                }
                else
                {
                    // Set cursor position to haptic position mapped to world coordinates
                    cursorPt.X = hapticToWorldMap(getPositionXHaptics()/2, 1);
                    cursorPt.Y = hapticToWorldMap(getPositionYHaptics()/2, 2);
                    cursorPt.Z = hapticToWorldMap(getPositionZHaptics()/2, 3);
                    // Check haptic buttons
                    int topButton = getTopButtonDownHaptics();
                    int bottomButton = getBottomButtonDownHaptics();

                    if (topButton == 1)
                    {
                        leftClick = true;
                    }
                    else
                    {
                        leftClick = false;
                        isRotating = false;
                    }

                    if (bottomButton == 1)
                    {
                        rightClick = true;
                    }
                    else
                    {
                        // Haptic Translation
                        if (hapticTranslation)
                        {
                            if (activeprotein == 1)
                            {
                                Translation_p1.R4C1 -= getPositionXHaptics()*2; 
                                Translation_p1.R4C2 -= getPositionYHaptics()*2;
                                Translation_p1.R4C3 -= getPositionZHaptics()*2;
                            }
                            else
                            {
                                Translation_p2.R4C1 -= getPositionXHaptics()*2;
                                Translation_p2.R4C2 -= getPositionYHaptics()*2;
                                Translation_p2.R4C3 -= getPositionZHaptics()*2;
                            }
                            hapticTranslation = false;
                        }
                        rightClick = false;
                        isTranslating = false;
                    }
                }

                //Update viewing and rotations
                if (leftClick && rightClick)
                {
                    viewingModeCount += deltaTime;
                }
                else if (leftClick && (glc.Focused == true || hapticsEnabled))
                {
                    rotateProtein();
                }
                else if (rightClick)
                {
                    if (viewingMode == 0)
                    {
                        if (isTranslating)
                        {
                            translateProtein(closerToP1());
                        }
                        else
                        {
                            float tempX = 0, tempY = 0;
                            if (!hapticsEnabled)
                            {
                                unProject(new Vector4x1(cursorPt.X, cursorPt.Y, (float)projZ, 1.0F));
                                tempX = (float)unprojX;
                                tempY = (float)unprojY;
                            }
                            else
                            {
                                tempX = cursorPt.X;
                                tempY = cursorPt.Y;
                            }
                            if (closerToP1())
                            {
                                float distX = tempX - Translation_p1.R4C1;
                                float distY = tempY - Translation_p1.R4C2;
                                if (distX < 0)
                                    distX *= -1;
                                if (distY < 0)
                                    distY *= -1;
                                if (distX * distX + distY * distY < transThresh * transThresh && (glc.Focused == true || hapticTranslation))
                                {
                                    isTranslating = true;
                                    activeprotein = 1;
                                    startedTranslating = true;
                                    this.P1radioButton.IsChecked = true;
                                    this.P2radioButton.IsChecked = false;
                                }
                            }
                            else
                            {
                                float distX = tempX - Translation_p2.R4C1;
                                float distY = tempY - Translation_p2.R4C2;
                                if (distX < 0)
                                    distX *= -1;
                                if (distY < 0)
                                    distY *= -1;
                                if (distX * distX + distY * distY < transThresh * transThresh && (glc.Focused == true || hapticTranslation))
                                {
                                    isTranslating = true;
                                    activeprotein = 2;
                                    startedTranslating = true;
                                    this.P2radioButton.IsChecked = true;
                                    this.P1radioButton.IsChecked = false;
                                }
                            }
                        }
                    }
                }

                if (viewingModeCount > viewingModeThreshold)
                {
                    if (viewingMode != 0)
                    {
                        viewingMode = 0;
                    }
                    else
                    {
                        // Setup variables for single protein view mode
                        if (closerToP1())
                        {
                            viewingMode = 1;
                            Rotation_p1_single = arcball.Matrix4x4Identity();
                            ThisRot_p1_single = arcball.Matrix3x3Identity();
                            LastRot_p1_single = arcball.Matrix3x3Identity();
                            ZOOM_Single = 0;
                        }
                        else
                        {
                            viewingMode = 2;
                            Rotation_p2_single = arcball.Matrix4x4Identity();
                            ThisRot_p2_single = arcball.Matrix3x3Identity();
                            LastRot_p2_single = arcball.Matrix3x3Identity();
                            ZOOM_Single = 0;
                        }
                    }
                    viewingModeCount = 0;
                }

                // Adjust correct zoom variable
                if (mouseWheel < prevMouseWheel)
                {
                    if (viewingMode == 0)
                        ZOOM += 100;
                    else
                        ZOOM_Single += 100;
                }
                else if (mouseWheel > prevMouseWheel)
                {
                    if (viewingMode == 0)
                        ZOOM -= 100;
                    else
                        ZOOM_Single -= 100;
                }
                ////////////////////////////////////

                //Check if focus is on OpenTK (graphics area is being clicked).
                if (glc.Focused)
                {
                    //Set focus away if no longer clicked.
                    if (!current.IsButtonDown(OpenTK.Input.MouseButton.Left))
                        GridMain.Focus();
                }
            }

            previous = current;
            glc.VSync = vsync; //Set vertical refresh sync.

            if (qualitychange == true) //If quality has been changed, remake lists.
            {
                p.makeLists(quality);
                qualitychange = false;
            }


            KeyPress(); //Check for keypresses

            //********** Update Collisions and forces ************//

            //Temporarily convert rotations matrices to a format the DLL can interpret
            float[] r1mat =  {Rotation_p1.R1C1, Rotation_p1.R1C2, Rotation_p1.R1C3, Rotation_p1.R1C4,
                              Rotation_p1.R2C1, Rotation_p1.R2C2, Rotation_p1.R2C3, Rotation_p1.R2C4,
                              Rotation_p1.R3C1, Rotation_p1.R3C2, Rotation_p1.R3C3, Rotation_p1.R3C4,
                              Rotation_p1.R4C1, Rotation_p1.R4C2, Rotation_p1.R4C3, Rotation_p1.R4C4};

            float[] r2mat =  {Rotation_p2.R1C1, Rotation_p2.R1C2, Rotation_p2.R1C3, Rotation_p2.R1C4,
                              Rotation_p2.R2C1, Rotation_p2.R2C2, Rotation_p2.R2C3, Rotation_p2.R2C4,
                              Rotation_p2.R3C1, Rotation_p2.R3C2, Rotation_p2.R3C3, Rotation_p2.R3C4,
                              Rotation_p2.R4C1, Rotation_p2.R4C2, Rotation_p2.R4C3, Rotation_p2.R4C4};

            //Send Translations to the DLL
            if (hapticsEnabled && hapticTranslation)
            {
                if (activeprotein == 1)
                {
                    applyTranslation(1, Translation_p1.R4C1 - getPositionXHaptics()*2, Translation_p1.R4C2 - getPositionYHaptics()*2, Translation_p1.R4C3 - getPositionZHaptics()*2);
                    applyTranslation(2, Translation_p2.R4C1, Translation_p2.R4C2, Translation_p2.R4C3);
                }
                else
                {
                    applyTranslation(1, Translation_p1.R4C1, Translation_p1.R4C2, Translation_p1.R4C3);
                    applyTranslation(2, Translation_p2.R4C1 - getPositionXHaptics()*2, Translation_p2.R4C2 - getPositionYHaptics()*2, Translation_p2.R4C3 - getPositionZHaptics()*2);
                }
            }
            else
            {
                applyTranslation(1, Translation_p1.R4C1, Translation_p1.R4C2, Translation_p1.R4C3);
                applyTranslation(2, Translation_p2.R4C1, Translation_p2.R4C2, Translation_p2.R4C3);
            }
            
            //Update DLL's rotation matrices
            for (int i = 0; i < 16; i++) {
                setMatrix(1, i, r1mat[i]);
                setMatrix(2, i, r2mat[i]);
            }

            colliding = false;

            //Rebuild Grids.
            buildRegularGrid(1, Translation_p1.R4C1, Translation_p1.R4C2, Translation_p1.R4C3);
            populateGrid(1);
            buildRegularGridTwo(2, Translation_p2.R4C1, Translation_p2.R4C2, Translation_p2.R4C3);
            populateGridTwo(2);
            

            int collidingI = 0; //Set Current Colliding count to zero.
            
            //Perform Grid Collisions if activated.
            if (!forcesOff)
            {
                //Find colliding atoms from grid.
                testColls = findGridCollisions(Translation_p1.R4C1, Translation_p1.R4C2, Translation_p1.R4C3, Translation_p2.R4C1, Translation_p2.R4C2, Translation_p2.R4C3);
                //Recalculate the forces using colliding atoms. 
                collidingI = calculateForce(Translation_p1.R4C1, Translation_p1.R4C2, Translation_p1.R4C3, Translation_p2.R4C1, Translation_p2.R4C2, Translation_p2.R4C3);
            }

            if (collidingI > 0) {
                colliding = true;
                sineWaveProvider.colliding = true;
            }
            else
            {
                sineWaveProvider.colliding = false;
            }

            //Push colliding molecules appart. 
           if (colliding && startedTranslating)
            {
                double mag = Math.Sqrt((getForceVectorX() * getForceVectorX()) + (getForceVectorY() * getForceVectorY()) + (getForceVectorZ() * getForceVectorZ()));
                if (mag > 100000) //Very large force, attempt to fix intercolliding molecules with repulsion.
                {
                    if (activeprotein == 2)
                    {
                        Translation_p1.R4C1 += (float)(-getForceVectorX() / mag);
                        Translation_p1.R4C2 += (float)(-getForceVectorY() / mag);
                        Translation_p1.R4C3 += (float)(-getForceVectorZ() / mag);
                    }
                    else
                    {
                        Translation_p2.R4C1 += (float)(getForceVectorX() / mag);
                        Translation_p2.R4C2 += (float)(getForceVectorY() / mag);
                        Translation_p2.R4C3 += (float)(getForceVectorZ() / mag);
                    }
                }
            }
            else
            {
                startedTranslating = false;
            }

            ///////////////////////////////////////////////////////////////////
        }

        /* Method to render the atoms of a protein */
        void renderProtein(int id)
        {
            for (int i = 0; i < getProteinSize(id); i++)  //Loop all the atoms.
            {
                if (canRender(id, i) || hydro) //Check if hydrogen and can render it.
                {
                    if (!clippingEnabled || (clippingEnabled && clippingTarget != 0 && clippingTarget != id)) //Check for clipping
                    {
                        GL.PushMatrix();
                        GL.Enable(EnableCap.Normalize);
                        p.renderSphere(getAtomX(id, i), getAtomY(id, i), getAtomZ(id, i), getAtomR(id, i), getAtomG(id, i), getAtomB(id, i), getAtomRadius(id, i)); //Draw Atom
                        GL.Disable(EnableCap.Normalize);
                        GL.PopMatrix();
                        resetCollision(id, i);
                    }
                    else
                    {
                        if (getAtomZ(id, i) < clipZ)
                        {
                            GL.PushMatrix();
                            GL.Enable(EnableCap.Normalize);
                            p.renderSphere(getAtomX(id, i), getAtomY(id, i), getAtomZ(id, i), getAtomR(id, i), getAtomG(id, i), getAtomB(id, i), getAtomRadius(id, i)); //Draw Atom
                            GL.Disable(EnableCap.Normalize);
                            GL.PopMatrix();
                            resetCollision(id, i);
                        }
                    }
                }
            }
        }

        /* Method to Rotate a Protein */
        void rotateProtein()
        {
            if (viewingMode == 0)
            {
                if (closerToP1())
                {
                    //Rotate protein 1
                    if (!isRotating)
                    {
                        isRotating = true;
                        LastRot_p1 = ThisRot_p1;
                        arcball.click(cursorPt);
                    }
                    else
                    {
                        Vector4x1 ThisQuat = new Vector4x1();
                        ThisQuat = arcball.drag(cursorPt);
                        ThisRot_p1 = arcball.Matrix3x3SetRotationFromVector4x1(ThisQuat);
                        ThisRot_p1 = arcball.Matrix3x3Mult(LastRot_p1, ThisRot_p1);
                        Rotation_p1 = arcball.Matrix4x4SetRotationFromMatrix3x3(Rotation_p1, ThisRot_p1);
                        activeprotein = 1;
                        startedTranslating = true;
                        this.P1radioButton.IsChecked = true;
                        this.P2radioButton.IsChecked = false;  
                    }
                }
                else
                {
                    //Rotate protein 2
                    if (!isRotating)
                    {
                        isRotating = true;
                        LastRot_p2 = ThisRot_p2;
                        arcball.click(cursorPt);
                    }
                    else
                    {
                        Vector4x1 ThisQuat = new Vector4x1();
                        ThisQuat = arcball.drag(cursorPt);
                        ThisRot_p2 = arcball.Matrix3x3SetRotationFromVector4x1(ThisQuat);
                        ThisRot_p2 = arcball.Matrix3x3Mult(ThisRot_p2, LastRot_p2);
                        Rotation_p2 = arcball.Matrix4x4SetRotationFromMatrix3x3(Rotation_p2, ThisRot_p2);
                        isTranslating = true;
                        activeprotein = 2;
                        startedTranslating = true;
                        this.P2radioButton.IsChecked = true;
                        this.P1radioButton.IsChecked = false;
                    }
                }
            }
            else
            {
                if (viewingMode == 1)
                {
                    //Rotate protein 1
                    if (!isRotating)
                    {
                        isRotating = true;
                        LastRot_p1_single = ThisRot_p1_single;
                        arcball.click(cursorPt);
                    }
                    else
                    {
                        Vector4x1 ThisQuat = new Vector4x1();
                        ThisQuat = arcball.drag(cursorPt);
                        ThisRot_p1_single = arcball.Matrix3x3SetRotationFromVector4x1(ThisQuat);
                        ThisRot_p1_single = arcball.Matrix3x3Mult(LastRot_p1_single, ThisRot_p1_single);
                        Rotation_p1_single = arcball.Matrix4x4SetRotationFromMatrix3x3(Rotation_p1_single, ThisRot_p1_single);
                    }
                }
                else
                {
                    //Rotate protein 2
                    if (!isRotating)
                    {
                        isRotating = true;
                        LastRot_p2_single = ThisRot_p2_single;
                        arcball.click(cursorPt);
                    }
                    else
                    {
                        Vector4x1 ThisQuat = new Vector4x1();
                        ThisQuat = arcball.drag(cursorPt);
                        ThisRot_p2_single = arcball.Matrix3x3SetRotationFromVector4x1(ThisQuat);
                        ThisRot_p2_single = arcball.Matrix3x3Mult(ThisRot_p2_single, LastRot_p2_single);
                        Rotation_p2_single = arcball.Matrix4x4SetRotationFromMatrix3x3(Rotation_p2_single, ThisRot_p2_single);
                    }
                }
            }
        }

        /* Method to Translate a Protein */
        void translateProtein(bool p1)
        {
            if (!hapticsEnabled)
            {
                float oldX = (float)unprojX;
                float oldY = (float)unprojY;
                unProject(new Vector4x1(cursorPt.X, cursorPt.Y, (float)projZ, 1.0F));
                if (p1)
                {
                    isTranslating = true;
                    activeprotein = 1;
                    startedTranslating = true;
                    this.P1radioButton.IsChecked = true;
                    this.P2radioButton.IsChecked = false; 

                    //Translate protein 1
                    Translation_p1.R4C1 += ((float)unprojX - oldX);
                    Translation_p1.R4C2 += ((float)unprojY - oldY);
                    if ((float)unprojX - oldX < 0)
                        Translation_p1.R4C1 += clamp((int)getForceVectorX());
                    else
                        Translation_p1.R4C1 -= clamp((int)getForceVectorX());
                    if ((float)unprojY - oldY < 0)
                        Translation_p1.R4C1 += clamp((int)getForceVectorY());
                    else
                        Translation_p1.R4C1 -= clamp((int)getForceVectorY());
                }
                else
                {
                    isTranslating = true;
                    activeprotein = 2;
                    startedTranslating = true;
                    this.P2radioButton.IsChecked = true;
                    this.P1radioButton.IsChecked = false; 

                    //Translate protein 2
                    Translation_p2.R4C1 += ((float)unprojX - oldX);
                    Translation_p2.R4C2 += ((float)unprojY - oldY);
                    if ((float)unprojX - oldX < 0)
                        Translation_p2.R4C1 -= clamp((int)getForceVectorX());
                    else
                        Translation_p2.R4C1 += clamp((int)getForceVectorX());
                    if ((float)unprojY - oldY < 0)
                        Translation_p2.R4C1 -= clamp((int)getForceVectorY());
                    else
                        Translation_p2.R4C1 += clamp((int)getForceVectorY());
                }
            }
            else
            {
                hapticTranslation = true;
            }
        }

        /* Function to Check if Cursor is Closer to Protein 1 or Protein 2 */
        bool closerToP1()
        {
            double p1_x, p1_y, p2_x, p2_y;
            float l1, l2;
            if (!hapticsEnabled)
            {
                // Project protein 1 world translation coordinates to screen coordinates
                Vector4x1 extractedTrans = new Vector4x1(Translation_p1.R4C1, Translation_p1.R4C2, Translation_p1.R4C3, Translation_p1.R4C4);
                project(extractedTrans);
                p1_x = projX;
                p1_y = projY;
                // Project protein 2 world translation coordinates to screen coordinates
                extractedTrans = new Vector4x1(Translation_p2.R4C1, Translation_p2.R4C2, Translation_p2.R4C3, Translation_p2.R4C4);
                project(extractedTrans);
                p2_x = projX;
                p2_y = projY;
                // Calculate distances between positions on screen to mouse point
                l1 = (float)((cursorPt.X - p1_x) * (cursorPt.X - p1_x) + (cursorPt.Y - p1_y) * (cursorPt.Y - p1_y));
                l2 = (float)((cursorPt.X - p2_x) * (cursorPt.X - p2_x) + (cursorPt.Y - p2_y) * (cursorPt.Y - p2_y));
            }
            else
            {
                // Get world coordinates of proteins
                p1_x = Translation_p1.R4C1;
                p1_y = Translation_p1.R4C2;
                p2_x = Translation_p2.R4C1;
                p2_y = Translation_p2.R4C2;
                // Calculate distance between positions in world to world haptic point
                l1 = (float)((cursorPt.X - p1_x) * (cursorPt.X - p1_x) + (cursorPt.Y - p1_y) * (cursorPt.Y - p1_y));
                l2 = (float)((cursorPt.X - p2_x) * (cursorPt.X - p2_x) + (cursorPt.Y - p2_y) * (cursorPt.Y - p2_y));
            }
            if (isTranslating || isRotating)
            {
                return currentClosest;
            }

            if (l1 < l2)
            {
                currentClosest = true;
                return true;
            }
            else
            {
                currentClosest = false;
                return false;
            }
        }

        /* Function to Project World Coordinates to Screen Coordinates */
        void project(Vector4x1 point)
        {
            GL.GetInteger(GetPName.Viewport, viewport);
            GL.GetDouble(GetPName.ModelviewMatrix, modelviewData);
            GL.GetDouble(GetPName.ProjectionMatrix, projectionData);
            modelviewmatrix = new Matrix4d(modelviewData[0], modelviewData[1], modelviewData[2], modelviewData[3], modelviewData[4], modelviewData[5], modelviewData[6], modelviewData[7], modelviewData[8], modelviewData[9], modelviewData[10], modelviewData[11], modelviewData[12], modelviewData[13], modelviewData[14], modelviewData[15]);
            projectionmatrix = new Matrix4d(projectionData[0], projectionData[1], projectionData[2], projectionData[3], projectionData[4], projectionData[5], projectionData[6], projectionData[7], projectionData[8], projectionData[9], projectionData[10], projectionData[11], projectionData[12], projectionData[13], projectionData[14], projectionData[15]);
            gluProject(point.X, point.Y, point.Z, modelviewmatrix, projectionmatrix, viewport, ref projX, ref projY, ref projZ);
        }

        /* Function to Unproject Screen Coordinates to World Coordinates */
        void unProject(Vector4x1 point)
        {
            GL.GetInteger(GetPName.Viewport, viewport);
            GL.GetDouble(GetPName.ModelviewMatrix, modelviewData);
            GL.GetDouble(GetPName.ProjectionMatrix, projectionData);
            modelviewmatrix = new Matrix4d(modelviewData[0], modelviewData[1], modelviewData[2], modelviewData[3], modelviewData[4], modelviewData[5], modelviewData[6], modelviewData[7], modelviewData[8], modelviewData[9], modelviewData[10], modelviewData[11], modelviewData[12], modelviewData[13], modelviewData[14], modelviewData[15]);
            projectionmatrix = new Matrix4d(projectionData[0], projectionData[1], projectionData[2], projectionData[3], projectionData[4], projectionData[5], projectionData[6], projectionData[7], projectionData[8], projectionData[9], projectionData[10], projectionData[11], projectionData[12], projectionData[13], projectionData[14], projectionData[15]);
            gluUnProject(point.X, point.Y, point.Z, modelviewmatrix, projectionmatrix, viewport, ref unprojX, ref unprojY, ref unprojZ);
        }

        /* OpenTK port of OpenGL gluProject function http://www.opentk.com/files/Glu.cs */
        int gluProject(double objx, double objy, double objz, Matrix4d modelMatrix, Matrix4d projMatrix, int[] viewport, ref double winx, ref double winy, ref double winz)
        {
            Vector4d _in;
            Vector4d _out;

            _in.X = objx;
            _in.Y = objy;
            _in.Z = objz;
            _in.W = 1.0;
            _out = Vector4d.Transform(_in, modelMatrix);
            _in = Vector4d.Transform(_out, projMatrix);

            if (_in.W == 0.0)
                return (0);
            _in.X /= _in.W;
            _in.Y /= _in.W;
            _in.Z /= _in.W;
            // Map x, y and z to range 0-1
            _in.X = _in.X * 0.5 + 0.5;
            _in.Y = _in.Y * 0.5 + 0.5;
            _in.Z = _in.Z * 0.5 + 0.5;

            // Map x,y to viewport
            _in.X = _in.X * viewport[2] + viewport[0];
            _in.Y = _in.Y * viewport[3] + viewport[1];

            winx = _in.X;
            winy = _in.Y;
            winz = _in.Z;
            return (1);
        }

        /* OpenTK port of OpenGL gluUnProject function http://www.opentk.com/files/Glu.cs */
        static int gluUnProject(double winx, double winy, double winz, Matrix4d modelMatrix, Matrix4d projMatrix, int[] viewport, ref double objx, ref double objy, ref double objz)
        {
            Matrix4d finalMatrix;
            Vector4d _in;
            Vector4d _out;

            finalMatrix = Matrix4d.Mult(modelMatrix, projMatrix);

            finalMatrix.Invert();

            _in.X = winx;
            _in.Y = winy;
            _in.Z = winz;
            _in.W = 1.0;

            // Map x and y from window coordinates
            _in.X = (_in.X - viewport[0]) / viewport[2];
            _in.Y = (_in.Y - viewport[1]) / viewport[3];

            // Map to range -1 to 1
            _in.X = _in.X * 2 - 1;
            _in.Y = _in.Y * 2 - 1;
            _in.Z = _in.Z * 2 - 1;

            _out = Vector4d.Transform(_in, finalMatrix);

            if (_out.W == 0.0)
                return (0);
            _out.X /= _out.W;
            _out.Y /= _out.W;
            _out.Z /= _out.W;
            objx = _out.X;
            objy = _out.Y;
            objz = _out.Z;
            return (1);
        }

        /* Function to Map Haptic Coordinates to World Coordinates */
        float hapticToWorldMap(float val, int axis)
        {
            if (axis == 1)          // x axis
            {
                if (val < hapticMinX)
                    val = hapticMinX;
                else if (val > hapticMaxX)
                    val = hapticMaxX;

                val /= hapticMinX;
                val *= glc.Width / 2;
            }
            else if (axis == 2)     // y axis
            {
                if (val < hapticMinY)
                    val = hapticMinY;
                else if (val > hapticMaxY)
                    val = hapticMaxY;

                val /= hapticMinY;
                val *= glc.Height / 2;
            }
            else                    //z axis
            {
                if (val < hapticMinZ)
                    val = hapticMinZ;
                else if (val > hapticMaxZ)
                    val = hapticMaxZ;

                val /= hapticMinZ;
                val *= sceneDepth / 2;
            }
            return val;
        }

        /* Function to Draw Haptic Cursor */
        void drawHapticCursor()
        {
                    GL.PushMatrix();
                    GL.Enable(EnableCap.Normalize);
                    //Note z-coord is temporary
                    p.renderSphere(cursorPt.X, cursorPt.Y, cursorPt.Z, 0.0F, 0.0F, 0.0F, 10f);
                    GL.Disable(EnableCap.Normalize);
                    GL.PopMatrix();
        }

        //Set variable defining if the window is active.
        void MainWinActivated(object sender, EventArgs e)
        {
            MainWinActive = true;
        }

        void MainWinDeactivated(object sender, EventArgs e)
        {
            MainWinActive = false;
        }


        private void LoadButton1_Click(object sender, RoutedEventArgs e)
        {
            // Configure open file dialog box
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
            dlg.FileName = ""; // Default file name
            filename = dlg.SafeFileName;//FileName;
            dlg.DefaultExt = ".pdb"; // Default file extension
            dlg.Filter = "Program Debug Database (.pdb)|*.pdb"; // Filter files by extension 

            // Show open file dialog box
            Nullable<bool> result = dlg.ShowDialog();

            // Process open file dialog box results 
            if (result == true)
            {
                // Open document 
                filepath = dlg.FileName;
                filename = dlg.SafeFileName;

                loadAtom(1, filepath);

                buildRegularGrid(1, Translation_p1.R4C1, Translation_p1.R4C2, Translation_p1.R4C3);
                populateGrid(1);

                //Position correctly
                ZOOM = 500 + (getProteinSize(1) + getProteinSize(2))*0.15f;
                Translation_p1.R4C1 = -50.0f + -((float)(getxmax() - getxmin()) * 0.5f); 

                //Set relevant texts.
                Load1DataText.Text = "Molecule 1 size: " + getProteinSize(1).ToString();
                loadText1.Text = filename;
                TextBoxDataError.Text = "Load Successful!";
            }
            else TextBoxDataError.Text = "Load Unsuccessful!";
        }

        private void LoadButton2_Click(object sender, RoutedEventArgs e)
        {
            // Configure open file dialog box
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
            dlg.FileName = ""; // Default file name
            filename = dlg.SafeFileName;//FileName;
            dlg.DefaultExt = ".pdb"; // Default file extension
            dlg.Filter = "Program Debug Database (.pdb)|*.pdb"; // Filter files by extension 

            // Show open file dialog box
            Nullable<bool> result = dlg.ShowDialog();

            // Process open file dialog box results 
            if (result == true)
            {
                // Open document 
                filepath2 = dlg.FileName;
                filename2 = dlg.SafeFileName;

                loadAtom(2, filepath2);

                buildRegularGridTwo(2, Translation_p2.R4C1, Translation_p2.R4C2, Translation_p2.R4C3);
                populateGridTwo(2);

                //Position correctly
                ZOOM = 500 + (getProteinSize(1) + getProteinSize(2)) * 0.15f;
                Translation_p2.R4C1 = 50.0f + ((float)(getxmax2() - getxmin2()) * 0.5f);

                //Set relevant texts.
                Load2DataText.Text = "Molecule 2 size: " + getProteinSize(2).ToString();
                loadText2.Text = filename2;
                TextBoxDataError.Text = "Load Successful!";
            }
            else TextBoxDataError.Text = "Load Unsuccessful!";
        }


        //FPS calculation.
        void Application_Idle()
        {
            double milliseconds = ComputeTimeSlice();
            Accumulate(milliseconds);
        }

        private double ComputeTimeSlice()
        {
            sw.Stop();
            double timeslice = sw.Elapsed.TotalMilliseconds;
            /////////////// AVERAGE FPS IN MILLISECONDS ////////////////
            tot += timeslice;
            f++;
            if (consoleon == true)
            {
                Console.Out.WriteLine(" " + tot / f + "ms Frame ID:" + f);
            }
            if (f > 1000)
            {
                f = 0;
                tot = 0;
            }
            ///////////////////////////////////////////////////////////
            sw.Reset();
            sw.Start();
            return timeslice;
        }

        private void Accumulate(double milliseconds)
        {
            idleCounter++;
            accumulator += milliseconds;
            // Calculate delta time
            deltaTime = (float)(60.0f / (idleCounter * (1000 / accumulator)));
            if (accumulator > 1000)
            {
                FPSTextBlock.Text = "Fps: " + idleCounter.ToString();
                accumulator -= 1000;
                idleCounter = 0; // Reset the counter.
            }
        }


        //Force Viewer.
        void FV_Enable()
        {
            //Clear current rendering.
            modelGroup.Children.Clear();

            //Redraw sphere.
            SphereVisual3D sphere = new SphereVisual3D();
            sphere.Radius = 2;
            sphere.Center = new Point3D(0, 0, 0);
            var brush = new SolidColorBrush();
            var color = System.Windows.Media.Color.FromArgb(255, 255, 0, 0);
            brush.Color = color;
            sphere.Fill = brush;
            modelGroup.Children.Add(sphere.Content);

            //Create new Pipe depending on current force vector.
            pipeA.Point1 = new Point3D(0, 0, 0);
            pipeR.Point1 = new Point3D(0, 0, 0);
            var brushA = new SolidColorBrush();
            var colorA = System.Windows.Media.Color.FromArgb(255, 0, 255, 0);
            brushA.Color = colorA;
            pipeA.Fill = brushA;


                // Get the vector between the two proteins and compare to the force vector to identify if the force is attractive or repulsive. 
                Vector3 protein1 = new Vector3(Translation_p1.R4C1, Translation_p1.R4C2, Translation_p1.R4C3);
                Vector3 protein2 = new Vector3(Translation_p2.R4C1, Translation_p2.R4C2, Translation_p2.R4C3);
                Vector3 vec = protein2 - protein1;
                float dot = (vec.X * getForceVectorX()) + (vec.Y * getForceVectorY()) + (vec.Z * getForceVectorZ());
                double mag1 = Math.Sqrt((vec.X * vec.X + vec.Y * vec.Y + vec.Z * vec.Z));
                double mag2 = Math.Sqrt((getForceVectorX() * getForceVectorX() + getForceVectorY() * getForceVectorY() + getForceVectorZ() * getForceVectorZ()));
                double radianAngle = Math.Acos(dot / (mag1 + mag2));
                double angle = radianAngle * (180 / Math.PI);

                pipeA.Point2 = new Point3D(0, 0, 0);
                if (activeprotein == 2)
                {
                    if(angle > 90)
                    {

                        pipeR.Point2 = new Point3D(0, 0, 0);
                        pipeA.Point2 = new Point3D((double)getForceVectorX()*100, (double)getForceVectorY()*100, (double)getForceVectorZ()*100);
                        pipeA.Diameter = 0.5;
                        modelGroup.Children.Add(pipeA.Content);
                    }
                    else
                    {
                        pipeA.Point2 = new Point3D(0, 0, 0);
                        pipeR.Point2 = new Point3D((double)getForceVectorX(), (double)getForceVectorY(), (double)getForceVectorZ());
                    }
                }
                else
                {
                    if(angle > 90)
                    {

                        pipeR.Point2 = new Point3D(0, 0, 0);
                        pipeA.Point2 = new Point3D((double)-getForceVectorX() * 100, (double)-getForceVectorY() * 100, (double)-getForceVectorZ() * 100);
                        pipeA.Diameter = 0.5;
                        modelGroup.Children.Add(pipeA.Content);
                    }
                    else
                    {
                        pipeA.Point2 = new Point3D(0, 0, 0);
                        pipeR.Point2 = new Point3D((double)-getForceVectorX(), (double)-getForceVectorY(), (double)-getForceVectorZ());
                    }
                }
                pipeR.Diameter = 0.5;
                modelGroup.Children.Add(pipeR.Content);

            //Send modelGroup to ModelViewer3D for Force Viewer.
            MV3D.Content = modelGroup;
            
        }



        //Interaction logic for GUI elemetns.
        private void FFcheckBox_Checked(object sender, RoutedEventArgs e)
        {
            hapticsClamp = (float)hapticsSlider.Value;
            if (hapticsEnabled)
            {
                hapticsSlider.IsEnabled = true;
            }
            forcesOff = false;
        }

        private void FFcheckBox_UnChecked(object sender, RoutedEventArgs e)
        {
            hapticsClamp = 0;
            hapticsSlider.IsEnabled = false;
            resetForceValue();
            forcesOff = true;
        }

        private void VSyncCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            vsync = true;
        }

        private void VSyncCheckBox_UnChecked(object sender, RoutedEventArgs e)
        {
            vsync = false;
        }

        private void qualitySliderChange(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            quality = (int)qualitySlider.Value;
            qualitychange = true;
        }

        private void P1radioButton_Checked(object sender, RoutedEventArgs e)
        {
            activeprotein = 1;
        }

        private void P2radioButton_Checked(object sender, RoutedEventArgs e)
        {
            activeprotein = 2;
        }

        private void HCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            try
            {
                initHaptics();
                enableForcesHaptics(true);
                hapticsEnabled = true;
                hapticsSlider.IsEnabled = true;
            }
            catch (DllNotFoundException d)
            {
                Console.WriteLine("No haptics device found!");
            }
        }

        private void HCheckBox_UnChecked(object sender, RoutedEventArgs e)
        {
            try
            {
                if (hapticsAreInit)
                {
                    enableForcesHaptics(false);
                }
                hapticsSlider.IsEnabled = false;
                hapticsEnabled = false;
            }
            catch (DllNotFoundException d)
            {
                Console.WriteLine("No haptics device found!");
            }

        }

        private void ExitClick(object sender, EventArgs e)
        {
            this.Close(); //Close window.
        }

        //View Menu
        //Increase Zoom.
        private void IncreaseZoom(object sender, RoutedEventArgs e)
        {
            ZOOM -= 100;
        }

        //Decrease Zoom.
        private void DecreaseZoom(object sender, RoutedEventArgs e)
        {
            ZOOM += 100;
        }

        private void SFcheckBox_Checked(object sender, RoutedEventArgs e)
        {
            highlightCollisions(true); //Render collisions
        }

        private void SFcheckBox_UnChecked(object sender, RoutedEventArgs e)
        {
            highlightCollisions(false); //Do not render collisions
        }

        private void HFcheckBox_Checked(object sender, RoutedEventArgs e)
        {
            hearforces = true;
        }

        private void HFcheckBox_UnChecked(object sender, RoutedEventArgs e)
        {
            hearforces = false;
        }

        //Clipping Checkbox.
        private void CcheckBox_Checked(object sender, RoutedEventArgs e)
        {
            clippingEnabled = true;
            CRadioBoth.IsEnabled = true;
            CRadio1.IsEnabled = true;
            CRadio2.IsEnabled = true;
            CRadioBoth.IsChecked = true;
        }
        private void CcheckBox_UnChecked(object sender, RoutedEventArgs e)
        {
            clippingEnabled = false;
            CRadioBoth.IsEnabled = false;
            CRadio1.IsEnabled = false;
            CRadio2.IsEnabled = false;
        }

        private void FVButton_Click(object sender, RoutedEventArgs e)
        {
            if ((string)FV_Button.Content == "Enable")
            {
                enableFV = true;
                FV_Button.Content = "Disable";

            }
            else if ((string)FV_Button.Content == "Disable")
            {
                enableFV = false;
                FV_Button.Content = "Enable";
            }
        }

        private void AAcheckBox_Checked(object sender, RoutedEventArgs e)
        {
            GL.Enable(EnableCap.Blend);
            GL.Enable(EnableCap.PolygonSmooth);
        }
        private void AAcheckBox_UnChecked(object sender, RoutedEventArgs e)
        {
            GL.Disable(EnableCap.Blend);
            GL.Disable(EnableCap.PolygonSmooth);
        }

        private void SHcheckBox_Checked(object sender, RoutedEventArgs e)
        {
            hydro = true;
            renderHydrogen(true);
        }

        private void SHcheckBox_UnChecked(object sender, RoutedEventArgs e)
        {
            hydro = false;
            renderHydrogen(false);
        }

        private void CRadioBoth_Checked(object sender, RoutedEventArgs e)
        {
            clippingTarget = 0;
        }

        private void CRadio1_Checked(object sender, RoutedEventArgs e)
        {
            clippingTarget = 1;
        }

        private void CRadio2_Checked(object sender, RoutedEventArgs e)
        {
            clippingTarget = 2;
        }

        private void SCcheckBox_Checked(object sender, RoutedEventArgs e)
        {
            consoleon = true;
            ConsoleManager.Show();
        }

        private void SCcheckBox_UnChecked(object sender, RoutedEventArgs e)
        {
            consoleon = false;
            if(ConsoleManager.HasConsole)
            ConsoleManager.Hide();
        }

        private void TLcheckBox_Checked(object sender, RoutedEventArgs e)
        {
            logger = true;
        }

        private void TLcheckBox_UnChecked(object sender, RoutedEventArgs e)
        {
            logger = false;
        }

        private void hapticsSliderChange(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            hapticsClamp = (float)hapticsSlider.Value;
        }

        //Can be used to determine if any key is pressed.
        bool IsAnyKeyPressed()
        {
            var allPossibleKeys = Enum.GetValues(typeof(System.Windows.Input.Key));
            bool results = false;
            foreach (var currentKey in allPossibleKeys)
            {
                System.Windows.Input.Key key = (System.Windows.Input.Key)currentKey;
                if (key != System.Windows.Input.Key.None)
                    if (System.Windows.Input.Keyboard.IsKeyDown((System.Windows.Input.Key)currentKey)) { results = true; break; }
            }
            return results;
        }


        

    }
}

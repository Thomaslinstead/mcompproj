﻿#pragma checksum "..\..\..\MainWindow.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "1992927A0BE8FFEBDF7F432A8743F773"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18444
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using HelixToolkit.Wpf;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Forms.Integration;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace Molecular_Docking_Application {
    
    
    /// <summary>
    /// MainWindow
    /// </summary>
    public partial class MainWindow : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 21 "..\..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid GridMain;
        
        #line default
        #line hidden
        
        
        #line 33 "..\..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Forms.Integration.WindowsFormsHost host;
        
        #line default
        #line hidden
        
        
        #line 38 "..\..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid grid1;
        
        #line default
        #line hidden
        
        
        #line 58 "..\..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox loadText1;
        
        #line default
        #line hidden
        
        
        #line 59 "..\..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button LoadButton1;
        
        #line default
        #line hidden
        
        
        #line 61 "..\..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox loadText2;
        
        #line default
        #line hidden
        
        
        #line 62 "..\..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button LoadButton2;
        
        #line default
        #line hidden
        
        
        #line 63 "..\..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock TextBoxDataError;
        
        #line default
        #line hidden
        
        
        #line 64 "..\..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock Load1DataText;
        
        #line default
        #line hidden
        
        
        #line 65 "..\..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock Load2DataText;
        
        #line default
        #line hidden
        
        
        #line 69 "..\..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.RadioButton P1radioButton;
        
        #line default
        #line hidden
        
        
        #line 74 "..\..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.RadioButton P2radioButton;
        
        #line default
        #line hidden
        
        
        #line 79 "..\..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox HCheckBox;
        
        #line default
        #line hidden
        
        
        #line 84 "..\..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock HapticsTextBlock;
        
        #line default
        #line hidden
        
        
        #line 85 "..\..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Slider hapticsSlider;
        
        #line default
        #line hidden
        
        
        #line 90 "..\..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox FFcheckBox;
        
        #line default
        #line hidden
        
        
        #line 95 "..\..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox SHcheckBox;
        
        #line default
        #line hidden
        
        
        #line 100 "..\..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox SFcheckBox;
        
        #line default
        #line hidden
        
        
        #line 105 "..\..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox HFcheckBox;
        
        #line default
        #line hidden
        
        
        #line 110 "..\..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox SCcheckBox;
        
        #line default
        #line hidden
        
        
        #line 115 "..\..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox TLcheckBox;
        
        #line default
        #line hidden
        
        
        #line 122 "..\..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Separator separator2p1;
        
        #line default
        #line hidden
        
        
        #line 123 "..\..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox CcheckBox;
        
        #line default
        #line hidden
        
        
        #line 128 "..\..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.RadioButton CRadioBoth;
        
        #line default
        #line hidden
        
        
        #line 129 "..\..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.RadioButton CRadio1;
        
        #line default
        #line hidden
        
        
        #line 130 "..\..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.RadioButton CRadio2;
        
        #line default
        #line hidden
        
        
        #line 134 "..\..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock qualityTextBlock;
        
        #line default
        #line hidden
        
        
        #line 135 "..\..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Slider qualitySlider;
        
        #line default
        #line hidden
        
        
        #line 136 "..\..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox AAcheckBox;
        
        #line default
        #line hidden
        
        
        #line 141 "..\..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox VSyncCheckBox;
        
        #line default
        #line hidden
        
        
        #line 146 "..\..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock FPSTextBlock;
        
        #line default
        #line hidden
        
        
        #line 149 "..\..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Separator separator1;
        
        #line default
        #line hidden
        
        
        #line 154 "..\..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Separator separator2;
        
        #line default
        #line hidden
        
        
        #line 155 "..\..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Separator separator3;
        
        #line default
        #line hidden
        
        
        #line 160 "..\..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Separator separator4;
        
        #line default
        #line hidden
        
        
        #line 165 "..\..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Separator separator5;
        
        #line default
        #line hidden
        
        
        #line 170 "..\..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Separator separator6;
        
        #line default
        #line hidden
        
        
        #line 179 "..\..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal HelixToolkit.Wpf.HelixViewport3D myView;
        
        #line default
        #line hidden
        
        
        #line 184 "..\..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Media.Media3D.ModelVisual3D MV3D;
        
        #line default
        #line hidden
        
        
        #line 186 "..\..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button FV_Button;
        
        #line default
        #line hidden
        
        
        #line 198 "..\..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock CFX;
        
        #line default
        #line hidden
        
        
        #line 199 "..\..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock CFY;
        
        #line default
        #line hidden
        
        
        #line 200 "..\..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock CFZ;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/Molecular Docking Application;component/mainwindow.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\MainWindow.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            
            #line 6 "..\..\..\MainWindow.xaml"
            ((Molecular_Docking_Application.MainWindow)(target)).Loaded += new System.Windows.RoutedEventHandler(this.Window_Loaded);
            
            #line default
            #line hidden
            
            #line 6 "..\..\..\MainWindow.xaml"
            ((Molecular_Docking_Application.MainWindow)(target)).Activated += new System.EventHandler(this.MainWinActivated);
            
            #line default
            #line hidden
            
            #line 6 "..\..\..\MainWindow.xaml"
            ((Molecular_Docking_Application.MainWindow)(target)).Deactivated += new System.EventHandler(this.MainWinDeactivated);
            
            #line default
            #line hidden
            return;
            case 2:
            this.GridMain = ((System.Windows.Controls.Grid)(target));
            return;
            case 3:
            this.host = ((System.Windows.Forms.Integration.WindowsFormsHost)(target));
            return;
            case 4:
            this.grid1 = ((System.Windows.Controls.Grid)(target));
            return;
            case 5:
            this.loadText1 = ((System.Windows.Controls.TextBox)(target));
            return;
            case 6:
            this.LoadButton1 = ((System.Windows.Controls.Button)(target));
            
            #line 59 "..\..\..\MainWindow.xaml"
            this.LoadButton1.Click += new System.Windows.RoutedEventHandler(this.LoadButton1_Click);
            
            #line default
            #line hidden
            return;
            case 7:
            this.loadText2 = ((System.Windows.Controls.TextBox)(target));
            return;
            case 8:
            this.LoadButton2 = ((System.Windows.Controls.Button)(target));
            
            #line 62 "..\..\..\MainWindow.xaml"
            this.LoadButton2.Click += new System.Windows.RoutedEventHandler(this.LoadButton2_Click);
            
            #line default
            #line hidden
            return;
            case 9:
            this.TextBoxDataError = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 10:
            this.Load1DataText = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 11:
            this.Load2DataText = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 12:
            this.P1radioButton = ((System.Windows.Controls.RadioButton)(target));
            
            #line 69 "..\..\..\MainWindow.xaml"
            this.P1radioButton.Checked += new System.Windows.RoutedEventHandler(this.P1radioButton_Checked);
            
            #line default
            #line hidden
            return;
            case 13:
            this.P2radioButton = ((System.Windows.Controls.RadioButton)(target));
            
            #line 74 "..\..\..\MainWindow.xaml"
            this.P2radioButton.Checked += new System.Windows.RoutedEventHandler(this.P2radioButton_Checked);
            
            #line default
            #line hidden
            return;
            case 14:
            this.HCheckBox = ((System.Windows.Controls.CheckBox)(target));
            
            #line 79 "..\..\..\MainWindow.xaml"
            this.HCheckBox.Checked += new System.Windows.RoutedEventHandler(this.HCheckBox_Checked);
            
            #line default
            #line hidden
            
            #line 79 "..\..\..\MainWindow.xaml"
            this.HCheckBox.Unchecked += new System.Windows.RoutedEventHandler(this.HCheckBox_UnChecked);
            
            #line default
            #line hidden
            return;
            case 15:
            this.HapticsTextBlock = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 16:
            this.hapticsSlider = ((System.Windows.Controls.Slider)(target));
            
            #line 85 "..\..\..\MainWindow.xaml"
            this.hapticsSlider.ValueChanged += new System.Windows.RoutedPropertyChangedEventHandler<double>(this.hapticsSliderChange);
            
            #line default
            #line hidden
            return;
            case 17:
            this.FFcheckBox = ((System.Windows.Controls.CheckBox)(target));
            
            #line 90 "..\..\..\MainWindow.xaml"
            this.FFcheckBox.Checked += new System.Windows.RoutedEventHandler(this.FFcheckBox_Checked);
            
            #line default
            #line hidden
            
            #line 90 "..\..\..\MainWindow.xaml"
            this.FFcheckBox.Unchecked += new System.Windows.RoutedEventHandler(this.FFcheckBox_UnChecked);
            
            #line default
            #line hidden
            return;
            case 18:
            this.SHcheckBox = ((System.Windows.Controls.CheckBox)(target));
            
            #line 95 "..\..\..\MainWindow.xaml"
            this.SHcheckBox.Checked += new System.Windows.RoutedEventHandler(this.SHcheckBox_Checked);
            
            #line default
            #line hidden
            
            #line 95 "..\..\..\MainWindow.xaml"
            this.SHcheckBox.Unchecked += new System.Windows.RoutedEventHandler(this.SHcheckBox_UnChecked);
            
            #line default
            #line hidden
            return;
            case 19:
            this.SFcheckBox = ((System.Windows.Controls.CheckBox)(target));
            
            #line 100 "..\..\..\MainWindow.xaml"
            this.SFcheckBox.Checked += new System.Windows.RoutedEventHandler(this.SFcheckBox_Checked);
            
            #line default
            #line hidden
            
            #line 100 "..\..\..\MainWindow.xaml"
            this.SFcheckBox.Unchecked += new System.Windows.RoutedEventHandler(this.SFcheckBox_UnChecked);
            
            #line default
            #line hidden
            return;
            case 20:
            this.HFcheckBox = ((System.Windows.Controls.CheckBox)(target));
            
            #line 105 "..\..\..\MainWindow.xaml"
            this.HFcheckBox.Checked += new System.Windows.RoutedEventHandler(this.HFcheckBox_Checked);
            
            #line default
            #line hidden
            
            #line 105 "..\..\..\MainWindow.xaml"
            this.HFcheckBox.Unchecked += new System.Windows.RoutedEventHandler(this.HFcheckBox_UnChecked);
            
            #line default
            #line hidden
            return;
            case 21:
            this.SCcheckBox = ((System.Windows.Controls.CheckBox)(target));
            
            #line 110 "..\..\..\MainWindow.xaml"
            this.SCcheckBox.Checked += new System.Windows.RoutedEventHandler(this.SCcheckBox_Checked);
            
            #line default
            #line hidden
            
            #line 110 "..\..\..\MainWindow.xaml"
            this.SCcheckBox.Unchecked += new System.Windows.RoutedEventHandler(this.SCcheckBox_UnChecked);
            
            #line default
            #line hidden
            return;
            case 22:
            this.TLcheckBox = ((System.Windows.Controls.CheckBox)(target));
            
            #line 115 "..\..\..\MainWindow.xaml"
            this.TLcheckBox.Checked += new System.Windows.RoutedEventHandler(this.TLcheckBox_Checked);
            
            #line default
            #line hidden
            
            #line 115 "..\..\..\MainWindow.xaml"
            this.TLcheckBox.Unchecked += new System.Windows.RoutedEventHandler(this.TLcheckBox_UnChecked);
            
            #line default
            #line hidden
            return;
            case 23:
            this.separator2p1 = ((System.Windows.Controls.Separator)(target));
            return;
            case 24:
            this.CcheckBox = ((System.Windows.Controls.CheckBox)(target));
            
            #line 123 "..\..\..\MainWindow.xaml"
            this.CcheckBox.Checked += new System.Windows.RoutedEventHandler(this.CcheckBox_Checked);
            
            #line default
            #line hidden
            
            #line 123 "..\..\..\MainWindow.xaml"
            this.CcheckBox.Unchecked += new System.Windows.RoutedEventHandler(this.CcheckBox_UnChecked);
            
            #line default
            #line hidden
            return;
            case 25:
            this.CRadioBoth = ((System.Windows.Controls.RadioButton)(target));
            
            #line 128 "..\..\..\MainWindow.xaml"
            this.CRadioBoth.Checked += new System.Windows.RoutedEventHandler(this.CRadioBoth_Checked);
            
            #line default
            #line hidden
            return;
            case 26:
            this.CRadio1 = ((System.Windows.Controls.RadioButton)(target));
            
            #line 129 "..\..\..\MainWindow.xaml"
            this.CRadio1.Checked += new System.Windows.RoutedEventHandler(this.CRadio1_Checked);
            
            #line default
            #line hidden
            return;
            case 27:
            this.CRadio2 = ((System.Windows.Controls.RadioButton)(target));
            
            #line 130 "..\..\..\MainWindow.xaml"
            this.CRadio2.Checked += new System.Windows.RoutedEventHandler(this.CRadio2_Checked);
            
            #line default
            #line hidden
            return;
            case 28:
            this.qualityTextBlock = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 29:
            this.qualitySlider = ((System.Windows.Controls.Slider)(target));
            
            #line 135 "..\..\..\MainWindow.xaml"
            this.qualitySlider.ValueChanged += new System.Windows.RoutedPropertyChangedEventHandler<double>(this.qualitySliderChange);
            
            #line default
            #line hidden
            return;
            case 30:
            this.AAcheckBox = ((System.Windows.Controls.CheckBox)(target));
            
            #line 136 "..\..\..\MainWindow.xaml"
            this.AAcheckBox.Checked += new System.Windows.RoutedEventHandler(this.AAcheckBox_Checked);
            
            #line default
            #line hidden
            
            #line 136 "..\..\..\MainWindow.xaml"
            this.AAcheckBox.Unchecked += new System.Windows.RoutedEventHandler(this.AAcheckBox_Checked);
            
            #line default
            #line hidden
            return;
            case 31:
            this.VSyncCheckBox = ((System.Windows.Controls.CheckBox)(target));
            
            #line 141 "..\..\..\MainWindow.xaml"
            this.VSyncCheckBox.Checked += new System.Windows.RoutedEventHandler(this.VSyncCheckBox_Checked);
            
            #line default
            #line hidden
            
            #line 141 "..\..\..\MainWindow.xaml"
            this.VSyncCheckBox.Unchecked += new System.Windows.RoutedEventHandler(this.VSyncCheckBox_UnChecked);
            
            #line default
            #line hidden
            return;
            case 32:
            this.FPSTextBlock = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 33:
            this.separator1 = ((System.Windows.Controls.Separator)(target));
            return;
            case 34:
            this.separator2 = ((System.Windows.Controls.Separator)(target));
            return;
            case 35:
            this.separator3 = ((System.Windows.Controls.Separator)(target));
            return;
            case 36:
            this.separator4 = ((System.Windows.Controls.Separator)(target));
            return;
            case 37:
            this.separator5 = ((System.Windows.Controls.Separator)(target));
            return;
            case 38:
            this.separator6 = ((System.Windows.Controls.Separator)(target));
            return;
            case 39:
            this.myView = ((HelixToolkit.Wpf.HelixViewport3D)(target));
            return;
            case 40:
            this.MV3D = ((System.Windows.Media.Media3D.ModelVisual3D)(target));
            return;
            case 41:
            this.FV_Button = ((System.Windows.Controls.Button)(target));
            
            #line 186 "..\..\..\MainWindow.xaml"
            this.FV_Button.Click += new System.Windows.RoutedEventHandler(this.FVButton_Click);
            
            #line default
            #line hidden
            return;
            case 42:
            this.CFX = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 43:
            this.CFY = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 44:
            this.CFZ = ((System.Windows.Controls.TextBlock)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}


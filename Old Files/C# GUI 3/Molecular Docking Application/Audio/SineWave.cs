﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;

namespace Molecular_Docking_Application
{
    /* Implementation of the abstract wave prodifer specifically for sine wave */
    class SineWave : AbstractWaveProvider
    {
        //Imports for DLL force vector getter methods
        [DllImport("AtomData.dll")]
        private static extern float getForceVectorX();

        [DllImport("AtomData.dll")]
        private static extern float getForceVectorY();

        [DllImport("AtomData.dll")]
        private static extern float getForceVectorZ();

        public bool colliding; //Check if atoms are in hard collision.
        int sample;            //Store the sample count
  
        public SineWave() 
        {
            colliding = false;
            Frequency = 1000;
            double m = Math.Sqrt((getForceVectorX() * getForceVectorX()) + (getForceVectorY() * getForceVectorY()) + (getForceVectorZ() * getForceVectorZ())); //Force Magnitude
            Amplitude = (float)m/100f; // (Not too loud!)           
        } 
  
        public float Frequency { get; set; } 
        public float Amplitude { get; set; } 
  
        //Overloaded Read Function
        public override int Read(float[] buffer, int offset, int sampleCount) 
        { 
            //Calculate the force magnitude
            double mag = (float)Math.Abs(Math.Sqrt((getForceVectorX() * getForceVectorX()) + (getForceVectorY() * getForceVectorY()) + (getForceVectorZ() * getForceVectorZ())));

            int sampleRate = WaveFormat.SampleRate;
            if (mag != 0 || colliding)
            {
                //Update Frequency and Amplitude according to the force vector, 
                Frequency = 100 + (float)Math.Abs(Math.Sqrt((getForceVectorX() * getForceVectorX()) + (getForceVectorY() * getForceVectorY()) + (getForceVectorZ() * getForceVectorZ())));
                Amplitude = 0.5f - (float)Math.Abs(Math.Sqrt((getForceVectorX() * getForceVectorX()) + (getForceVectorY() * getForceVectorY()) + (getForceVectorZ() * getForceVectorZ()))) / 50f;
            }
            else
            {
                Amplitude = 0.0f; //Atoms not in collision range. 
            }

            //Clamp Parameters to avoid incorrect amplitudes
            if (Amplitude > 0.5) Amplitude = 0.5f;
            if (Amplitude < 0.0) Amplitude = 0.0f;

            //Set buffer values for sine wave
            for (int n = 0; n < sampleCount; n++) 
            { 
                buffer[n+offset] = (float)(Amplitude * Math.Sin((2 * Math.PI * sample * Frequency) / sampleRate)); 
                sample++; 
                if (sample >= sampleRate) sample = 0; 
            } 
            return sampleCount; 
        } 
    }
}

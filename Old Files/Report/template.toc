\contentsline {section}{\numberline {1}Introduction: What is Molecular Docking and Why is it Useful?}{1}{section.1}
\contentsline {section}{\numberline {2}Related Work}{2}{section.2}
\contentsline {subsection}{\numberline {2.1}Molecular Docking Techniques}{2}{subsection.2.1}
\contentsline {subsubsection}{\numberline {2.1.1}Automatic}{2}{subsubsection.2.1.1}
\contentsline {subsubsection}{\numberline {2.1.2}Interactive}{2}{subsubsection.2.1.2}
\contentsline {subsubsection}{\numberline {2.1.3}Additional Techniques}{3}{subsubsection.2.1.3}
\contentsline {subsection}{\numberline {2.2}Haptic Devices and Technology}{4}{subsection.2.2}
\contentsline {subsubsection}{\numberline {2.2.1}The PHANTOM Haptic Feedback Device}{4}{subsubsection.2.2.1}
\contentsline {subsubsection}{\numberline {2.2.2}Uses of Haptics and Haptic Devices}{6}{subsubsection.2.2.2}
\contentsline {paragraph}{\nonumberline Scientific Research}{6}{section*.5}
\contentsline {paragraph}{\nonumberline Overcoming Disabilities}{6}{section*.6}
\contentsline {paragraph}{\nonumberline Education and Training}{7}{section*.7}
\contentsline {subsection}{\numberline {2.3}Existing Software with Focus on Usability}{7}{subsection.2.3}
\contentsline {subsection}{\numberline {2.4}Collision Detection Methods}{10}{subsection.2.4}
\contentsline {subsubsection}{\numberline {2.4.1}Sphere Collisions}{10}{subsubsection.2.4.1}
\contentsline {subsubsection}{\numberline {2.4.2}Spatial Partitioning}{11}{subsubsection.2.4.2}
\contentsline {paragraph}{\nonumberline Regular Grids}{11}{section*.8}
\contentsline {paragraph}{\nonumberline Octrees}{11}{section*.9}
\contentsline {subsection}{\numberline {2.5}Collision Response}{12}{subsection.2.5}
\contentsline {section}{\numberline {3}Design and Scope of the Project}{13}{section.3}
\contentsline {section}{\numberline {4}Program Design}{14}{section.4}
\contentsline {subsection}{\numberline {4.1}Window Structure}{14}{subsection.4.1}
\contentsline {section}{\numberline {5}Protein Viewing}{15}{section.5}
\contentsline {subsection}{\numberline {5.1}Protein Loading}{15}{subsection.5.1}
\contentsline {subsection}{\numberline {5.2}Protein Rendering}{15}{subsection.5.2}
\contentsline {subsubsection}{\numberline {5.2.1}Sphere-rendering Techniques}{16}{subsubsection.5.2.1}
\contentsline {subsection}{\numberline {5.3}Viewing Proteins Individually}{17}{subsection.5.3}
\contentsline {subsection}{\numberline {5.4}Transformation of Molecules}{17}{subsection.5.4}
\contentsline {subsubsection}{\numberline {5.4.1}Projecting and Unprojecting Coordinates}{18}{subsubsection.5.4.1}
\contentsline {subsubsection}{\numberline {5.4.2}Proximity of Cursor to Molecules}{18}{subsubsection.5.4.2}
\contentsline {subsubsection}{\numberline {5.4.3}Translation of Molecules}{19}{subsubsection.5.4.3}
\contentsline {subsubsection}{\numberline {5.4.4}Rotation of Molecules}{19}{subsubsection.5.4.4}
\contentsline {subsection}{\numberline {5.5}Atom Clipping}{21}{subsection.5.5}
\contentsline {section}{\numberline {6}Collision Detection and Response}{23}{section.6}
\contentsline {subsection}{\numberline {6.1}Collision Detection}{23}{subsection.6.1}
\contentsline {subsubsection}{\numberline {6.1.1}Brute-Force Collisions}{23}{subsubsection.6.1.1}
\contentsline {subsubsection}{\numberline {6.1.2}Building a 3D Regular Grid}{24}{subsubsection.6.1.2}
\contentsline {subsubsection}{\numberline {6.1.3}Utilising a 3D Regular Grid to Aid Collision Detection}{29}{subsubsection.6.1.3}
\contentsline {subsection}{\numberline {6.2}Collision Response}{30}{subsection.6.2}
\contentsline {section}{\numberline {7}Force Calculations}{32}{section.7}
\contentsline {section}{\numberline {8}Haptics and Tactile Feedback}{36}{section.8}
\contentsline {subsection}{\numberline {8.1}Haptic Control}{36}{subsection.8.1}
\contentsline {subsection}{\numberline {8.2}Haptic Feedback}{37}{subsection.8.2}
\contentsline {section}{\numberline {9}Additional User Interaction, Control and Feedback}{38}{section.9}
\contentsline {subsection}{\numberline {9.1}Keyboard and Mouse Control}{38}{subsection.9.1}
\contentsline {subsection}{\numberline {9.2}User Feedback}{39}{subsection.9.2}
\contentsline {subsubsection}{\numberline {9.2.1}Audio Feedback}{39}{subsubsection.9.2.1}
\contentsline {subsubsection}{\numberline {9.2.2}The Force Viewer}{40}{subsubsection.9.2.2}
\contentsline {paragraph}{\nonumberline Direct3D}{40}{section*.17}
\contentsline {paragraph}{\nonumberline Showing the Correct Forces}{40}{section*.18}
\contentsline {section}{\numberline {10}Graphical User Interface (GUI)}{41}{section.10}
\contentsline {subsection}{\numberline {10.1}GUI Design}{41}{subsection.10.1}
\contentsline {subsection}{\numberline {10.2}Problems with the GUI}{42}{subsection.10.2}
\contentsline {subsection}{\numberline {10.3}Interaction Logic}{42}{subsection.10.3}
\contentsline {section}{\numberline {11}Testing}{44}{section.11}
\contentsline {subsection}{\numberline {11.1}Performance Testing}{44}{subsection.11.1}
\contentsline {subsubsection}{\numberline {11.1.1}Performance Comparison}{44}{subsubsection.11.1.1}
\contentsline {subsubsection}{\numberline {11.1.2}Rendering Performance Testing}{45}{subsubsection.11.1.2}
\contentsline {subsubsection}{\numberline {11.1.3}3D Regular Grid Performance Testing}{46}{subsubsection.11.1.3}
\contentsline {subsubsection}{\numberline {11.1.4}Attempting to Dock Molecules}{47}{subsubsection.11.1.4}
\contentsline {subsubsection}{\numberline {11.1.5}Force Calculation Testing}{48}{subsubsection.11.1.5}
\contentsline {subsubsection}{\numberline {11.1.6}OpenTK vs SharpGL}{50}{subsubsection.11.1.6}
\contentsline {subsection}{\numberline {11.2}User Testing}{50}{subsection.11.2}
\contentsline {section}{\numberline {12}Conclusions}{52}{section.12}
\contentsline {section}{\numberline {13}Future Work}{53}{section.13}
\contentsline {subsection}{\numberline {13.1}Saving and Loading Dock Sites}{53}{subsection.13.1}
\contentsline {subsection}{\numberline {13.2}Rendering Performance}{54}{subsection.13.2}
\contentsline {section}{References}{55}{section*.31}
